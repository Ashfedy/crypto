import eMail from '@sendgrid/mail';
import * as constants from '../config/const';

export default function send_mail(action, user_email, username, uid, full_name) {
  const mail_style = "<style>"
  +".wa-email-container{text-align: center;color: #333;font-size: 16px;}"
  +".wa-email-header{color: #000;}"
  +".main-content a:link{padding: 20px 40px; background: #5EC75D; color: #fff; text-decoration: none;}"
  +"</style>";

  const expiry_date = Date.now()+900000;

  eMail.setApiKey(constants.SEND_GRID_API_KEY);
  switch (action) {
    case 'FORGOT_PASSWORD':
      let forgot_password_object = {
        to: user_email,
        from: 'help@walletaddress.io',
        subject: 'Reset Wallet Address Password',
        text: "Click the following link to reset "+username+"'s Wallet Address Password \n https://walletaddress.io/reset-password/"+uid+"_"+expiry_date+"\n 'If you did not request this, please ignore this email and your password will remain unchanged.",
        html: mail_style+"<div class='wa-email-container'><h1 class='wa-email-header'>WALLET ADDRESS</h1><div class='main-content'><p>You are receiving this because you (or someone else) have requested the reset of the password for your account.</p><p>Please click on the following link or paste this into your browser to reset "+username+"'s Wallet Address Password</p></div><a href='https://walletaddress.io/reset-password/"+uid+"_"+expiry_date+"'>https://walletaddress.io/reset-password/"+uid+"_"+expiry_date+"</a><p>This link will expire in 15 minutes.</p><br/><h3>'If you did not request this, please ignore this email and your password will remain unchanged.</h3></div>"
      };
      eMail.send(forgot_password_object);
      break;
    case 'RESET_PASSWORD':
      let reset_password_object = {
        to: user_email,
        from: 'help@walletaddress.io',
        subject: 'Success - Reset Wallet Address Password',
        text: "Your wallet address password has been recently changed. If you do not recognise this activity please inform us immediately",
        html: mail_style+"<div class='wa-email-container'><h1 class='wa-email-header'>WALLET ADDRESS</h1><div class='main-content'>Your wallet address password has been recently changed. If you do not recognise this activity please inform us immediately.</div></div>"
      };
      eMail.send(reset_password_object);
      break;
    case 'SIGN_UP':
      let signup_object = {
        to: user_email,
        from: 'noreply@walletaddress.io',
        subject: 'Welcome to Wallet Address',
        text: 'Wallet Address is a central hub for all cryptocurrency address. Wallet address helps you share all your address with just one public url. Feel free to write to us. Your new public address https://walletaddress.io/user/'+username,
        html: mail_style+"<div class='wa-email-container'><h1 class='wa-email-header'>WALLET ADDRESS</h1><h1 class='wa-email-name'>"+full_name+"</h1><div class='main-content'>Wallet Address is a central hub for all cryptocurrency address. Wallet address helps you share all your address with just one public url. <br/>Feel free to write to us.</div><div>Your Public Wallet Address: https://walletaddress.io/user/"+username+"</div></div>"
      };
      eMail.send(signup_object);
    default:
  }
}
