import userController from './controllers/userController';
import walletController from './controllers/walletController';
import searchController from './controllers/searchController';
const router = (app) => { //(app) add "app" as parameter  to the router function
	// console.log('in the router....');
	app.post('/login', userController.login);
	app.post('/relogin', userController.relogin);
	app.post('/signup', userController.signup);
	app.get('/logout', userController.logout);
	app.post('/forgotPassword', userController.forgotPassword);
	app.post('/resetPassword', userController.resetPassword);
	app.post('/updateUsername', userController.updateUsername);
	app.post('/updateUser', userController.updateUser);
	app.post('/uploadDp', userController.uploadDp); //temp
	app.post('/loadWalletList', walletController.loadWalletList);
	app.post('/addWallet', walletController.addWallet);
	app.post('/updateWallet', walletController.updateWallet);
	app.post('/deleteWallet', walletController.deleteWallet);
	app.post('/sortWallet', walletController.sortWallet);
	app.post('/searchUsername', searchController.searchUsername);
};

export default router;
