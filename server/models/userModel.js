import mongoose from 'mongoose';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';

const Schema = mongoose.Schema;
// TODO: Add uniqueness to email
const userSchema = new Schema({
  username: {type: String, required: true},
  password: {type: String, required: true},
  reset_password: {type: Number, default: 0},
  password_history: {type: Array},
  email: {type: String, required: true, lowercase: true, index: true},
  full_name: {type: String, required: true},
  user_image: {type: String},
  profile_color: {type: String, default: 'blue-violet'}
}, { collection : 'user'});

userSchema.methods.generateJWT = function generateJWT() {
  return jwt.sign({
    uid: this._id,
    email: this.email,
    full_name: this.full_name,
    username: this.username,
    user_image: this.user_image
  },'secretkey');
};

userSchema.methods.isValidPassword = function isValidPassword(password) {
  const trueOrFalse = bcrypt.compareSync(password, this.password);
  return trueOrFalse;
};

userSchema.methods.toAuthJSON = function toAuthJSON() {
  return {
    uid: this._id,
    email: this.email,
    full_name: this.full_name,
    username: this.username,
    user_image: this.user_image,
    profile_color: this.profile_color,
    reset_password: this.reset_password,
    token: this.generateJWT()
  };
};

userSchema.methods.searchJSON = function searchJSON() {
  return {
    full_name: this.full_name,
    username: this.username,
    user_image: this.user_image,
    profile_color: this.profile_color,
  };
};

export default mongoose.model('user', userSchema);
