import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const orderSchema = new Schema({
  uid: {type: String, required: true, index: true},
  order: {type: Array}
}, { collection : 'order'});

export default mongoose.model('order', orderSchema);
