import mongoose from 'mongoose';

const Schema = mongoose.Schema;
// TODO: Add uniqueness to email
const walletSchema = new Schema({
  uid: {type: String, required: true, index: true},
  type: {type: String, required: true},
  name: {type: String, required: true},
  address: {type: String, required: true}
}, { collection : 'wallet'});

walletSchema.methods.response_data = function response_data() {
  return {
    wid: this._id, //Wallet ID
    type: this.type, //Wallet Type
    name: this.name, //Wallet Name
    address: this.address //Wallet Address
  };
};

export default mongoose.model('wallet', walletSchema);
