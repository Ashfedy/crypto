import Wallet from '../models/walletModel';
import WalletOrder from '../models/orderModel';

// // Load gallery items on initial page load
class WalletController {
  static loadWalletList(req, res, next) {
    const request_data = req.body;
    Wallet.find({uid: request_data.uid}, (err, walletList) => {
      if (err) { return next(err); }
      let sortedWalletList = [];
      WalletOrder.find({uid: request_data.uid}, (err, walletOrder) => {
        walletOrder[0].order.map(wid => {
          walletList.map((wallet) => {
            if(wallet._id.toString() == wid) {
              let tempWallet = {};
              tempWallet.wid = wallet._id;
              tempWallet.type = wallet.type;
              tempWallet.name = wallet.name;
              tempWallet.address = wallet.address;
              sortedWalletList.push(tempWallet);
            }
          });
        });
        return res.json(sortedWalletList);
      });
    });
  }

  static addWallet(req, res, next) {
    const request_data = req.body;
    Wallet.create(request_data, (err, wallet) => {
      if (err) { return next(err); }
      WalletOrder.find({uid: wallet.uid}, (err, walletOrder) => {
        const wallet_id_to_be_added = wallet._id.toString();
        walletOrder[0].order.push(wallet_id_to_be_added);
        WalletOrder.findOneAndUpdate({uid: wallet.uid},{ $set:{"order": walletOrder[0].order} }, {new: true}, (err, wallet) => {
          return true;
        });
        return true;
      });
      return res.json(wallet.response_data());
    });
  }

  static deleteWallet(req, res, next) {
    const request_data = req.body;
    Wallet.findByIdAndRemove(request_data.wid, (err, wallet) => {
      if (err) { return next(err); }
      WalletOrder.find({uid: wallet.uid}, (err, walletOrder) => {
          const wallet_id_to_be_removed = wallet._id.toString();
          const id_deleted_order_list = walletOrder[0].order.filter(wid => wid != wallet_id_to_be_removed);
          WalletOrder.findOneAndUpdate({uid: wallet.uid},{ $set:{"order": id_deleted_order_list} }, {new: true}, (err, wallet) => {
            return true;
          });
          return true;
      });
      return res.json(wallet.response_data());
    });
  }

  static updateWallet(req, res, next) {
    const request_data = req.body;
    Wallet.findByIdAndUpdate(request_data.wid,{ $set:request_data }, {new: true}, (err, wallet) => {
      if (err) { return next(err); }
      return res.json((wallet.response_data()));
    });
  }

  static sortWallet(req, res, next) {
    const request_data = req.body;
    WalletOrder.findOneAndUpdate({uid: request_data.uid},{ $set:{"order": request_data.walletOrder} }, {new: true}, (err, wallet) => {
      if (err) { return next(err); }
      return res.json({'status': "SUCCESS"});
    });
  }

}

export default WalletController;
