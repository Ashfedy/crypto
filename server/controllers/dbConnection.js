import mongoose from 'mongoose';
import Promise from 'bluebird';
import * as constants from '../config/const';

export default function db_connection() {
  mongoose.Promise = Promise;
  const mongoDB = 'mongodb://'+constants.DB_USER+':'+constants.DB_PASSWORD+'@'+constants.DB_HOST+'/'+constants.DB_NAME;
  // const mongoDB = 'mongodb://AshfedyAdmin:wallet_address_admin!18@ec2-35-168-237-142.compute-1.amazonaws.com/wallet_address';
  mongoose.connect(mongoDB, {
    useMongoClient: true
  });
  const db = mongoose.connection;
  return db;
}
