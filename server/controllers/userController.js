import User from '../models/userModel';
import WalletOrder from '../models/orderModel';
import bcrypt from 'bcrypt';
import AWS from 'aws-sdk';
import Busboy from 'busboy';
import * as constants from '../config/const';
import send_mail from '../config/emailTemplate';

// // Load gallery items on initial page load
class UserController {
  static login(req, res) {
    const credentials = req.body;
    User.findOne({email: credentials.email}).then(user => {
      // console.log(user);
      if (user && user.isValidPassword(credentials.password)) {
        return res.json({user:user.toAuthJSON()});
      }
      else {
        return res.json({error:"Invalid credentials"});
      }
    });
  }

  static forgotPassword(req, res, next) {
    const request_data = req.body;
    User.findOneAndUpdate({email: request_data.email},{ $set:{"reset_password": 1} }, {new: true}, (err, user) => {
      if (err) { return next(err); }
      if (user != undefined || null) {
        send_mail(constants.EMAIL_FORGOT_PASSWORD, user.email, user.username, user._id);
        return res.json({message:"SUCCESS"});
      }
      else {
        return res.json({message:"NO_USER"});
      }
    });
  }

  static resetPassword(req, res, next) {
    const request_data = req.body;
    request_data.password = bcrypt.hashSync(request_data.password, 10); //Encrypting password before saving to database
    User.findById(request_data.uid, (err1, user_temp) => {
      if (err1) { return next(err1); }
      if (user_temp != undefined || null) {
        if(user_temp.reset_password == 1) {
          user_temp.password_history.push(user_temp.password);
          User.findByIdAndUpdate(request_data.uid,{ $set:{"password": request_data.password, "reset_password": 0, "password_history": user_temp.password_history} }, {new: true}, (err, user) => {
            if (err) { return next(err); }
            send_mail(constants.EMAIL_RESET_PASSWORD, user.email, user.username);
            return res.json({message:"SUCCESS"});
          });
        }
        else {
          return res.json({message:"NO_REQUEST_INITIATED"});
        }
      }
      else {
        return res.json({message:"NO_USER"});
      }
    });
  }

  static relogin(req, res) {
    const request_data = req.body;
    User.findOne({_id: request_data.uid}).then(user => {
        return res.json({user:user.toAuthJSON()});
    });
  }

  static signup(req, res, next) {
    const request_data = req.body;
    request_data.password = bcrypt.hashSync(request_data.password, 10); //Encrypting password before saving to database

    let email_exist = true;
    let username_exist = true;
    //  console.log("1");
    User.find({"email": request_data.email}, (err1, userEmail) => {
      if (err1) { return next(err1); }
      //  console.log("2");
      if(userEmail.length == 0) {
        //  console.log("3");
        email_exist = false;
      }
      User.find({"username": request_data.username}, (err2, username) => {
        if (err2) { return next(err2); }
        //  console.log("4");
        if(username.length == 0) {
          //  console.log("5");
          username_exist = false;
        }
        if(!username_exist && !email_exist) {
          //  console.log("6");
          User.create(request_data, (err3, user) => { //Creating a new document
            //  console.log("7");
            if (err3) { return next(err3); }
            const newWalletOrder = {
              "uid": user._id.toString()
            };
            WalletOrder.create(newWalletOrder, (err4, walletOrder) => {
              //  console.log("8");
              if (err4) { return next(err4); }
              send_mail(constants.EMAIL_SIGN_UP, user.email, user.username, null, user.full_name);
              return res.json({user:user.toAuthJSON()});
            });
          });
        }
        else if(username_exist && email_exist) {
          return res.json({user:"USER_EXIST"});
        }
        else if(username_exist) {
          return res.json({user:"USERNAME_EXIST"});
        }
        else {
          return res.json({user:"EMAIL_EXIST"});
        }
      });
    });
  }

  static logout(req, res) {
    return res.send("Logged out");
  }

  static updateUsername(req, res, next) {
    const request_data = req.body;
    let username_exist = true;
    User.find({"username": request_data.username}, (err2, username) => {
      if (err2) { return next(err2); }
      if(username.length == 0) {
        username_exist = false;
      }
      if(!username_exist) {
        User.findByIdAndUpdate(request_data.uid,{ $set:{username: request_data.username} }, {new: true}, (err, user) => {
          if (err) { return next(err); }
          return res.json(user.toAuthJSON());
        });
      }
      else {
        return res.json({user:"USERNAME_EXIST"});
      }

    });

  }

  static updateUser(req, res, next) {
    const request_data = req.body;
    const id = request_data.uid;
    delete request_data.username;
    delete request_data.uid;
    if(request_data.password == "") delete request_data.password;
    else request_data.password = bcrypt.hashSync(request_data.password, 10);

    User.findByIdAndUpdate(id,{ $set: request_data}, {new: true}, (err, user) => {
      if (err) { return next(err); }
      return res.json(user.toAuthJSON());
    });
  }


  static uploadToS3(res, uid, file) {
    let s3bucket = new AWS.S3({
      accessKeyId: constants.AWS_IAM_USER_KEY,
      secretAccessKey: constants.AWS_IAM_USER_SECRET,
      Bucket: constants.AWS_BUCKET_NAME
    });
    s3bucket.createBucket(function () {
        var params = {
          Bucket: constants.AWS_DP_BUCKET,
          Key: file.name,
          Body: file.data
        };
        s3bucket.upload(params, function (err, data) {
          if (err) {
            console.log(err);
          }
          User.findByIdAndUpdate(uid,{ $set: {"user_image": data.Location}}, {new: true}, (err, user) => {
            if (err) { return next(err); }
            return res.json(user.toAuthJSON());
          });
        });
    });
  }

  static uploadDp(req, res, next) {
    const uid = req.body.uid;
    let fileNameArray = req.files.photo.name.split('.');
    const ext = fileNameArray[fileNameArray.length-1];
    const timeStamp = new Date().getTime();
    req.files.photo.name = uid+'_'+timeStamp+'.'+ext;
    let busboy = new Busboy({ headers: req.headers });
    busboy.on('finish', function() {
      const file = req.files.photo;
      UserController.uploadToS3(res, uid, file);
    });
    req.pipe(busboy);
  }
}

export default UserController;
