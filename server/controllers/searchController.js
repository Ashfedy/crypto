import User from '../models/userModel';
import Wallet from '../models/walletModel';
import WalletOrder from '../models/orderModel';

class SearchController {
  static searchUsername(req, res, next) {
    const request_data = req.body;
    User.find({"username": request_data.username}, (err1, user) => {
      if(user[0] != undefined || null) {
        const uid = user[0]._id;
        const searched_user = user[0].searchJSON();
        Wallet.find({uid: uid}, (err, walletList) => {
          if (err) { return next(err); }
          let sortedWalletList = [];
          WalletOrder.find({uid: uid}, (err, walletOrder) => {
            walletOrder[0].order.map(wid => {
              walletList.map((wallet) => {
                if(wallet._id.toString() == wid) {
                  let tempWallet = {};
                  tempWallet.wid = wallet._id;
                  tempWallet.type = wallet.type;
                  tempWallet.name = wallet.name;
                  tempWallet.address = wallet.address;
                  sortedWalletList.push(tempWallet);
                }
              });
            });
            return res.json({search_result:{user: searched_user, wallet: sortedWalletList}});
          });
        });
      }
      else {
        return res.json({search_result:"USER_NOT_FOUND"});
      }
    });
  }
}

export default SearchController;
