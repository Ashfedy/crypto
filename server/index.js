import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import router from './router';
import db_connection from './controllers/dbConnection';
import busboy from 'connect-busboy';
import busboyBodyParser from 'busboy-body-parser';

const app = express();

//Set up mongoose connection
const db = db_connection();
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
// console.log(db.gallery.find());
app.use(cors({
origin: ["http://localhost:8000"],
methods: ["GET", "POST", "OPTIONS"],
allowedHeaders: ["Content-Type", "Authorization", "multipart/form-data"]
}));

app.use(busboy());

app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

//app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.json({ type: 'text/plain' }));

app.use(busboyBodyParser({ limit: '2mb' }));

router(app);

app.set("port", process.env.PORT || 9000);

                            // Express only serves static assets in production //
// if (process.env.NODE_ENV === "production") {
//   app.use(express.static("client/build"));
// }

app.listen(app.get("port"), () => {
  console.log(`Find the server at: http://localhost:9000/`); // eslint-disable-line no-console
});
