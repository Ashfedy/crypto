import React from 'react';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as walletActions from '../../actions/walletActions';
import {Link} from 'react-router-dom';
import AddWallet from './AddWallet';
import WalletList from './walletList/WalletListView';
import alertify from 'alertify.js';
import QRCode from 'qrcode';
import * as data from '../../config/crypto_data';

class Dashboard extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      wallet: {
        type: "",
        name: "",
        address: ""
      },
      tempWallet: {
        Ttype: "",
        Tname: "",
        Taddress: "",
        wid: ""
      }
    };
    this.onChange = this.onChange.bind(this);
    this.walletOnChange = this.walletOnChange.bind(this);
    this.addWallet = this.addWallet.bind(this);
    this.editWallet = this.editWallet.bind(this);
    this.updateWallet = this.updateWallet.bind(this);
    this.deleteWallet = this.deleteWallet.bind(this);
    this.copyAddress = this.copyAddress.bind(this);
    this.generateQR = this.generateQR.bind(this);
    this.onSort = this.onSort.bind(this);
    this.ChangeHeaderLinkBackground = this.ChangeHeaderLinkBackground.bind(this);
  }

  ChangeHeaderLinkBackground(header_link_id) {
    $('.navbar-nav li').removeClass('activeLink');
    $('#'+header_link_id).addClass('activeLink');
  }

  onChange(e) {
    const _this = this;
    $( "#addWalletInput" ).autocomplete({
      select: function( event, ui ) {
        _this.setState({"wallet": { "address": _this.state.wallet.address || "", "name": _this.state.wallet.name || "", "type": ui.item.value}});
      },
      source: function(request, response) {
        let results = $.ui.autocomplete.filter(data.CRYPTO_DATA, request.term);
        response(results.splice(0,30));
      }
    });
    const { value, name } = e.target;
    const { wallet } = this.state;
    wallet[name] = value;
    this.setState({ wallet });
    if(wallet.type != '' && wallet.name != '' && wallet.address != '') {
      $('#addWalletSubmitBtn').removeClass('disabled');
    }
    else {
      $('#addWalletSubmitBtn').addClass('disabled');
    }
  }

  walletOnChange(e) {
    const { value, name } = e.target;
    const { tempWallet } = this.state;
    tempWallet[name] = value;
    this.setState({ tempWallet });
    if(tempWallet.name != '' && tempWallet.address != '') {
      $('#updateWalletSubmitBtn').removeClass('disabled');
    }
    else {
      $('#updateWalletSubmitBtn').addClass('disabled');
    }
  }

  addWallet() {
    const { wallet } = this.state;
    const uid = document.getElementById('uid').getAttribute("data-uid");
    this.props.actions.addWallet(wallet.type, wallet.name, wallet.address, uid, this.props.wallet.length);
    this.setState({"wallet": { "address": "", "name": "", "type": ""}});
  }

  editWallet(e) {
    // $('#myModal').on('shown.bs.modal');
    $('#walletEditModal').modal('show');
    const _this = e.currentTarget;
    let wid = $(_this).parents('.wallet-row').attr('data-wid');
    let wallet_type = $(_this).parents('.wallet-row').find('.wallet-type').attr('data-type');
    let wallet_name = $(_this).parents('.wallet-row').find('.wallet-name').attr('data-name');
    let wallet_address = $(_this).parents('.wallet-row').find('.wallet-address').attr('data-address');
    this.setState({"tempWallet": { "Taddress": wallet_address, "Tname": wallet_name, "Ttype": wallet_type, "wid": wid }});
  }

  deleteWallet(e) {
    const _this = e.currentTarget;
    const walletActions = this.props.actions;
    let wid = $(_this).parents('.wallet-row').attr('data-wid');
    let type = $(_this).parents('.wallet-row').find('.wallet-type').attr('data-type');
    alertify.confirm(type+" wallet will be removed!", function () {
          walletActions.deleteWallet(wid);
        });
  }

  updateWallet() {
    const { tempWallet } = this.state;
    this.props.actions.updateWallet(tempWallet.Ttype, tempWallet.Tname, tempWallet.Taddress, tempWallet.wid);
    this.setState({"tempWallet": { "Taddress": "", "Tname": "", "Ttype": "", "wid": "" }});
  }

  copyAddress(e) {
    const _this = e.currentTarget;
    let address = $(_this).parents('.wallet-row').find('.wallet-address').attr("data-address");
    let type = $(_this).parents('.wallet-row').find('.wallet-type').attr("data-type");
    $("#copyText").val(address);
    const copyText = document.getElementById("copyText");
    copyText.select();
    document.execCommand("Copy");
    alertify.logPosition("bottom right");
    alertify.log(type+" address copied!");
    // alert("Copied the text: " + copyText.value);
  }

  generateQR(e) {
    const _this = e.currentTarget;
    let address = $(_this).parents('.wallet-row').find('.wallet-address').attr("data-address");
    let type = $(_this).parents('.wallet-row').find('.wallet-type').attr("data-type");

    QRCode.toDataURL(address,
      {
        color: {
          dark: '#000',
          light: '#0000' // Transparent background
        }
      },
      function (err, url) {
        $("#qrcodeHolder").attr('src',url)
        $('#QRModal').modal('show');
    });
  }

  onSort(sortedList) {
    const uid = document.getElementById('uid').getAttribute("data-uid");
    const sortedWalletList = [];
    const unsortedWalletList = this.props.wallet;
    const WalletOrder = [];
    sortedList.map((wallet) => {
      let tempWalletObject = {
        "wid": wallet.content.key,
        "type": wallet.content.props.walletType,
        "name": wallet.content.props.walletName,
        "address": wallet.content.props.walletAddress
      }
      sortedWalletList.push(tempWalletObject);
      WalletOrder.push(wallet.content.key);
    });
    this.props.actions.sortWallet(uid, WalletOrder, sortedWalletList, unsortedWalletList);
  }

  render() {
    const { wallet: { type, name, address } } = this.state;
    const { tempWallet: { Ttype, Tname, Taddress } } = this.state;
    return (
       <div className="dashboard-wrapper">
         <div className="container dashboard-container">
         <div className="row">
           <div className="col-lg-2 col-md-2 display-none-sm navigation-tabs-section">
             <div className="navigation-tabs dashboard-tab active"><Link to="/dashboard">Dashboard</Link></div>
             <div className="navigation-tabs profile-tab"><Link to="/profile">Profile</Link></div>
           </div>
           <div className="col-lg-10 col-md-10 col-sm-12 col-xs-12 dashboard-section">
              <AddWallet type={type} name={name} address={address} onChange={this.onChange} addWallet={this.addWallet}/>
              <WalletList walletList={this.props.wallet} editWallet={this.editWallet} type={Ttype} name={Tname} address={Taddress} walletOnChange={this.walletOnChange} updateWallet={this.updateWallet} deleteWallet={this.deleteWallet} copyAddress={this.copyAddress} generateQR={this.generateQR} onSort={this.onSort}/>
            </div>
         </div>
         </div><span>{this.ChangeHeaderLinkBackground("dashboardHeaderLink")}</span>
       </div>
    );
  }
}

Dashboard.propTypes = {
  wallet: PropTypes.array,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    wallet: state.wallet
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(walletActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
