import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router';
import * as data from '../../../config/crypto_data';

const WalletView = ({walletId, walletType, walletName, walletAddress, editWallet, deleteWallet, copyAddress, generateQR}) => {
  const maskedAddress = ({walletAddress}) => {
    return walletAddress.substring(0,5)+"....."+walletAddress.substring(walletAddress.length - 5);
  }
  return (
    <div className="row wallet-row" data-wid={walletId}>
      <div className="col-lg-1 col-md-1 col-sm-2 col-xs-2 wallet-img-container">
        <img src={data.CRYPTO_DATA.filter(obj => obj.value == walletType)[0].img}/>
        <p className="wallet-type display-none" data-type={walletType}>{walletType}</p>
      </div>
      <div className="col-lg-3 col-md-3 col-sm-4 col-xs-4 wallet-name-container">
        <p className="wallet-name" data-name={walletName}>{walletName}</p>
      </div>
      <div className="col-lg-7 col-md-7 display-none-sm"><p className="wallet-address" data-address={walletAddress}>{walletAddress}</p></div>
      <div className="col-sm-4 col-xs-4 display-sm"><p className="wallet-address" data-address={walletAddress}>{maskedAddress({walletAddress})}</p></div>
      <div className="col-lg-1 col-md-1 col-sm-2 col-xs-2">

      <div className="dropdown wallet-actions">
        <button className="btn btn-default dropdown-toggle" type="button" id="walletActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
          <span className="glyphicon glyphicon-cog"></span>
        </button>
        <div className="wallet-sort-icon"><img src="https://s3.ap-south-1.amazonaws.com/cryptolink/essentials/sort_icon.png"/></div>
        <ul className="dropdown-menu" aria-labelledby="walletActions">
          <li onClick={editWallet}><span className="glyphicon glyphicon-pencil"></span><span className="wallet-action-title">EDIT</span></li>
          <li onClick={deleteWallet}><span className="glyphicon glyphicon-trash"></span><span className="wallet-action-title">DELETE</span></li>
          <li onClick={copyAddress}><span className="glyphicon glyphicon-copy"></span><span className="wallet-action-title">COPY</span></li>
          <li onClick={generateQR}><span className="glyphicon glyphicon-qrcode"></span><span className="wallet-action-title">QR CODE</span></li>
        </ul>
      </div>



        <div className="col-lg-2 col-md-2 col-sm-3 wallet-settings"></div>
        <div className="col-lg-2 col-md-2 col-sm-3 wallet-delete"></div>
        <div className="col-lg-2 col-md-2 col-sm-3 wallet-copy"></div>
        <div className="col-lg-2 col-md-2 col-sm-3 wallet-qr"></div>
      </div>
    </div>
  );
};
WalletView.propTypes = {
  walletId: PropTypes.string.isRequired,
  walletType: PropTypes.string.isRequired,
  walletName: PropTypes.string.isRequired,
  walletAddress: PropTypes.string.isRequired,
  editWallet: PropTypes.func.isRequired,
  deleteWallet: PropTypes.func.isRequired,
  copyAddress: PropTypes.func.isRequired,
  generateQR: PropTypes.func.isRequired
};

export default WalletView;
