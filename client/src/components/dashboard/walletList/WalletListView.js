import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router';
import WalletView from './WalletView';
import Text from '../../common/input/Text';
import Password from '../../common/input/Password';
import DragSortableList from 'react-drag-sortable';
// import Submit from '../../common/input/Submit';

const WalletListView = ({walletList, editWallet, type, name, address, walletOnChange, updateWallet, deleteWallet, copyAddress, generateQR, onSort}) => {
  const walletArray = [];
  {walletList.map(wallet =>
    walletArray.push({content: (<WalletView key={wallet.wid} walletId={wallet.wid} walletType={wallet.type} walletName={wallet.name} walletAddress={wallet.address} editWallet={editWallet} deleteWallet={deleteWallet} copyAddress={copyAddress} generateQR={generateQR}/>)})
  )}
  return (
    <div className="wallet-list-container">
      <h3 className="coin-list-title">Coin List</h3>
      <DragSortableList items={walletArray} dropBackTransitionDuration={0.3} onSort={onSort} type="vertical"/>
      <div className="modal fade" id="walletEditModal" tabIndex="-1" role="dialog" aria-labelledby="walletEdit">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 className="modal-title">Update Wallet</h4>
            </div>
            <div className="modal-body">
              <div className="wallet-edit-form">
                <div className="form-container">
                  <div className="text-field form-group wallet-edit-modal-input">
                    <label className="form-group-label">Wallet Type<span className="red">*</span></label>
                    <input type="text" name={name} value={type} onChange={walletOnChange} readOnly/>
                  </div>
                  <Text name="Tname" value={name} customClass="wallet-edit-modal-input" label="Wallet Name" onChange={walletOnChange} read="hello"/>
                  <Text name="Taddress" value={address} customClass="wallet-edit-modal-input" label="Public Address" onChange={walletOnChange} read="hello"/>
                </div>
              </div>
              <p className="fadeout"><span className="red">*</span> non editable field</p>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
              <button id="updateWalletSubmitBtn" type="button" className="btn btn-success disabled" onClick={updateWallet}>Update</button>
            </div>
          </div>
        </div>
      </div>
      <div className="modal fade" id="QRModal" tabIndex="-1" role="dialog" aria-labelledby="QR Modal">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 className="modal-title">QR Code</h4>
            </div>
            <div className="modal-body">
              <div className="qr-image-container">
                <img id="qrcodeHolder" src="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
WalletListView.propTypes = {
  walletList: PropTypes.array.isRequired,
  type: PropTypes.string,
  name: PropTypes.string,
  address: PropTypes.string,
  walletOnChange: PropTypes.func.isRequired,
  editWallet: PropTypes.func.isRequired,
  deleteWallet: PropTypes.func.isRequired,
  updateWallet: PropTypes.func.isRequired,
  copyAddress: PropTypes.func.isRequired,
  generateQR: PropTypes.func.isRequired,
  onSort: PropTypes.func.isRequired
};

export default WalletListView;
