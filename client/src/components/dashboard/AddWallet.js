import React from 'react';
import PropTypes from "prop-types";
import NoLabelTextInput from '../common/input/NoLabelTextInput';
import Submit from '../common/input/Submit';

const AddWallet = (walletDetails) => {
  return (
    <div>
      <p className="add-wallet-header-title">Add Wallet To List</p>
      <div className="form-container add-wallet-form">
        <div className="row">
        <div className="col-lg-7 col-md-8 col-sm-12 col-xs-12">
        <div className="row">
          <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div className="wallet-type-input-field dashboard-input">
              <label className="form-group-label dashboard-input-label">Type Of Wallet</label>
              <NoLabelTextInput id="addWalletInput" name="type" placeholder="eg., Bitcoin, Ethereum, ..." value={walletDetails.type} onChange={walletDetails.onChange}/>
            </div>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div className="wallet-name-input-field dashboard-input">
              <label className="form-group-label dashboard-input-label">Wallet Name</label>
              <NoLabelTextInput name="name" placeholder="eg., Personal, Business, ..." value={walletDetails.name} onChange={walletDetails.onChange}/>
            </div>
          </div>
          </div>
          </div>
          <div className="col-lg-7 col-md-8 col-sm-6 col-xs-6">
            <div className="wallet-address-input-field dashboard-input">
              <label className="form-group-label dashboard-input-label">Wallet Address</label>
              <NoLabelTextInput name="address" placeholder="1D5tovU1kX6qfVa5H9awAPGAuZ9grsrXZs" value={walletDetails.address} onChange={walletDetails.onChange}/>
            </div>
          </div>
          <div className="col-lg-2 col-md-2 col-sm-6 col-xs-6">
            <Submit id="addWalletSubmitBtn" value="Add Wallet" customClass="add-wallet-btn disabled" onSubmit={walletDetails.addWallet}/>
          </div>
        </div>
      </div>
    </div>
  );
};

AddWallet.propTypes = {
  walletDetails: PropTypes.string
};

export default AddWallet;
