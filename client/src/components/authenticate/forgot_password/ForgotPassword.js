import React from 'react';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import ForgotPasswordView from './ForgotPasswordView';
import * as authenticateActions from '../../../actions/authenticateActions';
import alertify from 'alertify.js';

class ForgotPassword extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      user: {
        email: ""
      }
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    $('.navbar-nav li').removeClass('activeLink');
    $('#cryptoLink').addClass('unauthenticated');
  }

  validateEmail(email) {
    const email_re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return email_re.test(email.toLowerCase());
  }

  onSubmit() {
    const { user } = this.state;
    let isValidEmail = this.validateEmail(user.email);
    if(user.email != '') {
      if(!isValidEmail) {
        alertify.error("Email Not Valid");
      }
      else {
        this.props.actions.forgot_password(user.email);
      }
    }
    else {
      alertify.error("Email field cannot be empty");
    }
  }

  onChange(e) {
    const { value, name } = e.target;
    const { user } = this.state;
    user[name] = value;
    this.setState({ user });
    if(user.email == '') {
      $('#forgotPasswordButton').removeClass('pristine');
    }
    else {
      $('#forgotPasswordButton').addClass('pristine');
    }
  }

  render() {
    const { user: { email } } = this.state;
    return (
      <div className="forgot-password-wrapper">
        <div className="forgot-password-container">
          <ForgotPasswordView email={email} onChange={this.onChange} onSubmit={this.onSubmit}/>
        </div>
      </div>
    );
  }
}

ForgotPassword.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(authenticateActions, dispatch)
  };
}

export default connect(null, mapDispatchToProps)(ForgotPassword);
