import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router';
import NoLabelTextInput from '../../common/input/NoLabelTextInput';
import Submit from '../../common/input/Submit';

const ForgotPasswordView = (forgotPasswordProps) => {
  return (
    <div className="forgot-password-section">
      <div className="forgot-password-header">
        <h3>Forgot Password? <span>Fret not!</span></h3>
        <p>Enter your email address to reset password</p>
      </div>
      <div className="form-container forgot-password-form">
        <div className="forgot-password-input-field">
          <label>Email</label>
          <NoLabelTextInput id="forgotPassword" name="email" value={forgotPasswordProps.email} label="Email" customClass="cl-login-email-field" placeholder="ashfedy@gmail.com" onChange={forgotPasswordProps.onChange}/>
        </div>
        <Submit id="forgotPasswordButton" value="Reset Password" customClass="cl-login-btn-container" onSubmit={forgotPasswordProps.onSubmit}/>
      </div>
    </div>
  );
};
ForgotPasswordView.propTypes = {
  forgotPasswordProps: PropTypes.object
};

export default ForgotPasswordView;
