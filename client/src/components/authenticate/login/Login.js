import React from 'react';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import LoginView from './LoginView';
import * as authenticateActions from '../../../actions/authenticateActions';

class Login extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      user: {
        email: "",
        password: ""
      }
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    $('.navbar-nav li').removeClass('activeLink');
    $('#cryptoLink').addClass('unauthenticated');
  }

  onSubmit() {
    const { user } = this.state;
    // const { login } = this.props.actions;
    this.props.actions.login(user.email, user.password).then(() => {
      if(this.props.isAuthenticated) {
        this.props.history.push("/dashboard");
      }
      else this.props.history.push("/login");
    });
}

  onChange(e) {
    const { value, name } = e.target;
    const { user } = this.state;
    user[name] = value;
    this.setState({ user });
    if(user.email == '' || user.password == '' ) {
      $('#loginButton').removeClass('pristine');
    }
    else {
      $('#loginButton').addClass('pristine');
    }
  }

  render() {
      const { user: { email, password } } = this.state;
      return (
        <div className="login-wrapper">
          <div className=" login-container container">
            <LoginView email={email} password={password} onChange={this.onChange} onSubmit={this.onSubmit}/>
          </div>
        </div>
      );
  }
}

Login.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  actions: PropTypes.object.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  })
};

function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.user.token
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(authenticateActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
