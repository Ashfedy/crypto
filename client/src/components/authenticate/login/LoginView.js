import React from 'react';
import PropTypes from "prop-types";
import NoLabelTextInput from '../../common/input/NoLabelTextInput';
import NoLabelPasswordInput from '../../common/input/NoLabelPasswordInput';
import Text from '../../common/input/Text';
import Password from '../../common/input/Password';
import Submit from '../../common/input/Submit';
import {Link} from 'react-router-dom';

const LoginView = (loginProps) => {
  return (
    <div className="login-section">
      <div className="login-section-header">
        <h3>Sign In <span>to Wallet Address</span></h3>
        <p>Don't have an account? <Link to="/signup">Sign up here</Link></p>
      </div>
      <div className="form-container login-form">
        <div className="login-input-field">
          <label>Email</label>
          <NoLabelTextInput id="loginEmail" name="email" value={loginProps.email} label="Email" customClass="cl-login-email-field" placeholder="johndoe@email.com" onChange={loginProps.onChange}/>
        </div>
        <div className="login-input-field">
          <label>Password</label>
          <NoLabelPasswordInput id="loginPassword" name="password" value={loginProps.password} label="Password" customClass="cl-login-password-field" placeholder="***********" onChange={loginProps.onChange}/>
        </div>
        <p className="forgot-password-link"><Link to="/forgot-password">Forgot Password?</Link></p>
        <Submit id="loginButton" value="Sign In to Wallet Address" customClass="cl-login-btn-container" onSubmit={loginProps.onSubmit}/>
      </div>
      <div className="login-error-section display-none">
        <p className="login-error">**INVALID CREDENTIALS**</p>
      </div>
    </div>
  );
};
LoginView.propTypes = {
  loginProps: PropTypes.object
};

export default LoginView;
