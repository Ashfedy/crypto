import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router';
import NoLabelPasswordInput from '../../common/input/NoLabelPasswordInput';
import Submit from '../../common/input/Submit';

const ResetPasswordView = (resetPasswordProps) => {
  return (
    <div className="forgot-password-section">
      <div className="forgot-password-header">
        <h3>Reset Password</h3>
        <p>Enter New Password</p>
      </div>
      <div className="form-container forgot-password-form">
        <div className="reset-password-input-field">
          <label>New Password</label>
          <NoLabelPasswordInput name="password" value={resetPasswordProps.password} label="Password" customClass="cl-login-password-field" placeholder="***********" onChange={resetPasswordProps.onChange}/>
        </div>
        <div className="reset-password-input-field">
          <label>Confirm New Password</label>
          <NoLabelPasswordInput name="confirm_password" value={resetPasswordProps.confirm_password} label="Password" customClass="cl-login-password-field" placeholder="***********" onChange={resetPasswordProps.onChange}/>
        </div>
        <Submit id="resetPasswordButton" value="Reset Password" customClass="cl-login-btn-container" onSubmit={resetPasswordProps.onSubmit}/>
      </div>
    </div>
  );
};
ResetPasswordView.propTypes = {
  resetPasswordProps: PropTypes.object
};

export default ResetPasswordView;
