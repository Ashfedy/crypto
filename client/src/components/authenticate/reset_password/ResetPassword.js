import React from 'react';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import ResetPasswordView from './ResetPasswordView';
import alertify from 'alertify.js';
import * as authenticateActions from '../../../actions/authenticateActions';

class ResetPassword extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      user: {
        password: "",
        confirm_password: ""
      }
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    let url = props.location.pathname.split('_');
    let url_expireToken = url[url.length-1];
    const currentTime = Date.now();
    if(url_expireToken < currentTime) {
      props.history.push("/");
      alertify.error("Reset password link is expired");
    }
  }

  componentDidMount() {
    $('.navbar-nav li').removeClass('activeLink');
    $('#cryptoLink').addClass('unauthenticated');
  }

  onSubmit() {
    const { user } = this.state;
    const url = this.props.location.pathname.split('/');
    const url_uid_expired = url[url.length-1];
    const url_uid = url_uid_expired.split('_')[0]
    if(user.password != '' && user.confirm_password != '') {
      if(user.password === user.confirm_password) {
        this.props.actions.reset_password(url_uid, user.password);
      }
      else {
        alertify.error("New Passwords do not match.")
      }
    }
    else {
      alertify.error("Password fields cannot be empty");
    }
  }

  onChange(e) {
    const { value, name } = e.target;
    const { user } = this.state;
    user[name] = value;
    this.setState({ user });
    if(user.password == '' || user.confirm_password == '') {
      $('#resetPasswordButton').removeClass('pristine');
    }
    else {
      $('#resetPasswordButton').addClass('pristine');
    }
  }

  render() {
    const { user: { password, confirm_password } } = this.state;
    return (
      <div className="forgot-password-wrapper">
        <div className="forgot-password-container">
          <ResetPasswordView password={password} confirm_password={confirm_password} onChange={this.onChange} onSubmit={this.onSubmit}/>
        </div>
      </div>
    );
  }
}

ResetPassword.propTypes = {
  actions: PropTypes.object.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  })
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(authenticateActions, dispatch)
  };
}

export default connect(null, mapDispatchToProps)(ResetPassword);
