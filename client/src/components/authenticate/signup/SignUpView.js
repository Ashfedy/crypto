import React from 'react';
import PropTypes from "prop-types";
import NoLabelTextInput from '../../common/input/NoLabelTextInput';
import NoLabelPasswordInput from '../../common/input/NoLabelPasswordInput';
import Text from '../../common/input/Text';
import Password from '../../common/input/Password';
import Submit from '../../common/input/Submit';
import {Link} from 'react-router-dom';

const SignUpView = (signUpProps) => {
  return (
    <div className="signup-section">
      <div className="signup-section-header">
        <h3>Sign Up <span>to Wallet Address</span></h3>
        <p>Already have an account? <Link to="/login">Sign in here</Link></p>
      </div>
      <div className="form-container signup-form">
      <div className="signup-input-field">
        <label>Username</label>
        <NoLabelTextInput id="signupUsername" name="username" value={signUpProps.username} label="Username" customClass="cl-login-text-field" placeholder="johndoe" onChange={signUpProps.onChange}/>
      </div>
      <div className="signup-input-field">
        <label>Full Name</label>
        <NoLabelTextInput id="signupFullname" name="fullname" value={signUpProps.fullname} customClass="cl-login-text-field" placeholder="John Doe" onChange={signUpProps.onChange}/>
      </div>
      <div className="signup-input-field">
        <label>Email</label>
        <NoLabelTextInput id="signupEmail" name="email" value={signUpProps.email} label="Email" customClass="cl-login-email-field" placeholder="johndoe@example.com" onChange={signUpProps.onChange}/>
      </div>
      <div className="signup-input-field">
        <label>Password</label>
        <NoLabelPasswordInput id="signupPassword" name="password" value={signUpProps.password} label="Password" customClass="cl-login-password-field" placeholder="**********" onChange={signUpProps.onChange}/>
      </div>
        <Submit id="signupButton" value="Sign Up to Wallet Address" customClass="cl-signup-btn-container signup-btn" onSubmit={signUpProps.onSubmit}/>
      </div>
      <div className="signup-error-section">
        <p className="signup-common-error display-none">**ALL FIELDS ARE MANDATORY**</p>
        <p className="signup-email-error display-none">**EMAIL IS INVALID**</p>
        <p className="signup-other-error display-none">**USERNAME ALREADY EXISTS**</p>
      </div>
    </div>
  );
};
SignUpView.propTypes = {
  signUpProps: PropTypes.object
};

export default SignUpView;
