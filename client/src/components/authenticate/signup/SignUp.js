import React from 'react';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import SignUpView from './SignUpView';
import * as authenticateActions from '../../../actions/authenticateActions';

class SignUp extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      user: {
        username: "",
        password: "",
        fullname: "",
        email: ""
      }
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    $('.navbar-nav li').removeClass('activeLink');
    $('#cryptolink').addClass('unauthenticated');
  }

  validateEmail(email) {
    const email_re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return email_re.test(email.toLowerCase());
  }

  onSubmit() {
    const { user } = this.state;
    const isValidEmail = this.validateEmail(user.email.toLowerCase());
    if((user.email == "" || undefined) || (user.password == "" || undefined) || (user.username == "" || undefined) || (user.fullname == "" || undefined)) {
      $('.signup-common-error').removeClass('display-none');
      $('.signup-common-error').addClass('shake');
      setTimeout(function(){
        $('.signup-common-error').removeClass('shake');
      }, 2000);
    }
    else {
      if(!isValidEmail) {
        $('.signup-email-error').removeClass('display-none');
        $('.signup-email-error').addClass('shake');
        setTimeout(function(){
          $('.signup-email-error').removeClass('shake');
        }, 2000);
      }
      else {
        $('.signup-common-error').addClass('display-none');
        $('.signup-email-error').addClass('display-none');
        this.props.actions.signup(user.email.toLowerCase(), user.password, user.username.toLowerCase(), user.fullname).then(() => {
          if(this.props.isAuthenticated) {
            this.props.history.push("/dashboard");
          }
          else this.props.history.push("/signup");
        });
      }
    }
}

  onChange(e) {
    const { value, name } = e.target;
    const { user } = this.state;
    user[name] = value;
    this.setState({ user });
    if((user.email == "" || undefined) || (user.password == "" || undefined) || (user.username == "" || undefined) || (user.fullname == "" || undefined)) {
      $('#signupButton').removeClass('pristine');
    }
    else {
      $('#signupButton').addClass('pristine');
    }
  }

  render() {
      const { user: { username, password, fullname, email } } = this.state;
      return (
        <div className="signup-wrapper">
          <div className="signup-container container">
            <SignUpView email={email} fullname={fullname} username={username} password={password} onChange={this.onChange} onSubmit={this.onSubmit}/>
          </div>
        </div>
      );
  }
}

SignUp.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  actions: PropTypes.object.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  })
};

function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.user.token
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(authenticateActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
