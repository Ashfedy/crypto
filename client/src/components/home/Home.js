import React from 'react';
import FirstFold from './FirstFold';
import {ChangeHeaderLinkBackground} from '../common/header/Header.js';

class Home extends React.Component {
  constructor(props, context) {
    super(props, context);

    // this.state = {}; /*Local State for this component*/
    // this.action_name = this.action_name.bind(this); /*Bind actions in the constructor*/
  }

  componentDidMount() {
    $('.navbar-nav li').removeClass('activeLink');
    $('#cryptoLink').addClass('unauthenticated');
  }

  render() {
    return (
       <div className="homepage-wrapper">
        <FirstFold/>
       </div>
    );
  }
}

export default Home;
