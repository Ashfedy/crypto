import React from 'react';
import {Link} from 'react-router-dom';

const FirstFold = () => {
  return (
    <div className="first-fold">
      <div className="container-fluid">
        <div className="homepage-bg">
          <div className="circle-bg-container">
            <svg id="homepageBgCircle">
              <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g>
                  <g>
                    <g stroke="#56EDB5">
                      <g transform="translate(665.000000, -356.000000)">
                        <circle cx="706" cy="706" r="379"></circle>
                        <circle opacity="0.88745471" cx="706.5" cy="705.5" r="455.5"></circle>
                        <circle opacity="0.642379982" cx="706" cy="706" r="527"></circle>
                        <circle opacity="0.420006793" cx="706.5" cy="705.5" r="610.5"></circle>
                        <circle opacity="0.271569293" cx="706" cy="706" r="706"></circle>
                      </g>
                    </g>
                  </g>
                </g>
              </g>
            </svg>
          </div>
        </div>
        <div className="homepage-content">
          <div className="row">
            <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div className="homepage-firstfold-left">
                <h1>Crypto Public Addresses Sharing Made Easy</h1>
                <p>Signup, Save and Share all your cryptocurrency public addresses with just <span className="green">One address</span> to your friends and family.</p>
                <Link className="get-started-link" to="/signup">Get Started</Link>
              </div>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div className="homepage-secondfold-left">
                <img src="https://s3.ap-south-1.amazonaws.com/cryptolink/essentials/macbook.png"/>
              </div>
            </div>
          </div>
        </div>
        <div className="wave-bg-container">
<svg id="homepageBgWave">
  <defs>
      <linearGradient x1="0%" y1="100%" x2="100%" y2="0%" id="linearGradient-3">
          <stop stopColor="#00F198" offset="0%"></stop>
          <stop stopColor="#A0EACE" offset="100%"></stop>
      </linearGradient>
  </defs>
  <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g>
          <path d="M0,680.806033 C115.136169,577.602011 237.037587,550.333333 365.704254,599 C490.760929,646.301229 616.132495,622.751959 741.818952,528.352189 L741.818952,528.352189 C748.554313,523.293438 754.623506,517.403937 759.88234,510.823609 C863.086266,381.685525 986.026904,347.410989 1128.70425,408 C1274.70425,470 1387.37092,433.666667 1466.70425,299 C1496.70425,257 1527.37092,257 1558.70425,299 C1555.37092,653 1553.70425,830 1553.70425,830 L50.7042536,830 L0,680.806033 Z" id="Path-3" fill="url(#linearGradient-3)" mask="url(#mask-2)"></path>
          <path d="M50.7042536,685.57408 C145.37092,612.524693 257.037587,600.333333 385.704254,649 C510.760929,696.301229 636.132495,672.751959 761.818952,578.352189 L761.818952,578.352189 C768.554313,573.293438 774.623506,567.403937 779.88234,560.823609 C883.086266,431.685525 1006.0269,397.410989 1148.70425,458 C1294.70425,520 1407.37092,483.666667 1486.70425,349 C1516.70425,307 1547.37092,307 1578.70425,349 C1575.37092,703 1573.70425,880 1573.70425,880 C1312.03354,880 1115.78051,880 984.94515,880 C781.780507,880 477.033541,880 70.7042536,880 L60.2905262,778.765074 L50.7042536,685.57408 Z" id="Path-3" fill="#FFFFFF" opacity="0.156193388" mask="url(#mask-2)"></path>
      </g>
  </g>
</svg>
        </div>
      </div>
    </div>
  );
};
//  Leave a comment or Whatsapp at <a href="tel:07338876655">07338876655</a> to place your orders and for enquiries.

export default FirstFold;
