import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router';

const PrivacyPolicy = ({/*name*/}) => {
  return (
    <div className="privacy-policy-wrapper">
      <div className="privacy-policy-container container">
        <div className="privacy-policy-section">
          <h1>Privacy Policy</h1>
          <p>By accessing or using Wallet Address, you agree to the terms of this Online Privacy Policy, as outlined below. If you do not agree to these terms, please do not access or use this site.</p>
        </div>
        <div className="collect-info-section">
          <h1>Collection of Personal Information</h1>
          <p>When you engage in certain activities on this site, you may be asked to provide certain information about yourself by filling out and submitting an online form. It is completely optional for you to engage in these activities. If you elect to engage in these activities, however, you may be required to provide personal information, such as your name, e-mail address, wallet address(Public key) and other personal identifying information.</p>
          <p>When you submit personal information, you understand and agree that Wallet Address may transfer, store, and process your information in any of the countries in which Wallet Address maintain offices, including without limitations.</p>
          <p>Wallet Address collects this information in order to record and support your participation in the activities you select. Wallet Address also uses information that you provide as part of our effort to keep you informed about upgrades and other services. Wallet Address will never sell or share your information with other companies.</p>
        </div>
      </div>
    </div>
  );
};
PrivacyPolicy.propTypes = {
  // name: PropTypes.string.isRequired
};

export default PrivacyPolicy;
