import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router';

const TermsOfUse = ({/*name*/}) => {
  return (
    <div className="terms-of-use-wrapper">
      <div className="terms-of-use-container container">Terms Of Use</div>
    </div>
  );
};
TermsOfUse.propTypes = {
  // name: PropTypes.string.isRequired
};

export default TermsOfUse;
