import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router-dom';

const FooterBottom = ({copyAddress}) => {
  return (
    <div className="footer-bottom">
      <div className="row">
        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <div className="sec-1">&copy; WalletAddress.io, 2018</div>
        </div>
        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <div className="sec-2">Made with <img src="https://s3.ap-south-1.amazonaws.com/cryptolink/essentials/like.png"/> by <Link className="green" to="https://twitter.com/ashfedy">Ashwin</Link> and <Link className="green" to="https://twitter.com/interpid16">Harish</Link></div>
        </div>
        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <ul className="support-us-section sec-3">
            <li>Buy us a beer <span className="footer-beer-mug">&#x1F37B;</span></li>
            <li><img data-type="Bitcoin" data-address="1D5tovU1kX6qfVa5H9awAPGAuZ9grsrXZs" className="footer-support-bitcoin" onClick={copyAddress} src="https://s3.ap-south-1.amazonaws.com/cryptolink/currency/Bitcoin.png"/></li>
            <li><img data-type="Ethereum" data-address="0x00da786c59F7BAb3a90BbD36DeC7729D8f729738" className="footer-support-ethereum" onClick={copyAddress} src="https://s3.ap-south-1.amazonaws.com/cryptolink/currency/Ethereum.png"/></li>
            <li><img data-type="Litecoin" data-address="La22JdX5XCkKzXqL6RuPidzNEsF35KA53j" className="footer-support-litecoin" onClick={copyAddress} src="https://s3.ap-south-1.amazonaws.com/cryptolink/currency/Litecoin.png"/></li>
          </ul>
        </div>
      </div>
    </div>
  );
};
FooterBottom.propTypes = {
  copyAddress: PropTypes.func.isRequired
};

export default FooterBottom;
