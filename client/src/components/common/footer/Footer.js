import React from 'react';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import FooterLeft from './FooterLeft';
import FooterMiddle from './FooterMiddle';
import FooterRight from './FooterRight';
import FooterBottom from './FooterBottom';
import alertify from 'alertify.js';

class Footer extends React.Component {
  constructor(props, context) {
    super(props, context);

    // this.state = {}; /*Local State for this component*/
    // this.action_name = this.action_name.bind(this); /*Bind actions in the constructor*/
    this.copyAddress = this.copyAddress.bind(this);
  }

  copyAddress(e) {
    const _this = e.currentTarget;
    let address = $(_this).attr("data-address");
    let type = $(_this).attr("data-type");
    $("#copyText").val(address);
    const copyText = document.getElementById("copyText");
    copyText.select();
    document.execCommand("Copy");
    alertify.logPosition("bottom right");
    alertify.log("Thank you for copying our "+type.toUpperCase()+" address");
    // alert("Copied the text: " + copyText.value);
  }

  render() {
    return (
      <div className="footer-wrapper">
        <div className="footer-section">
          <div className="row">
            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <FooterLeft/>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              {<FooterMiddle/>}
            </div>
            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <FooterRight/>
            </div>
          </div>
          <FooterBottom copyAddress={this.copyAddress}/>
            <div>
              <input name="copyText" id="copyText" type="text" value=""/>
              <div id="uid" data-uid={this.props.uid}></div>
            </div>
        </div>
      </div>
    );
  }
}

Footer.propTypes = {
  uid: PropTypes.string,
  isAuthenticated: PropTypes.bool
};

function mapStateToProps(state) {
  return {
    uid: state.user.uid,
    isAuthenticated: !!state.auth.token
  };
}

export default connect(mapStateToProps)(Footer);
