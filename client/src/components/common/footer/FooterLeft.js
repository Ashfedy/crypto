import React from 'react';
import {Link} from 'react-router-dom';
const FooterLeft = () => {
  return (
    <div className="footer-left">
      <div className="footer-left-name"><Link to="/"><span className="footer-left-name-1">Wallet </span><span className="footer-left-name-2">Address</span><span className="footer-left-name-3">.io</span></Link></div>
    </div>
  );
};
FooterLeft.propTypes = {
  // name: PropTypes.string.isRequired
};

export default FooterLeft;
