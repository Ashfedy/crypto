import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router-dom';

const FooterMiddle = ({/*name*/}) => {
  return (
    <div className="footer-middle">
      <div className="footer-links">
        <div className="row">
          <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <ul>
              <li><Link to="/">About us</Link></li>
              <li><Link to="/privacy-policy">Privacy Policy</Link></li>
              <li><Link to="/terms-of-use">Terms of use</Link></li>
            </ul>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <ul>
              <li><Link to="/faq">FAQ</Link></li>
              <li><Link to="/user">Search User</Link></li>
              <li><a href="mailto:walletaddressio@gmail.com?Subject=I%20would%20like%20to%20get%20more%20information&body=Username:">Help</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};
FooterMiddle.propTypes = {
  // name: PropTypes.string.isRequired
};

export default FooterMiddle;
