import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router-dom';

const FooterRight = () => {
  return (
    <div className="footer-right">
      <div className="footer-links">
        <div className="contact-us-section">
        <h4>Connect with us</h4>
          <ul className="contact-us-icons">
            <li><Link className="footer-contact-twitter" to="https://twitter.com/walletaddress" target="_blank"><img src="https://s3.ap-south-1.amazonaws.com/cryptolink/essentials/Facebook-black.svg"/></Link></li>
            <li><Link className="footer-contact-mail" to="mailto:walletaddressio@gmail.com?Subject=I%20would%20like%20to%20get%20more%20information&body=Username:"><img src="https://s3.ap-south-1.amazonaws.com/cryptolink/essentials/Medium-black.svg"/></Link></li>
            <li><Link className="footer-contact-twitter" to="https://twitter.com/walletaddress" target="_blank"><img src="https://s3.ap-south-1.amazonaws.com/cryptolink/essentials/Twitter-black.svg"/></Link></li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default FooterRight;
