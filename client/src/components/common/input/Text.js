import React from 'react';
import PropTypes from "prop-types";

const Text = ({id, name, value, label, customClass, onChange, placeholder}) => {
  return (
    <div className={customClass+" text-field form-group"}>
      <label className="form-group-label">{label}</label>
      <input id={id} type="text" placeholder={placeholder} name={name} value={value} onChange={onChange}/>
    </div>
  );
};
Text.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  customClass: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string
};

export default Text;
