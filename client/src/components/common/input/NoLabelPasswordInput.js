import React from 'react';
import PropTypes from "prop-types";

const NoLabelPasswordInput = ({id, name, value, customClass, onChange, placeholder}) => {
  return (
    <div className={customClass+" text-field form-group"}>
      <input id={id} type="password" placeholder={placeholder} name={name} value={value} onChange={onChange}/>
    </div>
  );
};
NoLabelPasswordInput.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  customClass: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string
};

export default NoLabelPasswordInput;
