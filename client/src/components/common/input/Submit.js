import React from 'react';
import PropTypes from "prop-types";

const Submit = ({value, onSubmit, customClass, id}) => {
  return (
    <div className="form-group">
      <button id={id} className={customClass+" form-group-submit-btn btn"} onClick={onSubmit}>{value}</button>
    </div>
  );
};
Submit.propTypes = {
  value: PropTypes.string.isRequired,
  customClass: PropTypes.string,
  id: PropTypes.string,
  onSubmit: PropTypes.func.isRequired
};

export default Submit;
