import React from 'react';
import PropTypes from "prop-types";

const Password = ({name, value, label, customClass, onChange, placeholder}) => {
  return (
    <div className={customClass+" password-field form-group"}>
      <label className="form-group-label">{label.toUpperCase()}</label>
      <input type="password" name={name} value={value} placeholder={placeholder} onChange={onChange}/>
    </div>
  );
};
Password.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  customClass: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string
};

export default Password;
