import React from 'react';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import HeaderBrand from './HeaderBrand';
import HeaderLeftLink from './HeaderLeftLink';
import HeaderRightLink from './HeaderRightLink';
import Loader from '../loader/Loader';
import * as authenticateActions from '../../../actions/authenticateActions';

class Header extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {loading: 0, count: 0};
    this.activateBackground = this.activateBackground.bind(this); /*Bind actions in the constructor*/
    this.activateHomeBackground = this.activateHomeBackground.bind(this);
    this.logout = this.logout.bind(this);

    // Closes navigation dropdown on mobile devices
    $(function(){
      let navMain = $(".navbar-collapse");
      navMain.on("click", "a:not([data-toggle])", null, function () {
        navMain.collapse('hide');
      });
    });
  }

  activateBackground(event) {
    const _this = event.target;
    $(_this).parents('.navbar-nav').find('li').removeClass('active');
    $(_this).parent().addClass('active');
  }

  activateHomeBackground() {
    $('.navbar-nav').find('li').removeClass('active');
    $('.home-link').addClass('active');
  }

  logout() {
    this.props.actions.logout();
  }

  render() {
    return (
      <div className="crypto-header">
      {this.props.loading>0 && <Loader/>}
        <header>
            <nav className="navbar navbar-default">
              <div className="container-fluid">
                <HeaderBrand activateHomeBackground={this.activateHomeBackground}/>
                <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  {<HeaderRightLink isAuthenticated={this.props.isAuthenticated} logout={this.logout} username={this.props.username}/>}
                </div>
              </div>
            </nav>
        </header>

      </div>
    );
  }
}

Header.contextTypes = {
  router: PropTypes.object
  // this.context.router.push('/courses');
};

Header.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  loading: PropTypes.number,
  username: PropTypes.string,
  actions: PropTypes.object.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func
  })
  // activateHomeBackground: PropTypes.func.isRequired,
  // activateBackground: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.auth.token,
    loading: state.loading,
    username: state.user.username
  };
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(authenticateActions, dispatch)
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(Header);
