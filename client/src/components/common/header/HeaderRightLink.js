import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router-dom';

const HeaderRightLink = ({isAuthenticated, logout, username}) => {
  return (
    <ul className="nav navbar-nav navbar-right">
    {!isAuthenticated && <li className="authentication-links" id="signupHeaderLink"><Link to="/signup">Sign Up</Link></li>}
    {!isAuthenticated && <li className="authentication-links" id="loginHeaderLink"><Link to="/login">Sign In</Link></li>}
    {isAuthenticated && <li id="addressHeaderLink"><Link to={"/user/"+username} target="_blank">Address</Link></li>}
    {isAuthenticated && <li id="profileHeaderLink"><Link to="/profile">Profile</Link></li>}
    {isAuthenticated && <li id="dashboardHeaderLink"><Link to="/dashboard">Dashboard</Link></li>}
    {isAuthenticated && <li id="signoutHeaderLink"><Link to="/" onClick={logout}>Sign Out</Link></li>}
    </ul>
  );
};
HeaderRightLink.propTypes = {
  username: PropTypes.string,
  logout: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired
};

export default HeaderRightLink;
