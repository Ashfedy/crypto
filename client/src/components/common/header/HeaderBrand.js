import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router-dom';

const HeaderBrand = ({activateHomeBackground}) => {
  return (
    <div className="navbar-header">
      <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span className="sr-only">Toggle navigation</span>
        <span className="icon-bar"></span>
        <span className="icon-bar"></span>
        <span className="icon-bar"></span>
      </button>
      <Link to="/" className="navbar-brand" onClick={activateHomeBackground}><img src="https://s3.ap-south-1.amazonaws.com/cryptolink/essentials/logo.png"/></Link>
    </div>
  );
};
HeaderBrand.propTypes = {
  activateHomeBackground: PropTypes.func.isRequired
};

export default HeaderBrand;
// <Link to="/" className="navbar-brand"><span className="navbar-brand-1">Wallet</span><span className="navbar-brand-2">Address</span><span className="navbar-brand-3">.io</span></Link>
