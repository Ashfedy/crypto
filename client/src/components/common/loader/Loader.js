import React from 'react';
import Loading from 'react-loading';

const Loader = () => {
  return (
    <div id="loaderWrapper">
      <div className="loader-container">
        <Loading type={"spinningBubbles"} color={"#22BAA5"} height={200} width={200}/>
      </div>
    </div>
  );
};
Loader.propTypes = {
  // name: PropTypes.string.isRequired
};

export default Loader;
/*****LOADER TYPES******

blank
balls
bars
bubbles
cubes
cylon
spin
spinningBubbles
spokes

*****/
