import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router';
import './PublicLoader.css';


const PublicLoader = () => {
  return (
    <div className="public-loader-wrapper timeline-wrapper">
      <div className="public-loader-container">
        <div className="timeline-item">
            <div className="animated-background">
                <div className="background-masker header-top"></div>
                <div className="background-masker header-left"></div>
                <div className="background-masker header-right"></div>
                <div className="background-masker header-bottom"></div>
                <div className="background-masker subheader-left"></div>
                <div className="background-masker subheader-right"></div>
                <div className="background-masker subheader-bottom"></div>
                <div className="background-masker content-top"></div>
            </div>
            <div className="animated-background">
                <div className="background-masker header-top"></div>
                <div className="background-masker header-left"></div>
                <div className="background-masker header-right"></div>
                <div className="background-masker header-bottom"></div>
                <div className="background-masker subheader-left"></div>
                <div className="background-masker subheader-right"></div>
                <div className="background-masker subheader-bottom"></div>
                <div className="background-masker content-top"></div>
            </div>
            <div className="animated-background">
                <div className="background-masker header-top"></div>
                <div className="background-masker header-left"></div>
                <div className="background-masker header-right"></div>
                <div className="background-masker header-bottom"></div>
                <div className="background-masker subheader-left"></div>
                <div className="background-masker subheader-right"></div>
                <div className="background-masker subheader-bottom"></div>
                <div className="background-masker content-top"></div>
            </div>
            <div className="animated-background">
                <div className="background-masker header-top"></div>
                <div className="background-masker header-left"></div>
                <div className="background-masker header-right"></div>
                <div className="background-masker header-bottom"></div>
                <div className="background-masker subheader-left"></div>
                <div className="background-masker subheader-right"></div>
                <div className="background-masker subheader-bottom"></div>
                <div className="background-masker content-top"></div>
            </div>
        </div>
      </div>
    </div>
  );
};
PublicLoader.propTypes = {
  // name: PropTypes.string.isRequired
};

export default PublicLoader;
