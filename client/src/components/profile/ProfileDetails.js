import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router';
import NoLabelTextInput from '../common/input/NoLabelTextInput';
import NoLabelPasswordInput from '../common/input/NoLabelPasswordInput';
import Password from '../common/input/Password';

const ProfileDetails = ({user, color, password, onChange, updateUser, choseProfieStyle}) => {
  return (
    <div className="main-profile-details-container">
      <div className="main-profile-details-section">
        <div className="profile-input-section profile-element">
          <div className="row form-container">
            <div className="col-lg-6 col-md-6 col-sm-6 col-sx-12">
              <div className="profile-input">
                <label className="form-group-label profile-input-label">Name</label>
                <NoLabelTextInput name="full_name" placeholder="John Doe" value={user.full_name} onChange={onChange}/>
              </div>
              <div className="profile-input">
                <label className="form-group-label profile-input-label">Email ID</label>&nbsp; &nbsp;<span id="emailExistError" className="red display-none"> EMAIL ALREADY EXISTS</span>
                <NoLabelTextInput name="email" placeholder="johndoe@example.com" value={user.email} onChange={onChange}/>
                <p id="emailInvalidError" className="red display-none">INVALID EMAIL</p>
              </div>
              <div className="profile-input">
                <label className="form-group-label profile-input-label">Password</label>
                <NoLabelPasswordInput name="password" placeholder="***********" value={password} onChange={onChange}/>
              </div>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-6 col-sx-12">
              <div className="profile-color-container">
              <p className="profile-color-section-header">Profile Style</p>
                <div className="profile-color-section" data-style={color.profile_color}>
                  <div className={color.profile_color=="blue-violet" ? "active profile-color blue-violet":"profile-color blue-violet"} data-color="blue-violet" onClick={choseProfieStyle}></div>
                  <div className={color.profile_color=="pink-red" ? "active profile-color pink-red":"profile-color pink-red"} data-color="pink-red" onClick={choseProfieStyle}></div>
                  <div className={color.profile_color=="yellow-orange" ? "active profile-color yellow-orange":"profile-color yellow-orange"} data-color="yellow-orange" onClick={choseProfieStyle}></div>
                  <div className={color.profile_color=="yellow-pink" ? "active profile-color yellow-pink":"profile-color yellow-pink"} data-color="yellow-pink" onClick={choseProfieStyle}></div>
                </div>
              </div>
            </div>
          </div>
          <p id="fieldsEmptyError" className="red display-none">NAME or EMAIL field cannot be empty</p>
          <button id="profileDetailsSubmitBtn" className="form-group-submit-btn btn btn-success disabled" onClick={updateUser}>Update</button>
        </div>
      </div>
    </div>
  );
};
ProfileDetails.propTypes = {
  user: PropTypes.object.isRequired,
  color: PropTypes.object.isRequired,
  password: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  updateUser: PropTypes.func.isRequired,
  choseProfieStyle: PropTypes.func.isRequired
};

export default ProfileDetails;
