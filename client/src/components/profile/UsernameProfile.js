import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router';
import Text from '../common/input/Text';
// https://cryptolink.s3.amazonaws.com/5a3a199713249be5483d8e6b_1513998972692.png
const UsernameDashboard = ({username, userImage, onChange, updateUsername, fileUpload}) => {
  return (
    <div className="username-container">
      <div className="username-section">
      <h3 className="profile-section-title">Profile Settings</h3>
        <div className="row">
          <div className="col-lg-1 col-md-2 col-sm-2 col-xs-2 username-section-image">
            <img src={userImage ? userImage : "https://s3.ap-south-1.amazonaws.com/cryptolink/essentials/placeholder_dp.png"}/>
            <div className="dp-upload">
              <p className="dp-upload-label"><span className="glyphicon glyphicon-camera"></span><br/>Upload</p>
              <input type="file" id="dpUpload" name="user_dp" onChange={fileUpload}/>
            </div>
          </div>
          <div className="col-lg-9 col-md-9 col-sm-9 col-xs-9 username-section-title">
          <p>Change Username</p><span id="usernameExistError" className="red display-none">USERNAME NOT AVAILABLE</span> <span id="usernameEmptyError" className="red display-none">USERNAME cannot be empty</span>
          <div className="row form-container">
            <div className="username-input-section profile-element">
              <div className="col-lg-7 col-md-9 col-sm-9 col-xs-12">
                <Text name="username" value={username} label="walletaddress.io/user/" placeholder="eg., johndoe" onChange={onChange}/>
              </div>
              <div className="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                <button id="usernameSubmitBtn" className="form-group-submit-btn" className="btn btn-success disabled" onClick={updateUsername}>Update Username</button>
              </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  );
};
UsernameDashboard.propTypes = {
  username: PropTypes.string.isRequired,
  userImage: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  updateUsername: PropTypes.func.isRequired,
  fileUpload: PropTypes.func.isRequired
};

export default UsernameDashboard;
