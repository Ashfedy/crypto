import React from 'react';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as userActions from '../../actions/userActions';
import {Link} from 'react-router-dom';
import Username from './UsernameProfile';
import ProfileDetails from './ProfileDetails';


class Profile extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      user: {
        username: "",
        full_name: "",
        email: "",
        password: ""
      }
    };
    this.onChange = this.onChange.bind(this);
    this.updateUsername = this.updateUsername.bind(this);
    this.updateUser = this.updateUser.bind(this);
    this.fileUpload = this.fileUpload.bind(this);
    this.choseProfieStyle = this.choseProfieStyle.bind(this);
    this.ChangeHeaderLinkBackground = this.ChangeHeaderLinkBackground.bind(this);
  }

  ChangeHeaderLinkBackground(header_link_id) {
    $('.navbar-nav li').removeClass('activeLink');
    $('#'+header_link_id).addClass('activeLink');
  }

  componentWillMount() {
    this.setState(
      {"user":
        {
          "username": this.props.user.username || this.state.user.username,
          "full_name": this.props.user.full_name || this.state.user.full_name,
          "email": this.props.user.email || this.state.user.email,
          "password": ""
        }
      }
    );
  }

  componentWillReceiveProps(nextProps) {
    this.setState(
      {"user":
        {
          "username": nextProps.user.username,
          "full_name": nextProps.user.full_name,
          "email": nextProps.user.email,
          "password": ""
        }
      }
    );
  }

  validateEmail(email) {
    const email_re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return email_re.test(email.toLowerCase());
  }

  fileUpload(e) {
    // alert("New file uploaded");
    const uid = document.getElementById('uid').getAttribute("data-uid");
    const _this = e.currentTarget;
    const file = _this.files[0];
    // console.log(_this.files[0]);
    this.props.actions.uploadDp(file, uid);
  }

  onChange(e) {
    const _this = e.currentTarget;
    const { value, name } = e.target;
    const { user } = this.state;
    user[name] = value;
    this.setState({ user });
    if($(_this).val() != ''||null||undefined) {
      $(_this).parents('.profile-element').find('button').removeClass('disabled');
    }
    else {
      $(_this).parents('.profile-element').find('button').addClass('disabled');
    }
  }

  updateUsername() {
    const { user } = this.state;
    const uid = document.getElementById('uid').getAttribute("data-uid");
    if(user.username == "" || undefined) {
      $('#usernameEmptyError').removeClass('display-none');
      $('#usernameEmptyError').addClass('shake');
    }
    else {
      $('#usernameEmptyError').addClass('display-none');
      $('#usernameEmptyError').removeClass('shake');
      this.props.actions.updateUsername(user.username.toLowerCase(), uid);
    }
  }

  updateUser() {
    const { user } = this.state;
    const uid = document.getElementById('uid').getAttribute("data-uid");
    const profile_color = $('.profile-color-section').attr('data-style');
    const isValidEmail = this.validateEmail(user.email.toLowerCase());
    if((user.email == "" || undefined) || (user.full_name == "" || undefined)) {
      $('#fieldsEmptyError').removeClass('display-none');
    }
    else {
      if(!isValidEmail) {
        $('#emailInvalidError').removeClass('display-none');
      }
      else {
        $('#fieldsEmptyError').addClass('display-none');
        $('#emailInvalidError').addClass('display-none');
        if(this.props.user.email == user.email) {
          this.props.actions.updateUser({full_name: user.full_name, password: user.password, profile_color: profile_color}, uid);
        }
        else {
          this.props.actions.updateUser({full_name: user.full_name, email: user.email, password: user.password, profile_color: profile_color}, uid);
        }
      }
    }
  }

  choseProfieStyle(e) {
    const _this = e.currentTarget;
    $(_this).parents('.profile-color-section').find('.profile-color').removeClass('active');
    $(_this).addClass('active');
    let color = $(_this).attr('data-color');
    $(_this).parents('.profile-color-section').attr('data-style',color);
    $('#profileDetailsSubmitBtn').removeClass('disabled');
  }

  render() {
    return (
      <div className="profile-wrapper">
        <div className="container profile-container">
          <div className="row">
            <div className="col-lg-2 col-md-2 display-none-sm navigation-tabs-section">
            <div className="navigation-tabs dashboard-tab"><Link to="/dashboard">Dashboard</Link></div>
            <div className="navigation-tabs profile-tab active"><Link to="/profile">Profile</Link></div>
            </div>
            <div className="col-lg-10 col-md-10 col-sm-12 col-xs-12 profile-section">
              <Username username={this.state.user.username} userImage={this.props.user.user_image} onChange={this.onChange} updateUsername={this.updateUsername} fileUpload={this.fileUpload}/>
              <hr/>
              <ProfileDetails user={this.state.user} color={this.props.user} password={this.state.user.password} onChange={this.onChange} updateUser={this.updateUser} choseProfieStyle={this.choseProfieStyle}/>
            </div>
          </div>
        </div><span>{this.ChangeHeaderLinkBackground("profileHeaderLink")}</span>
      </div>
    );
  }
}

Profile.propTypes = {
  user: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    user: state.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(userActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
