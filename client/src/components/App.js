import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Switch, Route } from "react-router-dom";
import Header from './common/header/Header';
import Footer from './common/footer/Footer';
import Home from './home/Home';
import Login from './authenticate/login/Login';
import SignUp from './authenticate/signup/SignUp';
import ForgotPassword from './authenticate/forgot_password/ForgotPassword';
import ResetPassword from './authenticate/reset_password/ResetPassword';
import Profile from './profile/Profile';
import Dashboard from './dashboard/Dashboard';
import PublicProfile from './public_profile/PublicProfile';
import SearchUser from './public_profile/SearchUser';
import PrivacyPolicy from './common/page/PrivacyPolicy';
import TermsOfUse from './common/page/TermsOfUse';
import NotFound from './home/NotFound';
import UserRoute from "./routes/UserRoute";
import GuestRoute from "./routes/GuestRoute";

const App = ({ location, isAuthenticated }) => {
  return (
  <div className="crypto-wrapper">
    <Header/>
    <Switch>
      <GuestRoute location={location} path="/" exact component={Home} />
      <GuestRoute location={location} path="/home" exact component={Home} />
      <Route location={location} path="/user/:url_username" exact component={PublicProfile} />
      <Route location={location} path="/user" exact component={SearchUser} />
      <GuestRoute location={location} path="/login" exact component={Login} />
      <GuestRoute location={location} path="/signup" exact component={SignUp} />
      <UserRoute location={location} path="/dashboard" exact component={Dashboard} />
      <UserRoute location={location} path="/profile" exact component={Profile} />
      <GuestRoute location={location} path="/forgot-password" exact component={ForgotPassword} />
      <GuestRoute location={location} path="/reset-password/:url_uid" component={ResetPassword} />
      <Route location={location} path="/privacy-policy" exact component={PrivacyPolicy}/>
      <Route location={location} path="/terms-of-use" exact component={TermsOfUse}/>
      <Route component={NotFound}/>
    </Switch>
    <Footer/>
  </div>
);
};

App.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired,
  isAuthenticated: PropTypes.bool.isRequired
};

function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.auth.token
  };
}

export default connect(mapStateToProps)(App);
