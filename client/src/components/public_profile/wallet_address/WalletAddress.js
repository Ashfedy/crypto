import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router';
import * as data from '../../../config/crypto_data';

const WalletAddress = ({walletType, walletName, walletAddress, copyAddress, generateQR}) => {
  const maskedAddress = ({walletAddress}) => {
    return walletAddress.substring(0,5)+"....."+walletAddress.substring(walletAddress.length - 5);
  }
  return (
    <div className="row public-wallet-row">
      <div className="col-lg-1 col-md-1 col-sm-1 col-xs-2 public-wallet-address-img" data-type={walletType}><img src={data.CRYPTO_DATA.filter(obj => obj.value == walletType)[0].img}/></div>
      <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4"><p className="public-wallet-address-name">{walletType}</p><p className="public-wallet-address-type">{walletName}</p></div>
      <div className="col-lg-6 col-md-6 col-sm-6 col-xs-4 public-wallet-address display-none-xs" data-address={walletAddress}>{walletAddress}</div>
      <div className="col-lg-6 col-md-6 col-sm-6 col-xs-4 public-wallet-address display-xs" data-address={walletAddress}>{maskedAddress({walletAddress})}</div>
      <div className="col-lg-1 col-md-1 col-sm-1 col-xs-2 public-wallet-address-copy-qrcode"><img onClick={copyAddress} className="public-wallet-address-copy" src="https://s3.ap-south-1.amazonaws.com/cryptolink/essentials/copy.png"/>&nbsp;<img onClick={generateQR} className="public-wallet-address-qrcode" src="https://s3.ap-south-1.amazonaws.com/cryptolink/essentials/qr_code.png"/></div>
    </div>
  );
};
WalletAddress.propTypes = {
  walletType: PropTypes.string.isRequired,
  walletName: PropTypes.string.isRequired,
  walletAddress: PropTypes.string.isRequired,
  copyAddress: PropTypes.func.isRequired,
  generateQR: PropTypes.func.isRequired
};

export default WalletAddress;
