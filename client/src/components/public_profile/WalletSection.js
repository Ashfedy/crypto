import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router';
import WalletAddress from './wallet_address/WalletAddress';
import PublicLoader from '../common/loader/PublicLoader';

const WalletSection = ({wallet, copyAddress, generateQR, public_loading}) => {
  return (
    <div className="public-wallet-section">
      <div className="public-wallet-title text-center">PUBLIC WALLET ADDRESSES</div>
      <hr className="public-wallet-section-divider"/>
      <div className="public-wallet-address-section">
      {public_loading>0&&<PublicLoader/>}
      {wallet.map(wallet =>
        <WalletAddress key={wallet.wid} walletType={wallet.type} walletName={wallet.name} walletAddress={wallet.address} copyAddress={copyAddress} generateQR={generateQR}/>
      )}
      <input name="copyText" id="copyText" type="text" value=""/>
      </div>
      <div className="modal fade" id="QRModal" tabIndex="-1" role="dialog" aria-labelledby="QR Modal">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 className="modal-title">QR Code</h4>
            </div>
            <div className="modal-body">
              <div className="qr-image-container">
                <img id="qrcodeHolder" src="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
WalletSection.propTypes = {
  wallet: PropTypes.array.isRequired,
  copyAddress: PropTypes.func.isRequired,
  generateQR: PropTypes.func.isRequired,
  public_loading: PropTypes.number
};

export default WalletSection;
