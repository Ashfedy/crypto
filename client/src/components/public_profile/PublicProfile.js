import React from 'react';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import * as searchUserActions from '../../actions/searchUserActions';
import {bindActionCreators} from 'redux';
import UserSection from './UserSection';
import WalletSection from './WalletSection';
import alertify from 'alertify.js';
import QRCode from 'qrcode';

class PublicProfile extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      user: {},
      wallet: []
    }
    this.copyAddress = this.copyAddress.bind(this);
    this.generateQR = this.generateQR.bind(this);
    props.actions.searchUsername(props.match.params.url_username.toLowerCase());
  }

  componentDidMount() {
    $('.navbar-nav li').removeClass('activeLink');
    $('#cryptoLink').addClass('unauthenticated');
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps.searched_user);
    $('.navbar-nav li').removeClass('activeLink');
    $('#addressHeaderLink').addClass('activeLink');
  }

  copyAddress(e) {
    const _this = e.currentTarget;
    let address = $(_this).parents('.public-wallet-row').find('.public-wallet-address').attr("data-address");
    let type = $(_this).parents('.public-wallet-row').find('.public-wallet-address-img').attr("data-type");
    $("#copyText").val(address);
    const copyText = document.getElementById("copyText");
    copyText.select();
    document.execCommand("Copy");
    alertify.logPosition("bottom right");
    alertify.log(type+" address copied!");
  }

  generateQR(e) {
    const _this = e.currentTarget;
    let address = $(_this).parents('.public-wallet-row').find('.public-wallet-address').attr("data-address");
    let type = $(_this).parents('.public-wallet-row').find('.public-wallet-address-img').attr("data-type");

    QRCode.toDataURL(address,
      {
        color: {
          dark: '#000',
          light: '#0000' // Transparent background
        }
      },
      function (err, url) {
        $("#qrcodeHolder").attr('src',url)
        $('#QRModal').modal('show');
    });
  }

  render() {
    return (
      <div className="public-profile-wrapper">
        <div className="container public-profile-container">
          <div className="public-user-wallet-section">
            <UserSection user={this.state.user}/>
            <WalletSection wallet={this.state.wallet} copyAddress={this.copyAddress} generateQR={this.generateQR} public_loading={this.props.public_loading}/>
          </div>
        </div>
      </div>
    );
  }
}

PublicProfile.propTypes = {
  searched_user: PropTypes.object,
  actions: PropTypes.object.isRequired,
  public_loading: PropTypes.number
};

function mapStateToProps(state, ownProps) {
  return {
    searched_user: state.user_search,
    public_loading: state.public_loading
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(searchUserActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PublicProfile);
