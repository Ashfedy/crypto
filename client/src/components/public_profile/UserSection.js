import React from 'react';
import PropTypes from "prop-types";
import {Link} from 'react-router';

const UserSection = ({user}) => {
  return (
    <div className={user.profile_color+" public-user-section"}>
      <div className="public-user-img-section">
        <img src={user.user_image ? user.user_image : "https://s3.ap-south-1.amazonaws.com/cryptolink/essentials/placeholder_public_dp.png"}/>
      </div>
      <p className="public-user-fullname text-center">{user.full_name?user.full_name:"No User Found"}</p>
      <p className="public-user-username text-center">{user.username?"walletaddress.io/user/"+user.username:""}</p>
    </div>
  );
};
UserSection.propTypes = {
  user: PropTypes.object.isRequired
};

export default UserSection;
