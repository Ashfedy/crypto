import React from 'react';
import {Link} from 'react-router-dom';
import NoLabelTextInput from '../common/input/NoLabelTextInput';

class SearchUser extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {username: ""};
    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    const _this = e.currentTarget;
    const value = e.target.value;
    this.setState({ "username": value });
  }

  render() {
    return (
      <div className="search-user-wrapper">
        <div className="search-user-container container">
          <div className="search-user-section">
            <label className="form-group-label">Please Enter Username</label>
            <NoLabelTextInput name="username" customClass="search-username-field" placeholder="ashfedy" value={this.state.username} onChange={this.onChange}/>
            <Link to={"/user/"+this.state.username} id="searchPublicProfile" className="form-group-submit-btn btn btn-success pristine">Search</Link>
          </div>
        </div>
      </div>
    );
  }
}

SearchUser.propTypes = {
};

export default SearchUser;
