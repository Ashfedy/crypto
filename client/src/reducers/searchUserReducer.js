import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function userReducer(state = initialState.user_search, action) {
  switch (action.type) {
    case types.EMPTY_USERNAME_SEARCH:
    {
      return Object.assign({}, ...state, {});
    }
    case types.SEARCH_USERNAME_RECEIVED:
    {
      return Object.assign({},...state, action.userDetails);
    }
    default:
    return state;
  }
}
