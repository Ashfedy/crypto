import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function userReducer(state = initialState.user, action) {
  switch (action.type) {
    case types.LOGIN_SUCCESS:
      return action.userDetails;

    case types.SIGNUP_SUCCESS:
      return action.userDetails;

    case types.LOGOUT_USER_SUCCESS:
      return {};

    case types.UPDATE_USERNAME_SUCCESS:
    {
      return Object.assign({}, ...state, action.userDetails);
    }

    case types.UPDATE_USER_SUCCESS:
    {
      return Object.assign({},...state, action.userDetails);
    }

    default:
    return state;
  }
}
