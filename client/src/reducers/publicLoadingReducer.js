import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function loading(state = initialState.public_loading, action) {
  if (action.type == types.BEGIN_PUBLIC_LOADING) {
    return state + 1;
  } else if (action.type == types.TERMINATE_PUBLIC_LOADING) {
    return state - 1;
  }
  return state;
}
