import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function dashboardReducer(state = initialState.wallet_list, action) {
  switch (action.type) {

    case types.LOAD_WALLET_LIST_SUCCESS:
    return action.walletList;

    case types.ADD_WALLET_SUCCESS:
    return [
      ...state,
      Object.assign({}, action.wallet)
    ];

    case types.LOGOUT_WALLET_SUCCESS:
      return [];

    case types.UPDATE_WALLET_SUCCESS:
    {
      const object_index = state.findIndex(wallet => {
        return wallet.wid == action.modifiedWallet.wid;
      });
      const newState = [...state];
      newState.splice(object_index,1, Object.assign({},action.modifiedWallet));
      return newState;
    }

    case types.DELETE_WALLET_SUCCESS:
    {
      return [
        ...state.filter(wallet => wallet.wid !== action.wid)
      ];
    }

    default:
    return state;
  }
}
