import {combineReducers} from "redux";
import auth from './authReducer';
import user from './userReducer';
import wallet from './walletReducer';
import loading from './loadingReducer';
import public_loading from './publicLoadingReducer';
import user_search from './searchUserReducer';
const rootReducer = combineReducers({
  auth,
  user,
  wallet,
  loading,
  public_loading,
  user_search
});

export default rootReducer;
