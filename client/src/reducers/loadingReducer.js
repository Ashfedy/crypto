import * as types from '../actions/actionTypes';
import initialState from './initialState';

function actionTypeEndsInSuccess(type) {
  return type.substring(type.length - 8) == '_SUCCESS';
}

export default function loading(state = initialState.loading, action) {
  if (action.type == types.BEGIN_LOADING) {
    return state + 1;
  } else if (action.type == types.TERMINATE_LOADING || actionTypeEndsInSuccess(action.type)) {
    return state - 1;
  }
  return state;
}
