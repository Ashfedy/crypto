import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function authReducer(state = initialState.auth, action) {
  switch (action.type) {
    case types.AUTHENTICATION_SUCCESS:
    {
      return Object.assign({}, ...state, {"token":action.token});
    }
    case types.LOGOUT_SUCCESS:
    {
      return {};
    }
    default:
    return state;
  }
}
