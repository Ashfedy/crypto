export const CRYPTO_DATA = [
    {
      "img": "https://www.cryptocompare.com/media/12318415/42.png",
      "value": "42 Coin",
      "label": "42 Coin (42)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352070/365.png",
      "value": "365Coin",
      "label": "365Coin (365)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351001/404.png",
      "value": "404Coin",
      "label": "404Coin (404)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350985/611.png",
      "value": "SixEleven",
      "label": "SixEleven (611)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351513/808.png",
      "value": "808",
      "label": "808 (808)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351639/888.png",
      "value": "Octocoin",
      "label": "Octocoin (888)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350976/1337.png",
      "value": "1337",
      "label": "1337 (1337)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20180/2015.png",
      "value": "2015 coin",
      "label": "2015 coin (2015)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19633/btc.png",
      "value": "Bitcoin",
      "label": "Bitcoin (BTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19782/litecoin-logo.png",
      "value": "Litecoin",
      "label": "Litecoin (LTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19969/xmr.png",
      "value": "Monero",
      "label": "Monero (XMR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20627/nxt.png",
      "value": "Nxt",
      "label": "Nxt (NXT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19684/doge.png",
      "value": "Dogecoin",
      "label": "Dogecoin (DOGE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351360/zec.png",
      "value": "ZCash",
      "label": "ZCash (ZEC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20705/bts.png",
      "value": "Bitshares",
      "label": "Bitshares (BTS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19972/ripple.png",
      "value": "Ripple",
      "label": "Ripple (XRP)"
    },
    {
      "img": "https://s3.ap-south-1.amazonaws.com/cryptolink/currency/Dash.png",
      "value": "Dash",
      "label": "Dash (DASH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19864/peercoin-logo.png",
      "value": "PeerCoin",
      "label": "PeerCoin (PPC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20022/craig.png",
      "value": "CraigsCoin",
      "label": "CraigsCoin (CRAIG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351060/xbs_1.png",
      "value": "Bitstake",
      "label": "Bitstake (XBS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20076/xpy_1.png",
      "value": "PayCoin",
      "label": "PayCoin (XPY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20393/prc.png",
      "value": "ProsperCoin",
      "label": "ProsperCoin (PRC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19975/ybc.png",
      "value": "YbCoin",
      "label": "YbCoin (YBC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20247/dank.png",
      "value": "DarkKush",
      "label": "DarkKush (DANK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20297/give.png",
      "value": "GiveCoin",
      "label": "GiveCoin (GIVE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20329/kobo.png",
      "value": "KoboCoin",
      "label": "KoboCoin (KOBO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20031/dt.png",
      "value": "DarkToken",
      "label": "DarkToken (DT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20228/ceti.png",
      "value": "CETUS Coin",
      "label": "CETUS Coin (CETI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20442/sup.png",
      "value": "Supcoin",
      "label": "Supcoin (SUP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20162/xpd.png",
      "value": "PetroDollar",
      "label": "PetroDollar (XPD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20292/geo.png",
      "value": "GeoCoin",
      "label": "GeoCoin (GEO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20231/chash.png",
      "value": "CleverHash",
      "label": "CleverHash (CHASH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20438/spr.png",
      "value": "Spreadcoin",
      "label": "Spreadcoin (SPR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20376/nxti.png",
      "value": "NXTI",
      "label": "NXTI (NXTI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20559/wolf.png",
      "value": "Insanity Coin",
      "label": "Insanity Coin (WOLF)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20560/xdp.png",
      "value": "DogeParty",
      "label": "DogeParty (XDP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19593/ac.png",
      "value": "Asia Coin",
      "label": "Asia Coin (AC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20079/acoin.png",
      "value": "ACoin",
      "label": "ACoin (ACOIN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19594/aero.png",
      "value": "Aero Coin",
      "label": "Aero Coin (AERO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19600/alf.png",
      "value": "AlphaCoin",
      "label": "AlphaCoin (ALF)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19595/ags.png",
      "value": "Aegis",
      "label": "Aegis (AGS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19601/amc.png",
      "value": "AmericanCoin",
      "label": "AmericanCoin (AMC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20080/aln.png",
      "value": "AlienCoin",
      "label": "AlienCoin (ALN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19599/apex.png",
      "value": "ApexCoin",
      "label": "ApexCoin (APEX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20085/arch.png",
      "value": "ArchCoin",
      "label": "ArchCoin (ARCH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19602/arg.png",
      "value": "Argentum",
      "label": "Argentum (ARG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20082/ari.png",
      "value": "AriCoin",
      "label": "AriCoin (ARI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19608/aur.png",
      "value": "Aurora Coin",
      "label": "Aurora Coin (AUR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20086/axr.png",
      "value": "AXRON",
      "label": "AXRON (AXR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19620/bcx.png",
      "value": "BattleCoin",
      "label": "BattleCoin (BCX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19617/ben.png",
      "value": "Benjamins",
      "label": "Benjamins (BEN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19621/bet.png",
      "value": "BetaCoin",
      "label": "BetaCoin (BET)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350879/bitb.png",
      "value": "BitBean",
      "label": "BitBean (BITB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19624/blu.png",
      "value": "BlueCoin",
      "label": "BlueCoin (BLU)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351795/blk.png",
      "value": "BlackCoin",
      "label": "BlackCoin (BLK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19626/bost.png",
      "value": "BoostCoin",
      "label": "BoostCoin (BOST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19631/bqc.png",
      "value": "BQCoin",
      "label": "BQCoin (BQC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19815/myr.png",
      "value": "MyriadCoin",
      "label": "MyriadCoin (XMY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19802/moon.png",
      "value": "MoonCoin",
      "label": "MoonCoin (MOON)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19993/zet.png",
      "value": "ZetaCoin",
      "label": "ZetaCoin (ZET)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19924/sxc.png",
      "value": "SexCoin",
      "label": "SexCoin (SXC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19879/qtl.png",
      "value": "Quatloo",
      "label": "Quatloo (QTL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19697/enrg.png",
      "value": "EnergyCoin",
      "label": "EnergyCoin (ENRG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19882/qrk.png",
      "value": "QuarkCoin",
      "label": "QuarkCoin (QRK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19888/ric.jpg",
      "value": "Riecoin",
      "label": "Riecoin (RIC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19676/dgc.png",
      "value": "DigiCoin",
      "label": "DigiCoin (DGC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19769/limx.png",
      "value": "LimeCoinX",
      "label": "LimeCoinX (LIMX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20083/bitb.png",
      "value": "BitBar",
      "label": "BitBar (BTB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20226/caix.png",
      "value": "CAIx",
      "label": "CAIx (CAIx)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19632/bte.png",
      "value": "ByteCoin",
      "label": "ByteCoin (BTE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19634/btg.png",
      "value": "BitGem",
      "label": "BitGem (BTG*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20084/btm.png",
      "value": "BitMark",
      "label": "BitMark (BTM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19637/buk.png",
      "value": "CryptoBuk",
      "label": "CryptoBuk (BUK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19642/cach.png",
      "value": "Cachecoin",
      "label": "Cachecoin (CACH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20015/cann.png",
      "value": "CannabisCoin",
      "label": "CannabisCoin (CANN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20017/cap.png",
      "value": "BottleCaps",
      "label": "BottleCaps (CAP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20016/cash.png",
      "value": "CashCoin",
      "label": "CashCoin (CASH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19644/cat.png",
      "value": "Catcoin",
      "label": "Catcoin (CAT1)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20697/cbx.png",
      "value": "CryptoBullion",
      "label": "CryptoBullion (CBX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19643/ccn.png",
      "value": "CannaCoin",
      "label": "CannaCoin (CCN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20698/cinder.png",
      "value": "CinderCoin",
      "label": "CinderCoin (CIN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19651/cinni.jpeg",
      "value": "CINNICOIN",
      "label": "CINNICOIN (CINNI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20246/cxc.png",
      "value": "CheckCoin",
      "label": "CheckCoin (CXC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20020/clam.png",
      "value": "CLAMS",
      "label": "CLAMS (CLAM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19657/clr.png",
      "value": "CopperLark",
      "label": "CopperLark (CLR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20019/cmc.png",
      "value": "CosmosCoin",
      "label": "CosmosCoin (CMC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20021/cnc.png",
      "value": "ChinaCoin",
      "label": "ChinaCoin (CNC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20024/cnl.png",
      "value": "ConcealCoin",
      "label": "ConcealCoin (CNL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19661/comm.png",
      "value": "Community Coin",
      "label": "Community Coin (COMM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19658/cool.png",
      "value": "CoolCoin",
      "label": "CoolCoin (COOL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20023/crack.png",
      "value": "CrackCoin",
      "label": "CrackCoin (CRACK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19665/crc.png",
      "value": "CraftCoin",
      "label": "CraftCoin (CRC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19664/crypt.png",
      "value": "CryptCoin",
      "label": "CryptCoin (CRYPT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19667/csc.png",
      "value": "CasinoCoin",
      "label": "CasinoCoin (CSC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318032/xvg.png",
      "value": "Verge",
      "label": "Verge (XVG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20027/drkc.png",
      "value": "DarkCash",
      "label": "DarkCash (DRKC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20034/dsb.png",
      "value": "DarkShibe",
      "label": "DarkShibe (DSB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20563/dvc.png",
      "value": "DevCoin",
      "label": "DevCoin (DVC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19690/eac.png",
      "value": "EarthCoin",
      "label": "EarthCoin (EAC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19692/efl.png",
      "value": "E-Gulden",
      "label": "E-Gulden (EFL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19694/elc.png",
      "value": "Elacoin",
      "label": "Elacoin (ELC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20033/emc2.png",
      "value": "Einsteinium",
      "label": "Einsteinium (EMC2)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20278/emd.png",
      "value": "Emerald",
      "label": "Emerald (EMD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20035/excl.png",
      "value": "Exclusive Coin",
      "label": "Exclusive Coin (EXCL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19700/exe.png",
      "value": "ExeCoin",
      "label": "ExeCoin (EXE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19702/ezc.png",
      "value": "EZCoin",
      "label": "EZCoin (EZC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20032/flap.png",
      "value": "Flappy Coin",
      "label": "Flappy Coin (FLAP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19719/fuel.png",
      "value": "Fuel2Coin",
      "label": "Fuel2Coin (FC2)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19706/ffc.png",
      "value": "FireflyCoin",
      "label": "FireflyCoin (FFC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20030/fibre.png",
      "value": "FIBRE",
      "label": "FIBRE (FIBRE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382629/frc.png",
      "value": "FireRoosterCoin",
      "label": "FireRoosterCoin (FRC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19709/flt.png",
      "value": "FlutterCoin",
      "label": "FlutterCoin (FLT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19712/frk.png",
      "value": "Franko",
      "label": "Franko (FRK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19710/frac.png",
      "value": "FractalCoin",
      "label": "FractalCoin (FRAC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19720/fst.png",
      "value": "FastCoin",
      "label": "FastCoin (FST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19718/ftc.png",
      "value": "FeatherCoin",
      "label": "FeatherCoin (FTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20054/gdc.png",
      "value": "GrandCoin",
      "label": "GrandCoin (GDC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19724/glc.png",
      "value": "GlobalCoin",
      "label": "GlobalCoin (GLC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19728/glx.png",
      "value": "GalaxyCoin",
      "label": "GalaxyCoin (GLX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19725/glyph.png",
      "value": "GlyphCoin",
      "label": "GlyphCoin (GLYPH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19726/gml.png",
      "value": "GameLeagueCoin",
      "label": "GameLeagueCoin (GML)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19732/gue.png",
      "value": "GuerillaCoin",
      "label": "GuerillaCoin (GUE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20036/hal.png",
      "value": "Halcyon",
      "label": "Halcyon (HAL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19735/hbn.png",
      "value": "HoboNickels",
      "label": "HoboNickels (HBN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20037/hun.png",
      "value": "HunterCoin",
      "label": "HunterCoin (HUC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19745/hvc.png",
      "value": "HeavyCoin",
      "label": "HeavyCoin (HVC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20624/hyp.png",
      "value": "Hyperstake",
      "label": "Hyperstake (HYP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19747/icb.png",
      "value": "IceBergCoin",
      "label": "IceBergCoin (ICB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19754/ifc.png",
      "value": "Infinite Coin",
      "label": "Infinite Coin (IFC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20042/ioc.png",
      "value": "IOCoin",
      "label": "IOCoin (IOC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19761/ixc.png",
      "value": "IXcoin",
      "label": "IXcoin (IXC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20044/jbs.png",
      "value": "JumBucks Coin",
      "label": "JumBucks Coin (JBS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19757/jkc.png",
      "value": "JunkCoin",
      "label": "JunkCoin (JKC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20038/judge.png",
      "value": "JudgeCoin",
      "label": "JudgeCoin (JUDGE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19766/kdc.png",
      "value": "Klondike Coin",
      "label": "Klondike Coin (KDC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19763/kgc.png",
      "value": "KrugerCoin",
      "label": "KrugerCoin (KGC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20040/lab.png",
      "value": "CoinWorksCoin",
      "label": "CoinWorksCoin (LAB*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19770/lgd.png",
      "value": "Legendary Coin",
      "label": "Legendary Coin (LGD*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19776/lk7.png",
      "value": "Lucky7Coin",
      "label": "Lucky7Coin (LK7)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19774/lky.png",
      "value": "LuckyCoin",
      "label": "LuckyCoin (LKY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20041/lsd.png",
      "value": "LightSpeedCoin",
      "label": "LightSpeedCoin (LSD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20336/ltb.png",
      "value": "Litebar ",
      "label": "Litebar  (LTB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20043/ltcd.png",
      "value": "LitecoinDark",
      "label": "LitecoinDark (LTCD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19779/ltcx.png",
      "value": "LitecoinX",
      "label": "LitecoinX (LTCX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20045/lxc.png",
      "value": "LibrexCoin",
      "label": "LibrexCoin (LXC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19785/lyc.png",
      "value": "LycanCoin",
      "label": "LycanCoin (LYC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19786/max.png",
      "value": "MaxCoin",
      "label": "MaxCoin (MAX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19789/mec.png",
      "value": "MegaCoin",
      "label": "MegaCoin (MEC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20046/med.png",
      "value": "MediterraneanCoin",
      "label": "MediterraneanCoin (MED)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19793/min.png",
      "value": "Minerals Coin",
      "label": "Minerals Coin (MIN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19797/mint.png",
      "value": "MintCoin",
      "label": "MintCoin (MINT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19796/mn1.png",
      "value": "Cryptsy Mining Contract",
      "label": "Cryptsy Mining Contract (MN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19805/mincoin.png",
      "value": "MinCoin",
      "label": "MinCoin (MNC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19807/mry.jpg",
      "value": "MurrayCoin",
      "label": "MurrayCoin (MRY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20053/myst.png",
      "value": "MysteryCoin",
      "label": "MysteryCoin (MYST*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19816/mzc.png",
      "value": "MazaCoin",
      "label": "MazaCoin (MZC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19821/nan.png",
      "value": "NanoToken",
      "label": "NanoToken (NAN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19822/naut.png",
      "value": "Nautilus Coin",
      "label": "Nautilus Coin (NAUT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351431/nav.png",
      "value": "NavCoin",
      "label": "NavCoin (NAV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19825/nbl.png",
      "value": "Nybble",
      "label": "Nybble (NBL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19824/nec.png",
      "value": "NeoCoin",
      "label": "NeoCoin (NEC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19826/net.png",
      "value": "NetCoin",
      "label": "NetCoin (NET)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20049/nmb.png",
      "value": "Nimbus Coin",
      "label": "Nimbus Coin (NMB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19839/nrb.png",
      "value": "NoirBits",
      "label": "NoirBits (NRB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19833/nobl.png",
      "value": "NobleCoin",
      "label": "NobleCoin (NOBL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19834/nrs.png",
      "value": "NoirShares",
      "label": "NoirShares (NRS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20713/nvc.png",
      "value": "NovaCoin",
      "label": "NovaCoin (NVC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19830/nmc.png",
      "value": "NameCoin",
      "label": "NameCoin (NMC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19842/nyan.png",
      "value": "NyanCoin",
      "label": "NyanCoin (NYAN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20050/opal.png",
      "value": "OpalCoin",
      "label": "OpalCoin (OPAL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19845/orb.png",
      "value": "Orbitcoin",
      "label": "Orbitcoin (ORB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19847/osc.png",
      "value": "OpenSourceCoin",
      "label": "OpenSourceCoin (OSC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19857/phs.png",
      "value": "PhilosophersStone",
      "label": "PhilosophersStone (PHS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19863/points.png",
      "value": "Cryptsy Points",
      "label": "Cryptsy Points (POINTS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19865/pot.png",
      "value": "PotCoin",
      "label": "PotCoin (POT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20052/pseud.png",
      "value": "PseudoCash",
      "label": "PseudoCash (PSEUD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19869/pts.png",
      "value": "Protoshares",
      "label": "Protoshares (PTS*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20058/pxc.png",
      "value": "PhoenixCoin",
      "label": "PhoenixCoin (PXC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19878/pyc.png",
      "value": "PayCoin",
      "label": "PayCoin (PYC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19887/rdd.png",
      "value": "ReddCoin",
      "label": "ReddCoin (RDD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20051/ripo.png",
      "value": "RipOffCoin",
      "label": "RipOffCoin (RIPO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19895/rpc-2.png",
      "value": "RonPaulCoin",
      "label": "RonPaulCoin (RPC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19896/rt2.png",
      "value": "RotoCoin",
      "label": "RotoCoin (RT2)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19898/ryc.png",
      "value": "RoyalCoin",
      "label": "RoyalCoin (RYC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20055/rzr.png",
      "value": "RazorCoin",
      "label": "RazorCoin (RZR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19897/sat2.png",
      "value": "Saturn2Coin",
      "label": "Saturn2Coin (SAT2)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19900/sbc.png",
      "value": "StableCoin",
      "label": "StableCoin (SBC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20419/sdc.png",
      "value": "ShadowCash",
      "label": "ShadowCash (SDC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19903/sfr.png",
      "value": "SaffronCoin",
      "label": "SaffronCoin (SFR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20056/shade.png",
      "value": "ShadeCoin",
      "label": "ShadeCoin (SHADE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19904/shld.png",
      "value": "ShieldCoin",
      "label": "ShieldCoin (SHLD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20057/silk.png",
      "value": "SilkCoin",
      "label": "SilkCoin (SILK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20428/slg.png",
      "value": "SterlingCoin",
      "label": "SterlingCoin (SLG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20059/smc.png",
      "value": "SmartCoin",
      "label": "SmartCoin (SMC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20431/sole.png",
      "value": "SoleCoin",
      "label": "SoleCoin (SOLE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19911/spa.png",
      "value": "SpainCoin",
      "label": "SpainCoin (SPA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19917/spt.png",
      "value": "Spots",
      "label": "Spots (SPT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19918/src.png",
      "value": "SecureCoin",
      "label": "SecureCoin (SRC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20060/ssv.png",
      "value": "SSVCoin",
      "label": "SSVCoin (SSV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20696/str.png",
      "value": "Stellar",
      "label": "Stellar (XLM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20061/super.png",
      "value": "SuperCoin",
      "label": "SuperCoin (SUPER)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20446/swift.png",
      "value": "BitSwift",
      "label": "BitSwift (SWIFT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19922/sync.png",
      "value": "SyncCoin",
      "label": "SyncCoin (SYNC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792583/sys.png",
      "value": "SysCoin",
      "label": "SysCoin (SYS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19925/tag.png",
      "value": "TagCoin",
      "label": "TagCoin (TAG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19928/tak.png",
      "value": "TakCoin",
      "label": "TakCoin (TAK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19927/tes.png",
      "value": "TeslaCoin",
      "label": "TeslaCoin (TES)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19930/tgc.png",
      "value": "TigerCoin",
      "label": "TigerCoin (TGC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20069/tit.png",
      "value": "TitCoin",
      "label": "TitCoin (TIT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19934/tor.png",
      "value": "TorCoin",
      "label": "TorCoin (TOR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19938/terracoin.png",
      "value": "TerraCoin",
      "label": "TerraCoin (TRC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20064/ttc.png",
      "value": "TittieCoin",
      "label": "TittieCoin (TTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20063/ultc.png",
      "value": "Umbrella",
      "label": "Umbrella (ULTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19940/unb.png",
      "value": "UnbreakableCoin",
      "label": "UnbreakableCoin (UNB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20065/uno.png",
      "value": "Unobtanium",
      "label": "Unobtanium (UNO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20465/usde.png",
      "value": "UnitaryStatus Dollar",
      "label": "UnitaryStatus Dollar (USDE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19942/utc.png",
      "value": "UltraCoin",
      "label": "UltraCoin (UTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20067/util.png",
      "value": "Utility Coin",
      "label": "Utility Coin (UTIL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20066/vdo.png",
      "value": "VidioCoin",
      "label": "VidioCoin (VDO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20070/via.png",
      "value": "ViaCoin",
      "label": "ViaCoin (VIA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19946/voot.png",
      "value": "VootCoin",
      "label": "VootCoin (VOOT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20068/vrc.png",
      "value": "VeriCoin",
      "label": "VeriCoin (VRC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19948/wc.png",
      "value": "WhiteCoin",
      "label": "WhiteCoin (WC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19949/wdc.png",
      "value": "WorldCoin",
      "label": "WorldCoin (WDC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20071/xai.png",
      "value": "SapienceCoin",
      "label": "SapienceCoin (XAI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20073/xbot.png",
      "value": "SocialXbotCoin",
      "label": "SocialXbotCoin (XBOT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19956/xc.png",
      "value": "X11 Coin",
      "label": "X11 Coin (XC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20075/xcash.png",
      "value": "Xcash",
      "label": "Xcash (XCASH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19710/frac.png",
      "value": "Crypti",
      "label": "Crypti (XCR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19962/xjo.png",
      "value": "JouleCoin",
      "label": "JouleCoin (XJO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19966/xlb.png",
      "value": "LibertyCoin",
      "label": "LibertyCoin (XLB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19970/xpm.png",
      "value": "PrimeCoin",
      "label": "PrimeCoin (XPM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350617/xxx.png",
      "value": "XXXCoin",
      "label": "XXXCoin (XXX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19976/yac.png",
      "value": "YAcCoin",
      "label": "YAcCoin (YAC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19979/zcc.png",
      "value": "ZCC Coin",
      "label": "ZCC Coin (ZCC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19981/zed.png",
      "value": "ZedCoins",
      "label": "ZedCoins (ZED)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20078/zrc.png",
      "value": "ZiftrCoin",
      "label": "ZiftrCoin (ZRC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318404/bcn.png",
      "value": "ByteCoin",
      "label": "ByteCoin (BCN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20270/ekn.png",
      "value": "Elektron",
      "label": "Elektron (EKN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19957/xdn.jpg",
      "value": "DigitalNote ",
      "label": "DigitalNote  (XDN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20479/xau.png",
      "value": "XauCoin",
      "label": "XauCoin (XAU)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20451/tmc.png",
      "value": "TimesCoin",
      "label": "TimesCoin (TMC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20490/xem.png",
      "value": "NEM",
      "label": "NEM (XEM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746623/burst.png",
      "value": "BurstCoin",
      "label": "BurstCoin (BURST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20363/nbt.png",
      "value": "NuBits",
      "label": "NuBits (NBT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20422/sjcx.png",
      "value": "StorjCoin",
      "label": "StorjCoin (SJCX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19916/start.png",
      "value": "StartCoin",
      "label": "StartCoin (START)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20318/huge.png",
      "value": "BigCoin",
      "label": "BigCoin (HUGE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19960/xcp.png",
      "value": "CounterParty",
      "label": "CounterParty (XCP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352247/maid.png",
      "value": "MaidSafe Coin",
      "label": "MaidSafe Coin (MAID)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350595/007.png",
      "value": "007 coin",
      "label": "007 coin (007)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20378/nsr.png",
      "value": "NuShares",
      "label": "NuShares (NSR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19801/mona.png",
      "value": "MonaCoin",
      "label": "MonaCoin (MONA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20227/cell.png",
      "value": "SolarFarm",
      "label": "SolarFarm (CELL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19929/tek.png",
      "value": "TekCoin",
      "label": "TekCoin (TEK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20459/tron.png",
      "value": "Positron",
      "label": "Positron (TRON)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383137/bay1.png",
      "value": "BitBay",
      "label": "BitBay (BAY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318281/ntrn.png",
      "value": "Neutron",
      "label": "Neutron (NTRN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20425/sling.png",
      "value": "Sling Coin",
      "label": "Sling Coin (SLING)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350813/xvc.png",
      "value": "Vcash",
      "label": "Vcash (XVC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20242/crave.png",
      "value": "CraveCoin",
      "label": "CraveCoin (CRAVE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20204/block.png",
      "value": "BlockNet",
      "label": "BlockNet (BLOCK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20165/xsi.png",
      "value": "Stability Shares",
      "label": "Stability Shares (XSI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19565/cexio.png",
      "value": "Giga Hash",
      "label": "Giga Hash (GHS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20217/byc.png",
      "value": "ByteCent",
      "label": "ByteCent (BYC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20307/grc.png",
      "value": "GridCoin",
      "label": "GridCoin (GRC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19710/frac.png",
      "value": "Gemz Social",
      "label": "Gemz Social (GEMZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19771/ktk.png",
      "value": "KryptCoin",
      "label": "KryptCoin (KTK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20320/hz.png",
      "value": "Horizon",
      "label": "Horizon (HZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20287/fair.png",
      "value": "FairCoin",
      "label": "FairCoin (FAIR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19876/qora.png",
      "value": "QoraCoin",
      "label": "QoraCoin (QORA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351279/rby.png",
      "value": "RubyCoin",
      "label": "RubyCoin (RBY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19868/ptc.png",
      "value": "PesetaCoin",
      "label": "PesetaCoin (PTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14543972/kore.png",
      "value": "Kore",
      "label": "Kore (KORE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20477/wbb.png",
      "value": "Wild Beast Coin",
      "label": "Wild Beast Coin (WBB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20443/ssd.png",
      "value": "Sonic Screw Driver Coin",
      "label": "Sonic Screw Driver Coin (SSD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20167/xtc.png",
      "value": "TileCoin",
      "label": "TileCoin (XTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19832/note.png",
      "value": "Dnotes",
      "label": "Dnotes (NOTE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20313/grid.png",
      "value": "GridPay",
      "label": "GridPay (GRID*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383331/flo1.png",
      "value": "FlorinCoin",
      "label": "FlorinCoin (FLO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19798/mmxiv.png",
      "value": "MaieutiCoin",
      "label": "MaieutiCoin (MMXIV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20176/8bit.png",
      "value": "8BIT Coin",
      "label": "8BIT Coin (8BIT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20444/stv.png",
      "value": "Sativa Coin",
      "label": "Sativa Coin (STV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20267/ebs.png",
      "value": "EbolaShare",
      "label": "EbolaShare (EBS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20191/am.png",
      "value": "AeroMe",
      "label": "AeroMe (AM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20154/xmg.png",
      "value": "Coin Magi",
      "label": "Coin Magi (XMG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20187/amber.png",
      "value": "AmberCoin",
      "label": "AmberCoin (AMBER)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19759/jpc.png",
      "value": "JackPotCoin",
      "label": "JackPotCoin (JPC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20371/nkt.png",
      "value": "NakomotoDark",
      "label": "NakomotoDark (NKT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20350/j.png",
      "value": "JoinCoin",
      "label": "JoinCoin (J)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19721/ghc.png",
      "value": "GhostCoin",
      "label": "GhostCoin (GHC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19688/dtc.png",
      "value": "DayTrader Coin",
      "label": "DayTrader Coin (DTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383379/aby.png",
      "value": "ArtByte",
      "label": "ArtByte (ABY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20332/ldoge.png",
      "value": "LiteDoge",
      "label": "LiteDoge (LDOGE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19710/frac.png",
      "value": "MasterTraderCoin",
      "label": "MasterTraderCoin (MTR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350568/tri.png",
      "value": "Triangles Coin",
      "label": "Triangles Coin (TRI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20445/swarm.png",
      "value": "SwarmCoin",
      "label": "SwarmCoin (SWARM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19609/bbr.png",
      "value": "Boolberry",
      "label": "Boolberry (BBR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20210/btcry.png",
      "value": "BitCrystal",
      "label": "BitCrystal (BTCRY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20198/bcr.png",
      "value": "BitCredit",
      "label": "BitCredit (BCR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20158/xpb.png",
      "value": "Pebble Coin",
      "label": "Pebble Coin (XPB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19959/xdq.png",
      "value": "Dirac Coin",
      "label": "Dirac Coin (XDQ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20284/fldc.png",
      "value": "Folding Coin",
      "label": "Folding Coin (FLDC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20699/slr.png",
      "value": "SolarCoin",
      "label": "SolarCoin (SLR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20427/smac.png",
      "value": "Social Media Coin",
      "label": "Social Media Coin (SMAC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20460/trk.png",
      "value": "TruckCoin",
      "label": "TruckCoin (TRK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351629/u.jpg",
      "value": "Ucoin",
      "label": "Ucoin (U)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20455/uis.png",
      "value": "Unitus",
      "label": "Unitus (UIS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20248/cyp.png",
      "value": "CypherPunkCoin",
      "label": "CypherPunkCoin (CYP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318167/ufo1.png",
      "value": "UFO Coin",
      "label": "UFO Coin (UFO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20192/asn.png",
      "value": "Ascension Coin",
      "label": "Ascension Coin (ASN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19843/oc.png",
      "value": "OrangeCoin",
      "label": "OrangeCoin (OC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20316/gsm.png",
      "value": "GSM Coin",
      "label": "GSM Coin (GSM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19717/fsc.png",
      "value": "FriendshipCoin",
      "label": "FriendshipCoin (FSC2)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20379/nxtty.png",
      "value": "NXTTY",
      "label": "NXTTY (NXTTY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20400/qbk.png",
      "value": "QuBuck Coin",
      "label": "QuBuck Coin (QBK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19623/blc.png",
      "value": "BlakeCoin",
      "label": "BlakeCoin (BLC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20343/maryj.png",
      "value": "MaryJane Coin",
      "label": "MaryJane Coin (MARYJ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1381967/omc.png",
      "value": "OmniCron",
      "label": "OmniCron (OMC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20294/gig.png",
      "value": "GigCoin",
      "label": "GigCoin (GIG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20225/cc.png",
      "value": "CyberCoin",
      "label": "CyberCoin (CC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19622/bits.png",
      "value": "BitstarCoin",
      "label": "BitstarCoin (BITS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20336/ltb.png",
      "value": "LTBCoin",
      "label": "LTBCoin (LTBC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382788/neos1.png",
      "value": "NeosCoin",
      "label": "NeosCoin (NEOS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19744/hyper.png",
      "value": "HyperCoin",
      "label": "HyperCoin (HYPER)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20471/vtr.png",
      "value": "Vtorrent",
      "label": "Vtorrent (VTR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20359/metal.png",
      "value": "MetalCoin",
      "label": "MetalCoin (METAL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350588/pinkcoin-logo.png",
      "value": "PinkCoin",
      "label": "PinkCoin (PINK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382396/grn.png",
      "value": "GreenCoin",
      "label": "GreenCoin (GRE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20156/xg.png",
      "value": "XG Sports",
      "label": "XG Sports (XG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20233/child.png",
      "value": "ChildCoin",
      "label": "ChildCoin (CHILD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20208/boom.png",
      "value": "BOOM Coin",
      "label": "BOOM Coin (BOOM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20356/mine.png",
      "value": "Instamine Nuggets",
      "label": "Instamine Nuggets (MINE)"
    },
    {
      "value": "ROS Coin",
      "label": "ROS Coin (ROS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20456/unat.png",
      "value": "Unattanium",
      "label": "Unattanium (UNAT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20426/slm.png",
      "value": "SlimCoin",
      "label": "SlimCoin (SLM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20290/gaia.png",
      "value": "GAIA Platform",
      "label": "GAIA Platform (GAIA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19935/trust.png",
      "value": "TrustPlus",
      "label": "TrustPlus (TRUST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20282/fcn.png",
      "value": "FantomCoin ",
      "label": "FantomCoin  (FCN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20483/xcn.png",
      "value": "Cryptonite",
      "label": "Cryptonite (XCN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383812/cure.png",
      "value": "Curecoin",
      "label": "Curecoin (CURE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20299/gmc.png",
      "value": "Gridmaster",
      "label": "Gridmaster (GMC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19795/mmc.png",
      "value": "MemoryCoin",
      "label": "MemoryCoin (MMC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20488/xbc.png",
      "value": "BitcoinPlus",
      "label": "BitcoinPlus (XBC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19671/cyc.png",
      "value": "ConSpiracy Coin ",
      "label": "ConSpiracy Coin  (CYC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19983/888.png",
      "value": "OctoCoin",
      "label": "OctoCoin (OCTO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19814/mst.png",
      "value": "MasterCoin",
      "label": "MasterCoin (MSC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20269/egg.png",
      "value": "EggCoin",
      "label": "EggCoin (EGG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19640/c2.png",
      "value": "Coin.2",
      "label": "Coin.2 (C2)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20314/gsxjpeg.png",
      "value": "GlowShares",
      "label": "GlowShares (GSX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20220/cam.png",
      "value": "Camcoin",
      "label": "Camcoin (CAM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20408/rbr.png",
      "value": "Ribbit Rewards",
      "label": "Ribbit Rewards (RBR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318067/xqn.png",
      "value": "Quotient",
      "label": "Quotient (XQN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20319/icash.png",
      "value": "ICASH",
      "label": "ICASH (ICASH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20373/node.png",
      "value": "Node",
      "label": "Node (NODE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20436/soon.png",
      "value": "SoonCoin",
      "label": "SoonCoin (SOON)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20213/btmi.png",
      "value": "BitMiles",
      "label": "BitMiles (BTMI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20277/event.png",
      "value": "Event Token",
      "label": "Event Token (EVENT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20175/1cr.png",
      "value": "1Credit",
      "label": "1Credit (1CR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20469/viorjpeg.png",
      "value": "ViorCoin",
      "label": "ViorCoin (VIOR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20486/xco.png",
      "value": "XCoin",
      "label": "XCoin (XCO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19943/vmc.png",
      "value": "VirtualMining Coin",
      "label": "VirtualMining Coin (VMC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19808/mrs.png",
      "value": "MarsCoin",
      "label": "MarsCoin (MRS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20472/viral.png",
      "value": "Viral Coin",
      "label": "Viral Coin (VIRAL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19808/mrs.png",
      "value": "Equilibrium Coin",
      "label": "Equilibrium Coin (EQM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19808/mrs.png",
      "value": "IslaCoin",
      "label": "IslaCoin (ISL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20404/qslv.png",
      "value": "Quicksilver coin",
      "label": "Quicksilver coin (QSLV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19808/mrs.png",
      "value": "World Trade Funds",
      "label": "World Trade Funds (XWT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19808/mrs.png",
      "value": "DeOxyRibose",
      "label": "DeOxyRibose (XNA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19808/mrs.png",
      "value": "RadonPay",
      "label": "RadonPay (RDN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19808/mrs.png",
      "value": "SkullBuzz",
      "label": "SkullBuzz (SKB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19808/mrs.png",
      "value": "CryptoFocus",
      "label": "CryptoFocus (FCS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20293/gam.png",
      "value": "Gambit coin",
      "label": "Gambit coin (GAM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382387/nexus.jpg",
      "value": "Nexus",
      "label": "Nexus (NXS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350786/cesc.png",
      "value": "Crypto Escudo",
      "label": "Crypto Escudo (CESC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20472/viral.png",
      "value": "Twelve Coin",
      "label": "Twelve Coin (TWLV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19808/mrs.png",
      "value": "EagsCoin",
      "label": "EagsCoin (EAGS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19808/mrs.png",
      "value": "MultiWallet Coin",
      "label": "MultiWallet Coin (MWC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350880/adc.png",
      "value": "AudioCoin",
      "label": "AudioCoin (ADC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19808/mrs.png",
      "value": "MarsCoin ",
      "label": "MarsCoin  (MARS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19808/mrs.png",
      "value": "Megastake",
      "label": "Megastake (XMS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19808/mrs.png",
      "value": "Sphere Coin",
      "label": "Sphere Coin (SPHR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19808/mrs.png",
      "value": "Singular",
      "label": "Singular (SIGU)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20215/btx.png",
      "value": "BitcoinTX",
      "label": "BitcoinTX (BTX*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19808/mrs.png",
      "value": "DarkCrave",
      "label": "DarkCrave (DCC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19808/mrs.png",
      "value": "SupplyShock",
      "label": "SupplyShock (M1)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19808/mrs.png",
      "value": "DarkBit",
      "label": "DarkBit (DB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19808/mrs.png",
      "value": "Crypto",
      "label": "Crypto (CTO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20556/edge.png",
      "value": "EdgeCoin",
      "label": "EdgeCoin (EDGE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20557/lux.png",
      "value": "BitLux",
      "label": "BitLux (LUX*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20558/futc.png",
      "value": "FutCoin",
      "label": "FutCoin (FUTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20564/globe.png",
      "value": "Global",
      "label": "Global (GLOBE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20565/tam.png",
      "value": "TamaGucci",
      "label": "TamaGucci (TAM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20357/mrp.png",
      "value": "MorpheusCoin",
      "label": "MorpheusCoin (MRP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20571/creva.png",
      "value": "Creva Coin",
      "label": "Creva Coin (CREVA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20574/xfc.png",
      "value": "Forever Coin",
      "label": "Forever Coin (XFC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20575/nanas.png",
      "value": "BananaBits",
      "label": "BananaBits (NANAS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20335/log.png",
      "value": "Wood Coin",
      "label": "Wood Coin (LOG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20573/xce.png",
      "value": "Cerium",
      "label": "Cerium (XCE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351019/acp.png",
      "value": "Anarchists Prime",
      "label": "Anarchists Prime (ACP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20605/drz.png",
      "value": "Droidz",
      "label": "Droidz (DRZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20602/bucks.png",
      "value": "GorillaBucks",
      "label": "GorillaBucks (BUCKS*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20601/bsc.png",
      "value": "BowsCoin",
      "label": "BowsCoin (BSC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20604/drkt.png",
      "value": "DarkTron",
      "label": "DarkTron (DRKT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20603/circ.png",
      "value": "CryptoCircuits",
      "label": "CryptoCircuits (CIRC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20367/nka.png",
      "value": "IncaKoin",
      "label": "IncaKoin (NKA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20629/versa.png",
      "value": "Versa Token",
      "label": "Versa Token (VERSA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20628/epy.png",
      "value": "Empyrean",
      "label": "Empyrean (EPY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20441/sql.png",
      "value": "Squall Coin",
      "label": "Squall Coin (SQL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20631/poly.png",
      "value": "PolyBit",
      "label": "PolyBit (POLY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19854/piggy.png",
      "value": "Piggy Coin",
      "label": "Piggy Coin (PIGGY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19986/a3c.png",
      "value": "Charity Coin",
      "label": "Charity Coin (CHA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20354/mil.png",
      "value": "Milllionaire Coin",
      "label": "Milllionaire Coin (MIL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383378/crw1.png",
      "value": "Crown Coin",
      "label": "Crown Coin (CRW)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20640/gen.png",
      "value": "Genstake",
      "label": "Genstake (GEN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20641/xph.png",
      "value": "PharmaCoin",
      "label": "PharmaCoin (XPH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20642/grm.png",
      "value": "GridMaster",
      "label": "GridMaster (GRM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20643/qtz.png",
      "value": "Quartz",
      "label": "Quartz (QTZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20645/arb.png",
      "value": "Arbit Coin",
      "label": "Arbit Coin (ARB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20644/lts.png",
      "value": "Litestar Coin",
      "label": "Litestar Coin (LTS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20655/spc.png",
      "value": "SpinCoin",
      "label": "SpinCoin (SPC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20656/gp.png",
      "value": "GoldPieces",
      "label": "GoldPieces (GP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20654/bitz.png",
      "value": "Bitz Coin",
      "label": "Bitz Coin (BITZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19986/a3c.png",
      "value": "DubCoin",
      "label": "DubCoin (DUB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20659/grav.png",
      "value": "Graviton",
      "label": "Graviton (GRAV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20207/bob.png",
      "value": "Bob Coin",
      "label": "Bob Coin (BOB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20346/mcn.png",
      "value": "MonetaVerde",
      "label": "MonetaVerde (MCN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19877/qcn.png",
      "value": "Quazar Coin",
      "label": "Quazar Coin (QCN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20663/hedg.png",
      "value": "Hedgecoin",
      "label": "Hedgecoin (HEDG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20432/song.png",
      "value": "Song Coin",
      "label": "Song Coin (SONG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20163/xseed.png",
      "value": "BitSeeds",
      "label": "BitSeeds (XSEED)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20683/cre.png",
      "value": "Credits",
      "label": "Credits (CRE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20686/axiom.png",
      "value": "Axiom Coin",
      "label": "Axiom Coin (AXIOM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20429/smly.png",
      "value": "SmileyCoin",
      "label": "SmileyCoin (SMLY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20407/rbt.png",
      "value": "Rimbit",
      "label": "Rimbit (RBT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20685/chip.png",
      "value": "Chip",
      "label": "Chip (CHIP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20689/spec.png",
      "value": "SpecCoin",
      "label": "SpecCoin (SPEC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20688/gram.png",
      "value": "Gram Coin",
      "label": "Gram Coin (GRAM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20693/unc.png",
      "value": "UnCoin",
      "label": "UnCoin (UNC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20692/sprts.png",
      "value": "Sprouts",
      "label": "Sprouts (SPRTS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20691/zny.png",
      "value": "BitZeny",
      "label": "BitZeny (ZNY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19638/btq.png",
      "value": "BitQuark",
      "label": "BitQuark (BTQ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19920/str.png",
      "value": "StarCoin",
      "label": "StarCoin (STR*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20700/snrg.png",
      "value": "Synergy",
      "label": "Synergy (SNRG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20701/ghoul.png",
      "value": "Ghoul Coin",
      "label": "Ghoul Coin (GHOUL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20702/hnc.png",
      "value": "Hellenic Coin",
      "label": "Hellenic Coin (HNC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20706/digs.png",
      "value": "Diggits",
      "label": "Diggits (DIGS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20707/exp.png",
      "value": "Expanse",
      "label": "Expanse (EXP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20708/gcr.png",
      "value": "Global Currency Reserve",
      "label": "Global Currency Reserve (GCR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20710/mapc.png",
      "value": "MapCoin",
      "label": "MapCoin (MAPC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20711/mi.png",
      "value": "XiaoMiCoin",
      "label": "XiaoMiCoin (MI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20717/con_.png",
      "value": "Paycon",
      "label": "Paycon (CON_)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20721/neu.png",
      "value": "NeuCoin",
      "label": "NeuCoin (NEU*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20722/tx.png",
      "value": "Transfer",
      "label": "Transfer (TX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780736/grs.png",
      "value": "Groestlcoin ",
      "label": "Groestlcoin  (GRS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20726/siacon-logo.png",
      "value": "Siacoin",
      "label": "Siacoin (SC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20727/clv.png",
      "value": "CleverCoin",
      "label": "CleverCoin (CLV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382863/fct1.png",
      "value": "Factoids",
      "label": "Factoids (FCT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20339/lyb.png",
      "value": "LyraBar",
      "label": "LyraBar (LYB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350558/bst.png",
      "value": "BitStone",
      "label": "BitStone (BST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350559/pxi.png",
      "value": "Prime-X1",
      "label": "Prime-X1 (PXI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350560/cpc.png",
      "value": "CapriCoin",
      "label": "CapriCoin (CPC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350562/ams.png",
      "value": "Amsterdam Coin",
      "label": "Amsterdam Coin (AMS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350565/obits.png",
      "value": "Obits Coin",
      "label": "Obits Coin (OBITS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350609/club.png",
      "value": " ClubCoin",
      "label": " ClubCoin (CLUB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350610/rads.png",
      "value": "Radium",
      "label": "Radium (RADS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350611/emc.png",
      "value": "Emercoin",
      "label": "Emercoin (EMC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350612/blitz.png",
      "value": "BlitzCoin",
      "label": "BlitzCoin (BLITZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350613/hire.png",
      "value": "BitHIRE",
      "label": "BitHIRE (HIRE*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350616/mnd.png",
      "value": "MindCoin",
      "label": "MindCoin (MND)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350691/i0c.png",
      "value": "I0coin",
      "label": "I0coin (I0C)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383113/bta1.png",
      "value": "Bata",
      "label": "Bata (BTA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382607/decred.png",
      "value": "Decred",
      "label": "Decred (DCR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350776/nas2.png",
      "value": "Nas2Coin",
      "label": "Nas2Coin (NAS2)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350788/pak.png",
      "value": "Pakcoin",
      "label": "Pakcoin (PAK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382904/crbit1.png",
      "value": "Creditbit ",
      "label": "Creditbit  (CRB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20029/doged.png",
      "value": "DogeCoinDark",
      "label": "DogeCoinDark (DOGED)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350815/augur-logo.png",
      "value": "Augur",
      "label": "Augur (REP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350819/ok.png",
      "value": "OKCash",
      "label": "OKCash (OK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350824/vox.png",
      "value": "Voxels",
      "label": "Voxels (VOX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350825/amp.png",
      "value": "Synereo",
      "label": "Synereo (AMP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350840/hodl.png",
      "value": "HOdlcoin",
      "label": "HOdlcoin (HODL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350851/dgd.png",
      "value": "Digix DAO",
      "label": "Digix DAO (DGD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350858/edrc.jpg",
      "value": "EDRCoin",
      "label": "EDRCoin (EDRC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352246/lsk.png",
      "value": "Lisk",
      "label": "Lisk (LSK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350888/htc.png",
      "value": "Hitcoin",
      "label": "Hitcoin (HTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350887/game.png",
      "value": "Gamecredits",
      "label": "Gamecredits (GAME)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20026/dash.png",
      "value": "Dashcoin",
      "label": "Dashcoin (DSH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350891/dbic.png",
      "value": "DubaiCoin",
      "label": "DubaiCoin (DBIC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350892/xhi.png",
      "value": "HiCoin",
      "label": "HiCoin (XHI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350893/spots.png",
      "value": "Spots",
      "label": "Spots (SPOTS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350894/bios.png",
      "value": "BiosCrypto",
      "label": "BiosCrypto (BIOS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350895/knc.png",
      "value": "Khancoin",
      "label": "Khancoin (KNC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20260/diem_1.png",
      "value": "CarpeDiemCoin",
      "label": "CarpeDiemCoin (DIEM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350897/gbt.png",
      "value": "GameBetCoin",
      "label": "GameBetCoin (GBT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350901/sar.png",
      "value": "SARCoin",
      "label": "SARCoin (SAR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350902/rcx.png",
      "value": "RedCrowCoin",
      "label": "RedCrowCoin (RCX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350899/pwr.png",
      "value": "PowerCoin",
      "label": "PowerCoin (PWR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350905/trump.png",
      "value": "TrumpCoin",
      "label": "TrumpCoin (TRUMP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350906/prm.png",
      "value": "PrismChain",
      "label": "PrismChain (PRM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350903/bcy.png",
      "value": "BitCrystals",
      "label": "BitCrystals (BCY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350904/rbies.png",
      "value": "Rubies",
      "label": "Rubies (RBIES)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350907/steem.png",
      "value": "Steem",
      "label": "Steem (STEEM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350908/blry.png",
      "value": "BillaryCoin",
      "label": "BillaryCoin (BLRY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350911/xwc_1.png",
      "value": "WhiteCoin",
      "label": "WhiteCoin (XWC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350909/dot.png",
      "value": "Dotcoin",
      "label": "Dotcoin (DOT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20416/scot_1.png",
      "value": "Scotcoin",
      "label": "Scotcoin (SCOT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350912/dnet.png",
      "value": "Darknet",
      "label": "Darknet (DNET)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350913/bac.png",
      "value": "BitalphaCoin",
      "label": "BitalphaCoin (BAC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350916/xid.png",
      "value": "International Diamond Coin",
      "label": "International Diamond Coin (XID*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20310/grt.png",
      "value": "Grantcoin",
      "label": "Grantcoin (GRT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350918/tcr.png",
      "value": "Thecreed",
      "label": "Thecreed (TCR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350917/post.png",
      "value": "PostCoin",
      "label": "PostCoin (POST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350919/infx.png",
      "value": "Influxcoin",
      "label": "Influxcoin (INFX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350910/eths.png",
      "value": "EthereumScrypt",
      "label": "EthereumScrypt (ETHS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350930/pxl.png",
      "value": "Phalanx",
      "label": "Phalanx (PXL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350932/num.png",
      "value": "NumbersCoin",
      "label": "NumbersCoin (NUM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350930/pxl.png",
      "value": "SoulCoin",
      "label": "SoulCoin (SOUL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350933/ion.jpg",
      "value": "Ionomy",
      "label": "Ionomy (ION)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350934/grow.png",
      "value": "GrownCoin",
      "label": "GrownCoin (GROW)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350935/unity_1.png",
      "value": "SuperNET",
      "label": "SuperNET (UNITY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350936/oldsf.png",
      "value": "OldSafeCoin",
      "label": "OldSafeCoin (OLDSF)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350937/ssc.png",
      "value": "SunShotCoin",
      "label": "SunShotCoin (SSTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350938/netc.png",
      "value": "NetworkCoin",
      "label": "NetworkCoin (NETC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350939/gpu.png",
      "value": "GPU Coin",
      "label": "GPU Coin (GPU)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350940/tagr.png",
      "value": "Think And Get Rich Coin",
      "label": "Think And Get Rich Coin (TAGR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350941/hmp.png",
      "value": "HempCoin",
      "label": "HempCoin (HMP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351424/adz1.jpg",
      "value": "Adzcoin",
      "label": "Adzcoin (ADZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350943/gap.png",
      "value": "Gapcoin",
      "label": "Gapcoin (GAP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350947/myc.png",
      "value": "MayaCoin",
      "label": "MayaCoin (MYC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350944/ivz.png",
      "value": "InvisibleCoin",
      "label": "InvisibleCoin (IVZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350945/vta.png",
      "value": "VirtaCoin",
      "label": "VirtaCoin (VTA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350946/sls.png",
      "value": "SaluS",
      "label": "SaluS (SLS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350949/soil.png",
      "value": "SoilCoin",
      "label": "SoilCoin (SOIL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350948/cube.png",
      "value": "DigiCube",
      "label": "DigiCube (CUBE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350957/yoc.png",
      "value": "YoCoin",
      "label": "YoCoin (YOC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350950/coin.png",
      "value": "Coin",
      "label": "Coin (COIN*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350951/vpc.png",
      "value": "VapersCoin",
      "label": "VapersCoin (VPRC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350956/apc.png",
      "value": "AlpaCoin",
      "label": "AlpaCoin (APC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350952/steps.png",
      "value": "Steps",
      "label": "Steps (STEPS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350953/dbtc.png",
      "value": "DebitCoin",
      "label": "DebitCoin (DBTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350954/unit.png",
      "value": "Universal Currency",
      "label": "Universal Currency (UNIT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350955/aeon.png",
      "value": "AeonCoin",
      "label": "AeonCoin (AEON)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350959/moin.png",
      "value": "MoinCoin",
      "label": "MoinCoin (MOIN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350958/sib.png",
      "value": "SibCoin",
      "label": "SibCoin (SIB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350960/erc.png",
      "value": "EuropeCoin",
      "label": "EuropeCoin (ERC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350971/aib.png",
      "value": "AdvancedInternetBlock",
      "label": "AdvancedInternetBlock (AIB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350979/prime.png",
      "value": "PrimeChain",
      "label": "PrimeChain (PRIME)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350973/bern.png",
      "value": "BERNcash",
      "label": "BERNcash (BERN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350980/bigup.png",
      "value": "BigUp",
      "label": "BigUp (BIGUP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350974/kr.png",
      "value": "Krypton",
      "label": "Krypton (KR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350975/xre.jpg",
      "value": "RevolverCoin",
      "label": "RevolverCoin (XRE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383050/pepecoin-512.png",
      "value": "Pepe",
      "label": "Pepe (MEME)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350977/xdb.png",
      "value": "DragonSphere",
      "label": "DragonSphere (XDB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350972/anti.png",
      "value": "Anti Bitcoin",
      "label": "Anti Bitcoin (ANTI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350981/brk.png",
      "value": "BreakoutCoin",
      "label": "BreakoutCoin (BRK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318297/colx.png",
      "value": "ColossusCoinXT",
      "label": "ColossusCoinXT (COLX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350982/mnm.png",
      "value": "Mineum",
      "label": "Mineum (MNM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350983/adcn.png",
      "value": "Asiadigicoin",
      "label": "Asiadigicoin (ADCN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350984/zeit.png",
      "value": "ZeitCoin",
      "label": "ZeitCoin (ZEIT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350986/2give.png",
      "value": "2GiveCoin",
      "label": "2GiveCoin (2GIVE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350988/cga.png",
      "value": "Cryptographic Anomaly",
      "label": "Cryptographic Anomaly (CGA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350987/swing.png",
      "value": "SwingCoin",
      "label": "SwingCoin (SWING)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383986/safex.png",
      "value": "SafeExchangeCoin",
      "label": "SafeExchangeCoin (SAFEX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350990/nebu.png",
      "value": "Nebuchadnezzar",
      "label": "Nebuchadnezzar (NEBU)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350991/aec.png",
      "value": "AcesCoin",
      "label": "AcesCoin (AEC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350992/frn.png",
      "value": "Francs",
      "label": "Francs (FRN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350993/adn.png",
      "value": "Aiden",
      "label": "Aiden (ADN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350994/pulse.jpg",
      "value": "Pulse",
      "label": "Pulse (PULSE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350995/n7.jpg",
      "value": "Number7",
      "label": "Number7 (N7)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350997/cygnus.png",
      "value": "Cygnus",
      "label": "Cygnus (CYG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350996/lgbtq.png",
      "value": "LGBTQoin",
      "label": "LGBTQoin (LGBTQ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350998/uth.png",
      "value": "Uther",
      "label": "Uther (UTH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350999/mpro.jpg",
      "value": "MediumProject",
      "label": "MediumProject (MPRO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351028/katz.png",
      "value": "KATZcoin",
      "label": "KATZcoin (KAT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351002/sup.png",
      "value": "Supreme",
      "label": "Supreme (SPM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351003/mojo.png",
      "value": "Mojocoin",
      "label": "Mojocoin (MOJO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351933/bela.jpg",
      "value": "BelaCoin",
      "label": "BelaCoin (BELA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351007/flx.png",
      "value": "Flash",
      "label": "Flash (FLX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351008/boli.png",
      "value": "BolivarCoin",
      "label": "BolivarCoin (BOLI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351027/clud.png",
      "value": "CludCoin",
      "label": "CludCoin (CLUD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351013/fly.png",
      "value": "FlyCoin",
      "label": "FlyCoin (FLY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351014/hvco.png",
      "value": "High Voltage Coin",
      "label": "High Voltage Coin (HVCO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351015/giz.png",
      "value": "GIZMOcoin",
      "label": "GIZMOcoin (GIZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351016/grexit.png",
      "value": "GrexitCoin",
      "label": "GrexitCoin (GREXIT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351017/carbon.png",
      "value": "Carboncoin",
      "label": "Carboncoin (CARBON)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351018/deur.png",
      "value": "DigiEuro",
      "label": "DigiEuro (DEUR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351020/tur.png",
      "value": "Turron",
      "label": "Turron (TUR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351021/lemon.png",
      "value": "LemonCoin",
      "label": "LemonCoin (LEMON)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351022/sts.png",
      "value": "STRESScoin",
      "label": "STRESScoin (STS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351023/disk.png",
      "value": "Dark Lisk",
      "label": "Dark Lisk (DISK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351026/neva.png",
      "value": "NevaCoin",
      "label": "NevaCoin (NEVA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351024/cyt.png",
      "value": "Cryptokenz",
      "label": "Cryptokenz (CYT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351025/fuzz.png",
      "value": "Fuzzballs",
      "label": "Fuzzballs (FUZZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351041/nkc.png",
      "value": "Nukecoinz",
      "label": "Nukecoinz (NKC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351031/scrt.png",
      "value": "SecretCoin",
      "label": "SecretCoin (SCRT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351032/xra.png",
      "value": "Ratecoin",
      "label": "Ratecoin (XRA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351033/xnx.jpg",
      "value": "XanaxCoin",
      "label": "XanaxCoin (XNX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351043/star.jpg",
      "value": "StarCoin",
      "label": "StarCoin (STAR*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351042/sthr.png",
      "value": "Stakerush",
      "label": "Stakerush (STHR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351047/dbg.png",
      "value": "Digital Bullion Gold",
      "label": "Digital Bullion Gold (DBG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351045/bon_1.png",
      "value": "BonesCoin",
      "label": "BonesCoin (BON*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351044/wmc.png",
      "value": "WMCoin",
      "label": "WMCoin (WMC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351071/gotx.png",
      "value": "GothicCoin",
      "label": "GothicCoin (GOTX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351046/2flav.png",
      "value": "FlavorCoin",
      "label": "FlavorCoin (FLVR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351048/shrek.png",
      "value": "ShrekCoin",
      "label": "ShrekCoin (SHREK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351063/sta.png",
      "value": "Stakers",
      "label": "Stakers (STA*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351059/rise.png",
      "value": "Rise",
      "label": "Rise (RISE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351061/rev.png",
      "value": "Revenu",
      "label": "Revenu (REV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351062/pbc.png",
      "value": "PabyosiCoin",
      "label": "PabyosiCoin (PBC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351064/obs.png",
      "value": "Obscurebay",
      "label": "Obscurebay (OBS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351065/exit.png",
      "value": "ExitCoin",
      "label": "ExitCoin (EXIT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351066/edc.png",
      "value": "EducoinV",
      "label": "EducoinV (EDC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351067/clint.png",
      "value": "Clinton",
      "label": "Clinton (CLINT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351068/ckc.png",
      "value": "Clockcoin",
      "label": "Clockcoin (CKC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351069/vip.png",
      "value": "VIP Tokens",
      "label": "VIP Tokens (VIP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351070/nxe.png",
      "value": "NXEcoin",
      "label": "NXEcoin (NXE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351081/zoom.png",
      "value": "ZoomCoin",
      "label": "ZoomCoin (ZOOM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351390/dt-token.png",
      "value": "DT Token",
      "label": "DT Token (DRACO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351073/yovi.png",
      "value": "YobitVirtualCoin",
      "label": "YobitVirtualCoin (YOVI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351076/orly.png",
      "value": "OrlyCoin",
      "label": "OrlyCoin (ORLY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351077/kubo.png",
      "value": "KubosCoin",
      "label": "KubosCoin (KUBO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351078/incp.png",
      "value": "InceptionCoin",
      "label": "InceptionCoin (INCP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351079/sak.png",
      "value": "SharkCoin",
      "label": "SharkCoin (SAK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351080/evil.png",
      "value": "EvilCoin",
      "label": "EvilCoin (EVIL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20386/oma.png",
      "value": "OmegaCoin",
      "label": "OmegaCoin (OMA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351084/mue.png",
      "value": "MonetaryUnit",
      "label": "MonetaryUnit (MUE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351083/cox.png",
      "value": "CobraCoin",
      "label": "CobraCoin (COX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383549/bnt.jpg",
      "value": "Bancor Network Token",
      "label": "Bancor Network Token (BNT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351086/bsd.png",
      "value": "BitSend",
      "label": "BitSend (BSD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351087/des.png",
      "value": "Destiny",
      "label": "Destiny (DES)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20181/16bit.png",
      "value": "16BitCoin",
      "label": "16BitCoin (BIT16)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351088/pdc.png",
      "value": "Project Decorum",
      "label": "Project Decorum (PDC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351094/chess.jpg",
      "value": "ChessCoin",
      "label": "ChessCoin (CHESS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351095/space.png",
      "value": "SpaceCoin",
      "label": "SpaceCoin (SPACE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351096/ree.png",
      "value": "ReeCoin",
      "label": "ReeCoin (REE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351097/lqd.png",
      "value": "Liquid",
      "label": "Liquid (LQD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351099/marv.png",
      "value": "Marvelous",
      "label": "Marvelous (MARV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351100/xde2.png",
      "value": "XDE II",
      "label": "XDE II (XDE2)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351101/vec2.png",
      "value": "VectorCoin 2.0 ",
      "label": "VectorCoin 2.0  (VEC2)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351102/omni.png",
      "value": "Omni",
      "label": "Omni (OMNI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351103/gsy.png",
      "value": "GenesysCoin",
      "label": "GenesysCoin (GSY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351104/tkn.png",
      "value": "TrollTokens",
      "label": "TrollTokens (TKN*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351208/lir.png",
      "value": "Let it Ride",
      "label": "Let it Ride (LIR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351209/nxtasset.png",
      "value": "MMNXT ",
      "label": "MMNXT  (MMNXT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351210/scrpt.png",
      "value": "ScryptCoin",
      "label": "ScryptCoin (SCRPT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351211/lbc.png",
      "value": "LBRY Credits",
      "label": "LBRY Credits (LBC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351212/spx.png",
      "value": "Specie",
      "label": "Specie (SPX*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350907/steem.png",
      "value": "Steem Backed Dollars",
      "label": "Steem Backed Dollars (SBD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351234/cj.png",
      "value": "CryptoJacks",
      "label": "CryptoJacks (CJ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383668/put1.png",
      "value": "PutinCoin",
      "label": "PutinCoin (PUT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351236/krak.png",
      "value": "Kraken",
      "label": "Kraken (KRAK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351237/dlisk.png",
      "value": "Dlisk",
      "label": "Dlisk (DLISK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351303/stratis-logo.png",
      "value": "Stratis",
      "label": "Stratis (STRAT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351304/voya.png",
      "value": "Voyacoin",
      "label": "Voyacoin (VOYA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351305/enter.png",
      "value": "EnterCoin (ENTER)",
      "label": "EnterCoin (ENTER) (ENTER)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351310/wgc.png",
      "value": "World Gold Coin",
      "label": "World Gold Coin (WGC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351311/bm.png",
      "value": "BitMoon",
      "label": "BitMoon (BM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351361/frwc.png",
      "value": "Frankywillcoin",
      "label": "Frankywillcoin (FRWC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351362/psy.png",
      "value": "Psilocybin",
      "label": "Psilocybin (PSY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351364/xt.png",
      "value": "ExtremeCoin",
      "label": "ExtremeCoin (XT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351365/rust.png",
      "value": "RustCoin",
      "label": "RustCoin (RUST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351366/nzc.png",
      "value": "NewZealandCoin",
      "label": "NewZealandCoin (NZC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351368/sngls.png",
      "value": "SingularDTV",
      "label": "SingularDTV (SNGLS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351382/xaur.png",
      "value": "Xaurum",
      "label": "Xaurum (XAUR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19554/bitfinex.png",
      "value": "BitFinex Tokens",
      "label": "BitFinex Tokens (BFX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351387/uniq.png",
      "value": "Uniqredit",
      "label": "Uniqredit (UNIQ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351388/crx.png",
      "value": "ChronosCoin",
      "label": "ChronosCoin (CRX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351389/dct.png",
      "value": "Decent",
      "label": "Decent (DCT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351393/xpoke.png",
      "value": "PokeChain",
      "label": "PokeChain (XPOKE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351394/mudra.png",
      "value": "MudraCoin",
      "label": "MudraCoin (MUDRA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351395/warp.png",
      "value": "WarpCoin",
      "label": "WarpCoin (WARP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351396/cnmt.png",
      "value": "Coinomat",
      "label": "Coinomat (CNMT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351397/pizza.png",
      "value": "PizzaCoin",
      "label": "PizzaCoin (PIZZA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351398/lc.png",
      "value": "Lutetium Coin",
      "label": "Lutetium Coin (LC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351399/heat.png",
      "value": "Heat Ledger",
      "label": "Heat Ledger (HEAT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351400/icn.png",
      "value": "Iconomi",
      "label": "Iconomi (ICN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351401/exb.png",
      "value": "ExaByte (EXB)",
      "label": "ExaByte (EXB) (EXB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382758/1wings.png",
      "value": "Wings DAO",
      "label": "Wings DAO (WINGS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351404/cdx.png",
      "value": "Cryptodex",
      "label": "Cryptodex (CDX*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351405/rbit.png",
      "value": "ReturnBit",
      "label": "ReturnBit (RBIT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351407/cloud.png",
      "value": "deCLOUDs",
      "label": "deCLOUDs (DCS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351408/kmd.png",
      "value": "Komodo",
      "label": "Komodo (KMD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351411/db.png",
      "value": "GoldBlocks",
      "label": "GoldBlocks (GB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383858/neo.jpg",
      "value": "NEO",
      "label": "NEO (NEO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19598/anc.png",
      "value": "Anoncoin",
      "label": "Anoncoin (ANC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/15887426/synx.png",
      "value": "Syndicate",
      "label": "Syndicate (SYNX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351428/mc.png",
      "value": "Mass Coin",
      "label": "Mass Coin (MC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351430/edc.png",
      "value": "E-Dinar Coin",
      "label": "E-Dinar Coin (EDR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351432/jwl.png",
      "value": "Jewels",
      "label": "Jewels (JWL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351433/way.png",
      "value": "WayCoin",
      "label": "WayCoin (WAY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351488/tab.png",
      "value": "MollyCoin",
      "label": "MollyCoin (TAB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351489/trg.png",
      "value": "Trigger",
      "label": "Trigger (TRIG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351490/bitcny.png",
      "value": "bitCNY",
      "label": "bitCNY (BITCNY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351491/bitusd.png",
      "value": "bitUSD",
      "label": "bitUSD (BITUSD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351493/sto.png",
      "value": "Save The Ocean",
      "label": "Save The Ocean (STO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351494/sns.png",
      "value": "Sense",
      "label": "Sense (SNS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351495/fsn.png",
      "value": "Fusion",
      "label": "Fusion (FSN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351496/ctc.png",
      "value": "CarterCoin",
      "label": "CarterCoin (CTC)"
    },
    {
      "value": "TotCoin",
      "label": "TotCoin (TOT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351498/btd.png",
      "value": "Bitcloud",
      "label": "Bitcloud (BTD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351499/bot.png",
      "value": "ArkDAO",
      "label": "ArkDAO (BOTS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351500/mdc.png",
      "value": "MedicCoin",
      "label": "MedicCoin (MDC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351501/ftp.png",
      "value": "FuturePoints",
      "label": "FuturePoints (FTP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351502/zet2.png",
      "value": "Zeta2Coin",
      "label": "Zeta2Coin (ZET2)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351503/cov.png",
      "value": "CovenCoin",
      "label": "CovenCoin (COV*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351505/tell.png",
      "value": "Tellurion",
      "label": "Tellurion (TELL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351506/ene.png",
      "value": "EneCoin",
      "label": "EneCoin (ENE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351507/tdfb.png",
      "value": "TDFB",
      "label": "TDFB (TDFB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351508/blockpay.png",
      "value": "BlockPay",
      "label": "BlockPay (BLOCKPAY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351509/bxt.png",
      "value": "BitTokens",
      "label": "BitTokens (BXT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351510/zyd.png",
      "value": "ZayedCoin",
      "label": "ZayedCoin (ZYD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351529/mst1.png",
      "value": "MustangCoin",
      "label": "MustangCoin (MST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351512/goon.png",
      "value": "Goonies",
      "label": "Goonies (GOON)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351514/vlt.png",
      "value": "Veltor",
      "label": "Veltor (VLT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351515/zne.jpg",
      "value": "ZoneCoin",
      "label": "ZoneCoin (ZNE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351516/dck.png",
      "value": "DickCoin",
      "label": "DickCoin (DCK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351519/coval.png",
      "value": "Circuits of Value",
      "label": "Circuits of Value (COVAL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351520/dgd.png",
      "value": "DarkGold",
      "label": "DarkGold (DGDC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351521/today.png",
      "value": "TodayCoin",
      "label": "TodayCoin (TODAY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351522/vrm.png",
      "value": "Verium",
      "label": "Verium (VRM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351523/root.png",
      "value": "RootCoin",
      "label": "RootCoin (ROOT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351524/1st.png",
      "value": "FirstBlood",
      "label": "FirstBlood (1ST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351525/gpl.png",
      "value": "Gold Pressed Latinum",
      "label": "Gold Pressed Latinum (GPL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351526/dope.png",
      "value": "DopeCoin",
      "label": "DopeCoin (DOPE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351527/fx.png",
      "value": "FCoin",
      "label": "FCoin (FX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351528/pio.png",
      "value": "Pioneershares",
      "label": "Pioneershares (PIO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351531/gay.png",
      "value": "GayCoin",
      "label": "GayCoin (GAY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351543/smsr.png",
      "value": "Samsara Coin",
      "label": "Samsara Coin (SMSR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351544/ubiq.png",
      "value": "Ubiqoin",
      "label": "Ubiqoin (UBIQ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351545/arm.png",
      "value": "Armory Coin",
      "label": "Armory Coin (ARM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351546/ring.png",
      "value": "RingCoin",
      "label": "RingCoin (RING)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351550/erb.png",
      "value": "ERBCoin",
      "label": "ERBCoin (ERB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351552/laz.png",
      "value": "Lazarus",
      "label": "Lazarus (LAZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351553/fonz.png",
      "value": "FonzieCoin",
      "label": "FonzieCoin (FONZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351554/btr.png",
      "value": "BitCurrency",
      "label": "BitCurrency (BTCR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351555/drop.png",
      "value": "FaucetCoin",
      "label": "FaucetCoin (DROP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351556/sandt.png",
      "value": "Save and Gain",
      "label": "Save and Gain (SANDG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351557/pnk.png",
      "value": "SteamPunk",
      "label": "SteamPunk (PNK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351558/moond.png",
      "value": "Dark Moon",
      "label": "Dark Moon (MOOND)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351559/dlc.png",
      "value": "DollarCoin",
      "label": "DollarCoin (DLC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351560/sen.png",
      "value": "Sentaro",
      "label": "Sentaro (SEN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351563/scn.png",
      "value": "Swiscoin",
      "label": "Swiscoin (SCN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351564/wex.jpg",
      "value": "Wexcoin",
      "label": "Wexcoin (WEX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351565/lth.png",
      "value": "Lathaan",
      "label": "Lathaan (LTH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351566/bronz.png",
      "value": "BitBronze",
      "label": "BitBronze (BRONZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351567/sh.png",
      "value": "Shilling",
      "label": "Shilling (SH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/15887419/buzz.png",
      "value": "BuzzCoin",
      "label": "BuzzCoin (BUZZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351588/mg.png",
      "value": "Mind Gene",
      "label": "Mind Gene (MG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351589/psi.png",
      "value": "PSIcoin",
      "label": "PSIcoin (PSI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351590/xpo.png",
      "value": "Opair",
      "label": "Opair (XPO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351591/nlc.png",
      "value": "NoLimitCoin",
      "label": "NoLimitCoin (NLC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351594/psb.jpg",
      "value": "PesoBit",
      "label": "PesoBit (PSB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351617/beats.png",
      "value": "Beats",
      "label": "Beats (XBTS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351618/fit.png",
      "value": "Fitcoin",
      "label": "Fitcoin (FIT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351624/pinkx.png",
      "value": "PantherCoin",
      "label": "PantherCoin (PINKX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351625/fire.png",
      "value": "FireCoin",
      "label": "FireCoin (FIRE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351626/unf.png",
      "value": "Unfed Coin",
      "label": "Unfed Coin (UNF)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351627/sports.png",
      "value": "SportsCoin",
      "label": "SportsCoin (SPORT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351630/peerplays.png",
      "value": "Peerplays",
      "label": "Peerplays (PPY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351631/ntc.png",
      "value": "NineElevenTruthCoin",
      "label": "NineElevenTruthCoin (NTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351632/ego.png",
      "value": "EGOcoin",
      "label": "EGOcoin (EGO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351633/btlc.png",
      "value": "BitluckCoin",
      "label": "BitluckCoin (BTCL*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351634/rcn.png",
      "value": "RCoin",
      "label": "RCoin (RCN*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351635/x2.png",
      "value": "X2Coin",
      "label": "X2Coin (X2)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19453/mycelium.png",
      "value": "Mycelium Token",
      "label": "Mycelium Token (MT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351636/tia.png",
      "value": "Tianhe",
      "label": "Tianhe (TIA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351637/gbrc.png",
      "value": "GBR Coin",
      "label": "GBR Coin (GBRC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351638/xup.png",
      "value": "UPcoin",
      "label": "UPcoin (XUP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351657/hallo.png",
      "value": "Halloween Coin",
      "label": "Halloween Coin (HALLO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351658/bbcc.png",
      "value": "BaseballCardCoin",
      "label": "BaseballCardCoin (BBCC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351659/emirg.png",
      "value": "EmiratesGoldCoin",
      "label": "EmiratesGoldCoin (EMIGR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351660/bhc.png",
      "value": "BighanCoin",
      "label": "BighanCoin (BHC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351681/craft.png",
      "value": "Craftcoin",
      "label": "Craftcoin (CRAFT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351682/inv.png",
      "value": "Invictus",
      "label": "Invictus (INV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351683/olymp.png",
      "value": "OlympCoin",
      "label": "OlympCoin (OLYMP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351684/dpay.png",
      "value": "DelightPay",
      "label": "DelightPay (DPAY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351685/atom.png",
      "value": "Atomic Coin",
      "label": "Atomic Coin (ATOM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351689/hkg.jpg",
      "value": "Hacker Gold",
      "label": "Hacker Gold (HKG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351690/antc.png",
      "value": "AntiLitecoin",
      "label": "AntiLitecoin (ANTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351691/jobs.png",
      "value": "JobsCoin",
      "label": "JobsCoin (JOBS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351697/dgore.png",
      "value": "DogeGoreCoin",
      "label": "DogeGoreCoin (DGORE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351699/thc.png",
      "value": "The Hempcoin",
      "label": "The Hempcoin (THC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351700/tra.png",
      "value": "Tetra",
      "label": "Tetra (TRA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351701/rms.png",
      "value": "Resumeo Shares",
      "label": "Resumeo Shares (RMS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351708/vapor.png",
      "value": "Vaporcoin",
      "label": "Vaporcoin (VAPOR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351709/sdp.jpg",
      "value": "SydPakCoin",
      "label": "SydPakCoin (SDP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19554/bitfinex.png",
      "value": "Recovery Right Tokens",
      "label": "Recovery Right Tokens (RRT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382780/xzc1.png",
      "value": "ZCoin",
      "label": "ZCoin (XZC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351711/pre.png",
      "value": "Premium",
      "label": "Premium (PRE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351712/calc.png",
      "value": "CaliphCoin",
      "label": "CaliphCoin (CALC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351729/lea.png",
      "value": "LeaCoin",
      "label": "LeaCoin (LEA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351730/cf.png",
      "value": "Californium",
      "label": "Californium (CF)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351731/crnk.png",
      "value": "CrankCoin",
      "label": "CrankCoin (CRNK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351732/cfc.png",
      "value": "CoffeeCoin",
      "label": "CoffeeCoin (CFC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351733/vty.png",
      "value": "Victoriouscoin",
      "label": "Victoriouscoin (VTY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351735/sfe.png",
      "value": "Safecoin",
      "label": "Safecoin (SFE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351736/ardr.png",
      "value": "Ardor",
      "label": "Ardor (ARDR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351737/bs.png",
      "value": "BlackShadowCoin",
      "label": "BlackShadowCoin (BS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351738/jif.png",
      "value": "JiffyCoin",
      "label": "JiffyCoin (JIF)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351739/crab.png",
      "value": "CrabCoin",
      "label": "CrabCoin (CRAB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351747/hill.png",
      "value": "President Clinton",
      "label": "President Clinton (HILL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351748/forex.png",
      "value": "ForexCoin",
      "label": "ForexCoin (FOREX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351749/moneta.png",
      "value": "Moneta",
      "label": "Moneta (MONETA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351750/ec.jpg",
      "value": "Eclipse",
      "label": "Eclipse (EC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351751/rubit.png",
      "value": "Rublebit",
      "label": "Rublebit (RUBIT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351752/hcc.png",
      "value": "HappyCreatorCoin ",
      "label": "HappyCreatorCoin  (HCC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351753/brain.png",
      "value": "BrainCoin",
      "label": "BrainCoin (BRAIN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351754/vertex.png",
      "value": "Vertex",
      "label": "Vertex (VTX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351755/krc.png",
      "value": "KRCoin",
      "label": "KRCoin (KRC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351756/royal.png",
      "value": "RoyalCoin",
      "label": "RoyalCoin (ROYAL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351757/lfc.png",
      "value": "BigLifeCoin",
      "label": "BigLifeCoin (LFC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351758/zur.png",
      "value": "Zurcoin",
      "label": "Zurcoin (ZUR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351759/nubis.png",
      "value": "NubisCoin",
      "label": "NubisCoin (NUBIS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351760/tennet.png",
      "value": "Tennet",
      "label": "Tennet (TENNET)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351761/pec.png",
      "value": "PeaceCoin",
      "label": "PeaceCoin (PEC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351762/gmx.jpg",
      "value": "Goldmaxcoin",
      "label": "Goldmaxcoin (GMX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351763/32bit.png",
      "value": "32Bitcoin",
      "label": "32Bitcoin (32BIT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351789/gnj.png",
      "value": "GanjaCoin V2",
      "label": "GanjaCoin V2 (GNJ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351790/team.png",
      "value": "TeamUP",
      "label": "TeamUP (TEAM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351791/sct.png",
      "value": "ScryptToken",
      "label": "ScryptToken (SCT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351792/lana.png",
      "value": "LanaCoin",
      "label": "LanaCoin (LANA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351793/ele.png",
      "value": "Elementrem",
      "label": "Elementrem (ELE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351796/gcc.jpg",
      "value": "GuccioneCoin",
      "label": "GuccioneCoin (GCC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351797/and.png",
      "value": "AndromedaCoin",
      "label": "AndromedaCoin (AND)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351835/bytes.png",
      "value": "Byteball",
      "label": "Byteball (GBYTE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351867/equal.png",
      "value": "EqualCoin",
      "label": "EqualCoin (EQUAL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351868/sweet.png",
      "value": "SweetStake",
      "label": "SweetStake (SWEET)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351869/2bacco.png",
      "value": "2BACCO Coin",
      "label": "2BACCO Coin (2BACCO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351870/dkc.png",
      "value": "DarkKnightCoin",
      "label": "DarkKnightCoin (DKC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351872/coc.png",
      "value": "Community Coin",
      "label": "Community Coin (COC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351876/choof.png",
      "value": "ChoofCoin",
      "label": "ChoofCoin (CHOOF)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351877/csh.png",
      "value": "CashOut",
      "label": "CashOut (CSH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351756/royal.png",
      "value": "RoyalCoin 2.0",
      "label": "RoyalCoin 2.0 (RYCN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351927/pabyosi.png",
      "value": "Pabyosi Coin",
      "label": "Pabyosi Coin (PCS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351928/nbit.png",
      "value": "NetBit",
      "label": "NetBit (NBIT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351929/wine.png",
      "value": "WineCoin",
      "label": "WineCoin (WINE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351930/dar.png",
      "value": "Darcrus",
      "label": "Darcrus (DAR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351931/ark.png",
      "value": "ARK",
      "label": "ARK (ARK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351934/iflt.png",
      "value": "InflationCoin",
      "label": "InflationCoin (IFLT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351935/zecd.png",
      "value": "ZCashDarkCoin",
      "label": "ZCashDarkCoin (ZECD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351936/zxt.png",
      "value": "Zcrypt",
      "label": "Zcrypt (ZXT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351944/wash.png",
      "value": "WashingtonCoin",
      "label": "WashingtonCoin (WASH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351945/tesla.png",
      "value": "TeslaCoilCoin",
      "label": "TeslaCoilCoin (TESLA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351946/lucky.png",
      "value": "LuckyBlocks (LUCKY)",
      "label": "LuckyBlocks (LUCKY) (LUCKY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352113/d5a4e4f0366d3ae8cdbc45ad097f664c2557a55f0c237c1710-pimgpsh_fullsize_distr.jpg",
      "value": "vSlice",
      "label": "vSlice (VSL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351948/tpg.png",
      "value": "Troll Payment",
      "label": "Troll Payment (TPG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351988/leo.png",
      "value": "LEOcoin",
      "label": "LEOcoin (LEO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351990/cbd.png",
      "value": "CBD Crystals",
      "label": "CBD Crystals (CBD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351992/pex.png",
      "value": "PosEx",
      "label": "PosEx (PEX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351993/insane.png",
      "value": "InsaneCoin",
      "label": "InsaneCoin (INSANE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351995/golem_logo.png",
      "value": "Golem Network Token",
      "label": "Golem Network Token (GNT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20384/pen.png",
      "value": "PenCoin",
      "label": "PenCoin (PEN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352004/bash.png",
      "value": "LuckChain",
      "label": "LuckChain (BASH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352006/fame.png",
      "value": "FameCoin",
      "label": "FameCoin (FAME)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352007/liv.png",
      "value": "LiviaCoin",
      "label": "LiviaCoin (LIV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352018/sp.png",
      "value": "Sex Pistols",
      "label": "Sex Pistols (SP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352020/mega.png",
      "value": "MegaFlash",
      "label": "MegaFlash (MEGA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352021/vrs.png",
      "value": "Veros",
      "label": "Veros (VRS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352022/alc.png",
      "value": "Arab League Coin",
      "label": "Arab League Coin (ALC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352023/dogeth-2.png",
      "value": "EtherDoge",
      "label": "EtherDoge (DOGETH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352024/klc.png",
      "value": "KiloCoin",
      "label": "KiloCoin (KLC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383138/thehush_300x300.png",
      "value": "Hush",
      "label": "Hush (HUSH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352054/btlc.png",
      "value": "BitLuckCoin",
      "label": "BitLuckCoin (BTLC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352055/drm8.png",
      "value": "Dream8Coin",
      "label": "Dream8Coin (DRM8)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352056/fist.png",
      "value": "FistBump",
      "label": "FistBump (FIST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352069/ebz.png",
      "value": "Ebitz",
      "label": "Ebitz (EBZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352072/drs.png",
      "value": "Digital Rupees",
      "label": "Digital Rupees (DRS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352082/fgz.png",
      "value": "Free Game Zone",
      "label": "Free Game Zone (FGZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352083/boson.png",
      "value": "BosonCoin",
      "label": "BosonCoin (BOSON)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352084/atx.png",
      "value": "ArtexCoin",
      "label": "ArtexCoin (ATX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352085/pnc.png",
      "value": "PlatiniumCoin",
      "label": "PlatiniumCoin (PNC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352086/brdd.png",
      "value": "BeardDollars",
      "label": "BeardDollars (BRDD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352105/time.png",
      "value": "Time",
      "label": "Time (TIME)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352108/bip.png",
      "value": "BipCoin",
      "label": "BipCoin (BIP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352109/xnc.png",
      "value": "XenCoin",
      "label": "XenCoin (XNC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352110/emb.png",
      "value": "EmberCoin",
      "label": "EmberCoin (EMB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352111/bttf.png",
      "value": "Coin to the Future",
      "label": "Coin to the Future (BTTF)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352114/dollarcoin.png",
      "value": "DollarOnline",
      "label": "DollarOnline (DLR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352115/csmic.png",
      "value": "Cosmic",
      "label": "Cosmic (CSMIC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352116/first.png",
      "value": "FirstCoin",
      "label": "FirstCoin (FIRST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352117/scash.png",
      "value": "SpaceCash",
      "label": "SpaceCash (SCASH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352119/xen.jpg",
      "value": "XenixCoin",
      "label": "XenixCoin (XEN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352120/jio.png",
      "value": "JIO Token",
      "label": "JIO Token (JIO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352121/iw.png",
      "value": "iWallet",
      "label": "iWallet (IW)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352126/jns.png",
      "value": "Janus",
      "label": "Janus (JNS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352127/trick.png",
      "value": "TrickyCoin",
      "label": "TrickyCoin (TRICK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352128/dcre.png",
      "value": "DeltaCredits",
      "label": "DeltaCredits (DCRE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352129/fre.png",
      "value": "FreeCoin",
      "label": "FreeCoin (FRE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352130/npc.png",
      "value": "NPCcoin",
      "label": "NPCcoin (NPC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352132/dgms.png",
      "value": "Digigems",
      "label": "Digigems (DGMS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352133/icb.png",
      "value": "Icobid",
      "label": "Icobid (ICOB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352134/arco.png",
      "value": "AquariusCoin",
      "label": "AquariusCoin (ARCO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352155/kurt.png",
      "value": "Kurrent",
      "label": "Kurrent (KURT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352156/xcre.png",
      "value": "Creatio",
      "label": "Creatio (XCRE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352157/ent.jpg",
      "value": "Eternity",
      "label": "Eternity (ENT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352182/ur.jpg",
      "value": "UR",
      "label": "UR (UR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352183/mtmc3.png",
      "value": "Metal Music v3",
      "label": "Metal Music v3 (MTLM3)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352186/odnt.png",
      "value": "Old Dogs New Tricks",
      "label": "Old Dogs New Tricks (ODNT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382471/euc.png",
      "value": "Eurocoin",
      "label": "Eurocoin (EUC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352188/ccx.png",
      "value": "CoolDarkCoin",
      "label": "CoolDarkCoin (CCX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352189/btf.png",
      "value": "BitcoinFast",
      "label": "BitcoinFast (BCF)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352190/seeds.png",
      "value": "SeedShares",
      "label": "SeedShares (SEEDS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382902/posw.png",
      "value": "PoSWallet",
      "label": "PoSWallet (POSW)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352207/tks.jpg",
      "value": "Tokes",
      "label": "Tokes (TKS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350709/bccoin1.png",
      "value": "BitConnect Coin",
      "label": "BitConnect Coin (BCCOIN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352222/shorty.png",
      "value": "ShortyCoin",
      "label": "ShortyCoin (SHORTY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352223/pcm.png",
      "value": "Procom",
      "label": "Procom (PCM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352224/kc.png",
      "value": "Kernalcoin",
      "label": "Kernalcoin (KC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352225/coral.png",
      "value": "CoralPay",
      "label": "CoralPay (CORAL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352236/bam.png",
      "value": "BAM",
      "label": "BAM (BAM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352248/nxc.png",
      "value": "Nexium",
      "label": "Nexium (NXC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352249/money.png",
      "value": "MoneyCoin",
      "label": "MoneyCoin (MONEY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352250/bstar.png",
      "value": "Blackstar",
      "label": "Blackstar (BSTAR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352251/hsp.png",
      "value": "Horse Power",
      "label": "Horse Power (HSP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352291/hzt.png",
      "value": "HazMatCoin",
      "label": "HazMatCoin (HZT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352292/cs.png",
      "value": "CryptoSpots",
      "label": "CryptoSpots (CS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352293/xsp.png",
      "value": "PoolStamp",
      "label": "PoolStamp (XSP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352294/ccrb.png",
      "value": "CryptoCarbon",
      "label": "CryptoCarbon (CCRB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352295/bulls.png",
      "value": "BullshitCoin",
      "label": "BullshitCoin (BULLS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352296/incnt.png",
      "value": "Incent",
      "label": "Incent (INCNT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352297/icon.png",
      "value": "Iconic",
      "label": "Iconic (ICON)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352309/nic.png",
      "value": "NewInvestCoin",
      "label": "NewInvestCoin (NIC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352310/acn.png",
      "value": "AvonCoin",
      "label": "AvonCoin (ACN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352311/xng.png",
      "value": "Enigma",
      "label": "Enigma (XNG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352312/xci.png",
      "value": "Cannabis Industry Coin",
      "label": "Cannabis Industry Coin (XCI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1381970/look.png",
      "value": "LookCoin",
      "label": "LookCoin (LOOK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1381971/loc.png",
      "value": "Loco",
      "label": "Loco (LOC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1381972/mmxvi.png",
      "value": "MMXVI",
      "label": "MMXVI (MMXVI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1381975/trst.png",
      "value": "TrustCoin",
      "label": "TrustCoin (TRST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1381981/mis.png",
      "value": "MIScoin",
      "label": "MIScoin (MIS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1381982/wop.png",
      "value": "WorldPay",
      "label": "WorldPay (WOP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1381983/cqst.png",
      "value": "ConquestCoin",
      "label": "ConquestCoin (CQST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1381984/imps.jpg",
      "value": "Impulse Coin",
      "label": "Impulse Coin (IMPS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1381987/in.png",
      "value": "InCoin",
      "label": "InCoin (IN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1381988/chief.png",
      "value": "TheChiefCoin",
      "label": "TheChiefCoin (CHIEF)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1381990/goat.png",
      "value": "Goat",
      "label": "Goat (GOAT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1381991/anal.jpg",
      "value": "AnalCoin",
      "label": "AnalCoin (ANAL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1381992/rc.png",
      "value": "Russiacoin",
      "label": "Russiacoin (RC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318184/pnd.png",
      "value": "PandaCoin",
      "label": "PandaCoin (PND)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1381994/px.png",
      "value": "PXcoin",
      "label": "PXcoin (PX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1381997/cnd.png",
      "value": "Canada eCoin",
      "label": "Canada eCoin (CND*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1381998/option.png",
      "value": "OptionCoin",
      "label": "OptionCoin (OPTION)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382048/av.png",
      "value": "Avatar Coin",
      "label": "Avatar Coin (AV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382049/ltd.png",
      "value": "Limited Coin",
      "label": "Limited Coin (LTD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382050/units.png",
      "value": "GameUnits",
      "label": "GameUnits (UNITS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382051/heel.png",
      "value": "HeelCoin",
      "label": "HeelCoin (HEEL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382090/gakh.png",
      "value": "GAKHcoin",
      "label": "GAKHcoin (GAKH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382125/shift.png",
      "value": "Shift",
      "label": "Shift (SHIFT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382093/s8c.png",
      "value": "S88 Coin",
      "label": "S88 Coin (S8C)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382094/lvg.png",
      "value": "Leverage Coin",
      "label": "Leverage Coin (LVG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382095/dra.png",
      "value": "DraculaCoin",
      "label": "DraculaCoin (DRA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382097/ltcr.png",
      "value": "LiteCreed",
      "label": "LiteCreed (LTCR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19874/qbc.png",
      "value": "Quebecoin",
      "label": "Quebecoin (QBC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382098/xpro.png",
      "value": "ProCoin",
      "label": "ProCoin (XPRO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382170/ast.png",
      "value": "Astral",
      "label": "Astral (AST*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382171/gift.png",
      "value": "GiftNet",
      "label": "GiftNet (GIFT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382172/vidz.png",
      "value": "PureVidz",
      "label": "PureVidz (VIDZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382173/inc.png",
      "value": "Incrementum",
      "label": "Incrementum (INC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382236/pta.png",
      "value": "PentaCoin",
      "label": "PentaCoin (PTA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382237/acid.png",
      "value": "AcidCoin",
      "label": "AcidCoin (ACID)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382238/zlq.png",
      "value": "ZLiteQubit",
      "label": "ZLiteQubit (ZLQ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382239/rad.png",
      "value": "RadicalCoin",
      "label": "RadicalCoin (RADI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382240/rnc.png",
      "value": "ReturnCoin",
      "label": "ReturnCoin (RNC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382246/golos.png",
      "value": "Golos",
      "label": "Golos (GOLOS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382247/pasc.png",
      "value": "Pascal Coin",
      "label": "Pascal Coin (PASC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382250/twist1.png",
      "value": "TwisterCoin",
      "label": "TwisterCoin (TWIST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382251/payp.png",
      "value": "PayPeer",
      "label": "PayPeer (PAYP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382252/deth.png",
      "value": "DarkEther",
      "label": "DarkEther (DETH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382253/yay.png",
      "value": "YAYcoin",
      "label": "YAYcoin (YAY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382269/yes.png",
      "value": "YesCoin",
      "label": "YesCoin (YES)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382270/lenin.png",
      "value": "LeninCoin",
      "label": "LeninCoin (LENIN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382287/msra.png",
      "value": "MrsaCoin",
      "label": "MrsaCoin (MRSA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382288/os76.png",
      "value": "OsmiumCoin",
      "label": "OsmiumCoin (OS76)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382289/boss.png",
      "value": "BitBoss",
      "label": "BitBoss (BOSS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382296/mkr.png",
      "value": "Maker",
      "label": "Maker (MKR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382337/bic.png",
      "value": "Bikercoins",
      "label": "Bikercoins (BIC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382338/crps.png",
      "value": "CryptoPennies",
      "label": "CryptoPennies (CRPS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382339/moto.png",
      "value": "Motocoin",
      "label": "Motocoin (MOTO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382346/ntcc.png",
      "value": "NeptuneClassic",
      "label": "NeptuneClassic (NTCC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382347/xnc.png",
      "value": "Numismatic Collections",
      "label": "Numismatic Collections (XNC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382348/hexx.jpg",
      "value": "HexxCoin",
      "label": "HexxCoin (HXX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382349/spkr.png",
      "value": "Ghost Coin",
      "label": "Ghost Coin (SPKTR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382368/mac.png",
      "value": "MachineCoin",
      "label": "MachineCoin (MAC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382369/sel.png",
      "value": "SelenCoin",
      "label": "SelenCoin (SEL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382370/noo.png",
      "value": "Noocoin",
      "label": "Noocoin (NOO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382371/chao.png",
      "value": "23 Skidoo",
      "label": "23 Skidoo (CHAO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382372/xgb.png",
      "value": "GoldenBird",
      "label": "GoldenBird (XGB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382380/ymc.png",
      "value": "YamahaCoin",
      "label": "YamahaCoin (YMC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382381/jok.png",
      "value": "JokerCoin",
      "label": "JokerCoin (JOK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382382/gbit.jpg",
      "value": "GravityBit",
      "label": "GravityBit (GBIT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382383/tecoin.png",
      "value": "TeCoin",
      "label": "TeCoin (TEC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382384/bomb.png",
      "value": "BombCoin",
      "label": "BombCoin (BOMB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382388/ride.png",
      "value": "Ride My Car",
      "label": "Ride My Car (RIDE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382389/pivx.png",
      "value": "Private Instant Verified Transaction",
      "label": "Private Instant Verified Transaction (PIVX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382390/ked.png",
      "value": "Klingon Empire Darsek",
      "label": "Klingon Empire Darsek (KED)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382391/coino.png",
      "value": "Coino",
      "label": "Coino (CNO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382392/wealth.png",
      "value": "WealthCoin",
      "label": "WealthCoin (WEALTH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382395/xspec.png",
      "value": "Spectre",
      "label": "Spectre (XSPEC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382397/pepecash.png",
      "value": "Pepe Cash",
      "label": "Pepe Cash (PEPECASH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382399/click.png",
      "value": "Clickcoin",
      "label": "Clickcoin (CLICK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382400/els.png",
      "value": "Elysium",
      "label": "Elysium (ELS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382401/kush.png",
      "value": "KushCoin",
      "label": "KushCoin (KUSH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382403/ely2.png",
      "value": "Eryllium",
      "label": "Eryllium (ERY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382431/plu.png",
      "value": "Pluton",
      "label": "Pluton (PLU)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382432/pres.png",
      "value": "President Trump",
      "label": "President Trump (PRES)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382433/btz.png",
      "value": "BitzCoin",
      "label": "BitzCoin (BTZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382434/opes.png",
      "value": "Opes",
      "label": "Opes (OPES)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350884/waves_1.png",
      "value": "Waves Community Token",
      "label": "Waves Community Token (WCT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382441/ubq.png",
      "value": "Ubiq",
      "label": "Ubiq (UBQ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382442/ratio.png",
      "value": "Ratio",
      "label": "Ratio (RATIO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382466/ban.png",
      "value": "Babes and Nerds",
      "label": "Babes and Nerds (BAN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382467/nice.png",
      "value": "NiceCoin",
      "label": "NiceCoin (NICE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382468/xmf.png",
      "value": "SmurfCoin",
      "label": "SmurfCoin (SMF)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382470/cwxt.png",
      "value": "CryptoWorldXToken",
      "label": "CryptoWorldXToken (CWXT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382505/tech.png",
      "value": "TechCoin",
      "label": "TechCoin (TECH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382506/cir.png",
      "value": "CircuitCoin",
      "label": "CircuitCoin (CIR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382507/lepen.png",
      "value": "LePenCoin",
      "label": "LePenCoin (LEPEN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382508/round.png",
      "value": "RoundCoin",
      "label": "RoundCoin (ROUND)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382577/mar.png",
      "value": "MarijuanaCoin",
      "label": "MarijuanaCoin (MAR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382578/marx.png",
      "value": "MarxCoin",
      "label": "MarxCoin (MARX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382594/tat.png",
      "value": "Tatiana Coin",
      "label": "Tatiana Coin (TAT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382595/haze.png",
      "value": "HazeCoin",
      "label": "HazeCoin (HAZE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382603/prx.png",
      "value": "Printerium",
      "label": "Printerium (PRX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382604/nrc.png",
      "value": "Neurocoin",
      "label": "Neurocoin (NRC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780670/pac.png",
      "value": "PacCoin",
      "label": "PacCoin (PAC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382606/impch.png",
      "value": "Impeach",
      "label": "Impeach (IMPCH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382624/err.png",
      "value": "ErrorCoin",
      "label": "ErrorCoin (ERR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382625/tic.png",
      "value": "TrueInvestmentCoin",
      "label": "TrueInvestmentCoin (TIC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382626/nuke.png",
      "value": "NukeCoin",
      "label": "NukeCoin (NUKE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382628/eoc.png",
      "value": "EveryonesCoin",
      "label": "EveryonesCoin (EOC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382639/sfc.png",
      "value": "Solarflarecoin",
      "label": "Solarflarecoin (SFC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382640/jane.png",
      "value": "JaneCoin",
      "label": "JaneCoin (JANE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382641/para.png",
      "value": "ParanoiaCoin",
      "label": "ParanoiaCoin (PARA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382642/mm.jpg",
      "value": "MasterMint",
      "label": "MasterMint (MM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382649/bxc.jpg",
      "value": "Bitcedi",
      "label": "Bitcedi (BXC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382650/ndoge.png",
      "value": "NinjaDoge",
      "label": "NinjaDoge (NDOGE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382652/zbc.png",
      "value": "Zilbercoin",
      "label": "Zilbercoin (ZBC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382653/mln.png",
      "value": "Melon",
      "label": "Melon (MLN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382654/first.png",
      "value": "FirstCoin",
      "label": "FirstCoin (FRST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383687/pay.png",
      "value": "TenX",
      "label": "TenX (PAY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382656/oro.png",
      "value": "OroCoin",
      "label": "OroCoin (ORO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382657/alex.png",
      "value": "Alexandrite",
      "label": "Alexandrite (ALEX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382658/tbcx.png",
      "value": "TrashBurn",
      "label": "TrashBurn (TBCX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382659/mcar.png",
      "value": "MasterCar",
      "label": "MasterCar (MCAR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382660/ths.png",
      "value": "TechShares",
      "label": "TechShares (THS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382661/aces.png",
      "value": "AcesCoin",
      "label": "AcesCoin (ACES)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382684/uaec.png",
      "value": "United Arab Emirates Coin",
      "label": "United Arab Emirates Coin (UAEC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382685/ea.png",
      "value": "EagleCoin",
      "label": "EagleCoin (EA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382686/pie.png",
      "value": "Persistent Information Exchange",
      "label": "Persistent Information Exchange (PIE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382709/crea.png",
      "value": "CreativeChain",
      "label": "CreativeChain (CREA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382710/wisc.jpg",
      "value": "WisdomCoin",
      "label": "WisdomCoin (WISC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382711/bvc.png",
      "value": "BeaverCoin",
      "label": "BeaverCoin (BVC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382713/find.png",
      "value": "FindCoin",
      "label": "FindCoin (FIND)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382725/mlite.png",
      "value": "MeLite",
      "label": "MeLite (MLITE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382726/stalin.png",
      "value": "StalinCoin",
      "label": "StalinCoin (STALIN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382790/tato1.png",
      "value": "TattooCoin",
      "label": "TattooCoin (TSE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382738/vltc.png",
      "value": "VaultCoin",
      "label": "VaultCoin (VLTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382739/biob.png",
      "value": "BioBar",
      "label": "BioBar (BIOB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382740/swt.jpg",
      "value": "Swarm City Token",
      "label": "Swarm City Token (SWT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382741/pasl.png",
      "value": "Pascal Lite",
      "label": "Pascal Lite (PASL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382761/zer.png",
      "value": "Zero",
      "label": "Zero (ZER)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382762/chat.png",
      "value": "ChatCoin",
      "label": "ChatCoin (CHAT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382763/cdn.png",
      "value": "Canada eCoin",
      "label": "Canada eCoin (CDN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382770/tpay.png",
      "value": "TrollPlay",
      "label": "TrollPlay (TPAY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382771/netko.png",
      "value": "Netko",
      "label": "Netko (NETKO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382772/zoin.png",
      "value": "Zoin",
      "label": "Zoin (ZOI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382937/honey1.png",
      "value": "Honey",
      "label": "Honey (HONEY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382782/mxt.jpg",
      "value": "MartexCoin",
      "label": "MartexCoin (MXT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382783/music.png",
      "value": "Musicoin",
      "label": "Musicoin (MUSIC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382791/dtb.png",
      "value": "Databits",
      "label": "Databits (DTB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382792/veg.png",
      "value": "BitVegan",
      "label": "BitVegan (VEG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382793/mbit.png",
      "value": "Mbitbooks",
      "label": "Mbitbooks (MBIT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382794/volt.png",
      "value": "BitVolt",
      "label": "BitVolt (VOLT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382798/mgo.png",
      "value": "MobileGo",
      "label": "MobileGo (MGO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382799/edg.jpg",
      "value": "Edgeless",
      "label": "Edgeless (EDG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382804/b.png",
      "value": "BankCoin",
      "label": "BankCoin (B@)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382805/best.jpg",
      "value": "BestChain",
      "label": "BestChain (BEST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383907/chc.png",
      "value": "ChainCoin",
      "label": "ChainCoin (CHC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382807/zen.png",
      "value": "Zennies",
      "label": "Zennies (ZENI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382851/planet.png",
      "value": "PlanetCoin",
      "label": "PlanetCoin (PLANET)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382852/duckduckcoin.png",
      "value": "DuckDuckCoin",
      "label": "DuckDuckCoin (DUCK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382853/bnx.png",
      "value": "BnrtxCoin",
      "label": "BnrtxCoin (BNX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382858/bstk.png",
      "value": "BattleStake",
      "label": "BattleStake (BSTK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382859/rns.png",
      "value": "RenosCoin",
      "label": "RenosCoin (RNS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382860/dbix.png",
      "value": "DubaiCoin",
      "label": "DubaiCoin (DBIX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382863/kayi.png",
      "value": "Kayı",
      "label": "Kayı (KAYI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382865/xvp.png",
      "value": "VirtacoinPlus",
      "label": "VirtacoinPlus (XVP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382866/boat.png",
      "value": "Doubloon",
      "label": "Doubloon (BOAT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382867/taj.png",
      "value": "TajCoin",
      "label": "TajCoin (TAJ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382891/imx.png",
      "value": "Impact",
      "label": "Impact (IMX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382887/cjc.png",
      "value": "CryptoJournal",
      "label": "CryptoJournal (CJC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382935/amy.jpg",
      "value": "Amygws",
      "label": "Amygws (AMY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382936/qbt.png",
      "value": "Cubits",
      "label": "Cubits (QBT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382936/src1.png",
      "value": "StarCredits",
      "label": "StarCredits (SRC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382938/eb3.png",
      "value": "EB3coin",
      "label": "EB3coin (EB3)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382937/xve.png",
      "value": "The Vegan Initiative",
      "label": "The Vegan Initiative (XVE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382944/fazz.png",
      "value": "FazzCoin",
      "label": "FazzCoin (FAZZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382945/apt.png",
      "value": "Aptcoin",
      "label": "Aptcoin (APT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382946/blazr.png",
      "value": "BlazerCoin",
      "label": "BlazerCoin (BLAZR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382966/arpa.png",
      "value": "ArpaCoin",
      "label": "ArpaCoin (ARPA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382967/bnb.png",
      "value": "Boats and Bitches",
      "label": "Boats and Bitches (BNB*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382968/uni.png",
      "value": "Universe",
      "label": "Universe (UNI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382993/eco.png",
      "value": "ECOcoin",
      "label": "ECOcoin (ECO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382994/xlr.png",
      "value": "Solaris",
      "label": "Solaris (XLR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382995/dark.png",
      "value": "Dark",
      "label": "Dark (DARK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382995/don.png",
      "value": "DonationCoin",
      "label": "DonationCoin (DON)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913628/mer.png",
      "value": "Mercury",
      "label": "Mercury (MER)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382998/wgo.png",
      "value": "WavesGO",
      "label": "WavesGO (WGO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318418/rlc.png",
      "value": "iEx.ec",
      "label": "iEx.ec (RLC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383003/atms.png",
      "value": "Atmos",
      "label": "Atmos (ATMS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383004/inpay.png",
      "value": "InPay",
      "label": "InPay (INPAY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383046/ett.png",
      "value": "EncryptoTel",
      "label": "EncryptoTel (ETT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383045/wbtc.png",
      "value": "wBTC",
      "label": "wBTC (WBTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383047/visio.png",
      "value": "Visio",
      "label": "Visio (VISIO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383046/hpc.png",
      "value": "HappyCoin",
      "label": "HappyCoin (HPC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383079/got.png",
      "value": "Giotto Coin",
      "label": "Giotto Coin (GOT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383080/cxt.png",
      "value": "Coinonat",
      "label": "Coinonat (CXT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383081/empc.png",
      "value": "EmporiumCoin",
      "label": "EmporiumCoin (EMPC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383083/gnosis-logo.png",
      "value": "Gnosis",
      "label": "Gnosis (GNO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383085/lgd.png",
      "value": "Legends Cryptocurrency",
      "label": "Legends Cryptocurrency (LGD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383085/taas.png",
      "value": "Token as a Service",
      "label": "Token as a Service (TAAS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383089/bucks.png",
      "value": "SwagBucks",
      "label": "SwagBucks (BUCKS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780760/xby.png",
      "value": "XtraBYtes",
      "label": "XtraBYtes (XBY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383107/gup.png",
      "value": "Guppy",
      "label": "Guppy (GUP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383111/mcrn.png",
      "value": "MacronCoin",
      "label": "MacronCoin (MCRN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383112/lunyr-logo.png",
      "value": "Lunyr",
      "label": "Lunyr (LUN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383114/rain.png",
      "value": "Condensate",
      "label": "Condensate (RAIN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383144/wsx.png",
      "value": "WeAreSatoshi",
      "label": "WeAreSatoshi (WSX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383144/wsx.png",
      "value": "IvugeoEvolutionCoin",
      "label": "IvugeoEvolutionCoin (IEC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383145/ims.png",
      "value": "Independent Money System",
      "label": "Independent Money System (IMS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383149/argus.png",
      "value": "ArgusCoin",
      "label": "ArgusCoin (ARGUS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383150/cnt.png",
      "value": "Centurion",
      "label": "Centurion (CNT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383139/lmc.png",
      "value": "LomoCoin",
      "label": "LomoCoin (LMC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383157/tkn.png",
      "value": "TokenCard  ",
      "label": "TokenCard   (TKN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383159/proc.png",
      "value": "ProCurrency",
      "label": "ProCurrency (PROC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383161/xgr.png",
      "value": "GoldReserve",
      "label": "GoldReserve (XGR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383162/wrc.png",
      "value": "WarCoin",
      "label": "WarCoin (WRC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383163/benji.png",
      "value": "BenjiRolls",
      "label": "BenjiRolls (BENJI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383174/hmq.png",
      "value": "Humaniq",
      "label": "Humaniq (HMQ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383948/bcap1.png",
      "value": "Blockchain Capital",
      "label": "Blockchain Capital (BCAP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383196/duo.png",
      "value": "ParallelCoin",
      "label": "ParallelCoin (DUO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383197/rbx.png",
      "value": "RiptoBuX",
      "label": "RiptoBuX (RBX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383234/grw.png",
      "value": "GrowthCoin",
      "label": "GrowthCoin (GRW)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383235/apx.png",
      "value": "Apx",
      "label": "Apx (APX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383236/milo.png",
      "value": "MiloCoin",
      "label": "MiloCoin (MILO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383239/xvs.png",
      "value": "OldV",
      "label": "OldV (OLV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383238/ilc.png",
      "value": "ILCoin",
      "label": "ILCoin (ILC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350884/waves_1.png",
      "value": "MinersReward",
      "label": "MinersReward (MRT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383241/iou1.png",
      "value": "IOU1",
      "label": "IOU1 (IOU)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383242/pzm.jpg",
      "value": "Prizm",
      "label": "Prizm (PZM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383243/phr.jpg",
      "value": "Phreak",
      "label": "Phreak (PHR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383244/ant.png",
      "value": "Aragon",
      "label": "Aragon (ANT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383245/pupa.png",
      "value": "PupaCoin",
      "label": "PupaCoin (PUPA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383245/rice.png",
      "value": "RiceCoin",
      "label": "RiceCoin (RICE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383246/xct.png",
      "value": "C-Bits",
      "label": "C-Bits (XCT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383264/dea.png",
      "value": "Degas Coin",
      "label": "Degas Coin (DEA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383265/red.png",
      "value": "Redcoin",
      "label": "Redcoin (RED)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383266/zse.png",
      "value": "ZSEcoin",
      "label": "ZSEcoin (ZSE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383267/ctic.png",
      "value": "Coinmatic",
      "label": "Coinmatic (CTIC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383283/tap.png",
      "value": "TappingCoin",
      "label": "TappingCoin (TAP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383282/bitok.jpg",
      "value": "BitOKX",
      "label": "BitOKX (BITOK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383324/pbt.png",
      "value": "Primalbase",
      "label": "Primalbase (PBT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383325/muu.png",
      "value": "MilkCoin",
      "label": "MilkCoin (MUU)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383326/inf8.png",
      "value": "Infinium-8",
      "label": "Infinium-8 (INF8)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383327/html5.png",
      "value": "HTML Coin",
      "label": "HTML Coin (HTML5)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383328/mne.png",
      "value": "Minereum",
      "label": "Minereum (MNE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383361/dice.png",
      "value": "Etheroll",
      "label": "Etheroll (DICE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383362/sub.png",
      "value": "Subscriptio",
      "label": "Subscriptio (SUB*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383363/usc.png",
      "value": "Ultimate Secure Cash",
      "label": "Ultimate Secure Cash (USC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383364/dux.png",
      "value": "DuxCoin",
      "label": "DuxCoin (DUX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383365/xps.png",
      "value": "PoisonIvyCoin",
      "label": "PoisonIvyCoin (XPS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383366/eqt.png",
      "value": "EquiTrader",
      "label": "EquiTrader (EQT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383366/insn.png",
      "value": "Insane Coin",
      "label": "Insane Coin (INSN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383370/bat.png",
      "value": "Basic Attention Token",
      "label": "Basic Attention Token (BAT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383371/mat.png",
      "value": "Manet Coin",
      "label": "Manet Coin (MAT*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383372/f16.png",
      "value": "F16Coin",
      "label": "F16Coin (F16)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383381/hams.png",
      "value": "HamsterCoin",
      "label": "HamsterCoin (HAMS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383382/qtum.png",
      "value": "QTUM",
      "label": "QTUM (QTUM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383383/nef.png",
      "value": "NefariousCoin",
      "label": "NefariousCoin (NEF)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383521/bos.png",
      "value": "BOScoin",
      "label": "BOScoin (BOS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383522/qwark.png",
      "value": "Qwark",
      "label": "Qwark (QWARK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383540/iota_logo.png",
      "value": "IOTA",
      "label": "IOTA (IOT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383543/qrl.png",
      "value": "Quantum Resistant Ledger",
      "label": "Quantum Resistant Ledger (QRL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383544/adl.png",
      "value": "Adelphoi",
      "label": "Adelphoi (ADL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383546/ecc.png",
      "value": "E-CurrencyCoin",
      "label": "E-CurrencyCoin (ECC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383547/ptoy.png",
      "value": "Patientory",
      "label": "Patientory (PTOY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383548/xzc.png",
      "value": "ZrCoin",
      "label": "ZrCoin (ZRC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383553/lkk.png",
      "value": "Lykke",
      "label": "Lykke (LKK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14761907/esp.png",
      "value": "Espers",
      "label": "Espers (ESP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383556/mad.png",
      "value": "SatoshiMadness",
      "label": "SatoshiMadness (MAD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383557/dyn.png",
      "value": "Dynamic",
      "label": "Dynamic (DYN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383558/seq.png",
      "value": "Sequence",
      "label": "Sequence (SEQ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383559/mcap.png",
      "value": "MCAP",
      "label": "MCAP (MCAP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383561/myst.png",
      "value": "Mysterium",
      "label": "Mysterium (MYST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383562/veri.png",
      "value": "Veritaseum",
      "label": "Veritaseum (VERI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383564/snm.png",
      "value": "SONM",
      "label": "SONM (SNM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383565/sky.png",
      "value": "Skycoin",
      "label": "Skycoin (SKY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383567/cfi.png",
      "value": "Cofound.it",
      "label": "Cofound.it (CFI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383568/snt.png",
      "value": "Status Network Token",
      "label": "Status Network Token (SNT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383599/avt.png",
      "value": "AventCoin",
      "label": "AventCoin (AVT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383611/cvc.png",
      "value": "Civic",
      "label": "Civic (CVC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383612/ixt.png",
      "value": "iXledger",
      "label": "iXledger (IXT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383613/dent.png",
      "value": "Dent",
      "label": "Dent (DENT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404851/ethos.png",
      "value": "Ethos",
      "label": "Ethos (ETHOS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383620/crs.png",
      "value": "Starta",
      "label": "Starta (STA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383621/tfl.png",
      "value": "True Flip Lottery",
      "label": "True Flip Lottery (TFL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383646/efyt.png",
      "value": "Ergo",
      "label": "Ergo (EFYT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383653/mco.jpg",
      "value": "Monaco",
      "label": "Monaco (MCO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383655/nmr.png",
      "value": "Numerai",
      "label": "Numerai (NMR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383667/adx1.png",
      "value": "AdEx",
      "label": "AdEx (ADX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383669/qau.png",
      "value": "Quantum",
      "label": "Quantum (QAU)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383670/ecob.png",
      "value": "EcoBit",
      "label": "EcoBit (ECOB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383671/polybius.png",
      "value": "Polybius",
      "label": "Polybius (PLBT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383672/usdt.png",
      "value": "Tether",
      "label": "Tether (USDT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383688/ahc.png",
      "value": "Ahoolee",
      "label": "Ahoolee (AHT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383689/atb.png",
      "value": "ATB coin",
      "label": "ATB coin (ATB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383690/tix.png",
      "value": "Blocktix",
      "label": "Blocktix (TIX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383692/compcoin.png",
      "value": "Compcoin",
      "label": "Compcoin (CMP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383694/rvt.png",
      "value": "Rivetz",
      "label": "Rivetz (RVT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383695/hrb.png",
      "value": "Harbour DAO",
      "label": "Harbour DAO (HRB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383697/net1.png",
      "value": "Nimiq Exchange Token",
      "label": "Nimiq Exchange Token (NET*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383698/8bt.png",
      "value": "8 Circuit Studios",
      "label": "8 Circuit Studios (8BT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383699/cdt.png",
      "value": "CoinDash",
      "label": "CoinDash (CDT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383701/dnt.png",
      "value": "district0x",
      "label": "district0x (DNT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383696/sur.png",
      "value": "Suretly",
      "label": "Suretly (SUR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383706/ping1.png",
      "value": "CryptoPing",
      "label": "CryptoPing (PING)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383728/miv.png",
      "value": "MakeItViral",
      "label": "MakeItViral (MIV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383729/bet.png",
      "value": "DAO.casino",
      "label": "DAO.casino (BET*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383730/san.png",
      "value": "Santiment",
      "label": "Santiment (SAN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383731/kin.png",
      "value": "Kin",
      "label": "Kin (KIN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383736/wgr.png",
      "value": "Wagerr",
      "label": "Wagerr (WGR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383737/xel.png",
      "value": "Elastic",
      "label": "Elastic (XEL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383732/nvst.png",
      "value": "NVO",
      "label": "NVO (NVST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383738/fun.png",
      "value": "FunFair",
      "label": "FunFair (FUN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383739/func.png",
      "value": "FunCoin",
      "label": "FunCoin (FUNC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383741/pqt.png",
      "value": "PAquarium",
      "label": "PAquarium (PQT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383742/wtt.png",
      "value": "Giga Watt",
      "label": "Giga Watt (WTT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383743/mtl.png",
      "value": "Metal",
      "label": "Metal (MTL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383746/myb.png",
      "value": "MyBit",
      "label": "MyBit (MYB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383747/ppt.png",
      "value": "Populous",
      "label": "Populous (PPT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383748/snc.png",
      "value": "SunContract",
      "label": "SunContract (SNC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383750/star1.png",
      "value": "Starbase",
      "label": "Starbase (STAR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383753/cor.png",
      "value": "Corion",
      "label": "Corion (COR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383754/xrl.png",
      "value": "Rialto.AI",
      "label": "Rialto.AI (XRL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383755/oroc.png",
      "value": "Orocrypt",
      "label": "Orocrypt (OROC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383756/oax.png",
      "value": "OpenANX",
      "label": "OpenANX (OAX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383759/mbi.png",
      "value": "Monster Byte Inc",
      "label": "Monster Byte Inc (MBI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383761/dim.png",
      "value": "DIMCOIN",
      "label": "DIMCOIN (DIM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383762/ggs.png",
      "value": "Gilgam",
      "label": "Gilgam (GGS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746452/dna.png",
      "value": "Encrypgen",
      "label": "Encrypgen (DNA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383764/fyn.png",
      "value": "FundYourselfNow",
      "label": "FundYourselfNow (FYN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383765/fnd.png",
      "value": "FundRequest",
      "label": "FundRequest (FND)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383767/dcy.png",
      "value": "Dinastycoin",
      "label": "Dinastycoin (DCY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383769/cft.png",
      "value": "CryptoForecast",
      "label": "CryptoForecast (CFT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383770/dnr.png",
      "value": "Denarius",
      "label": "Denarius (DNR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383772/dp.png",
      "value": "DigitalPrice",
      "label": "DigitalPrice (DP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383773/vuc.png",
      "value": "Virta Unique Coin",
      "label": "Virta Unique Coin (VUC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383774/btpl.png",
      "value": "Bitcoin Planet",
      "label": "Bitcoin Planet (BTPL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383775/unify.png",
      "value": "Unify",
      "label": "Unify (UNIFY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383776/ipc.png",
      "value": "ImperialCoin",
      "label": "ImperialCoin (IPC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383777/brit.png",
      "value": "BritCoin",
      "label": "BritCoin (BRIT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383778/ammo.png",
      "value": "Ammo Rewards",
      "label": "Ammo Rewards (AMMO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383779/socc.png",
      "value": "SocialCoin",
      "label": "SocialCoin (SOCC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383781/mass.png",
      "value": "Mass.Cloud",
      "label": "Mass.Cloud (MASS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383782/lato.png",
      "value": "LAToken",
      "label": "LAToken (LA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383783/iml.png",
      "value": "IMMLA",
      "label": "IMMLA (IML)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383784/xuc.png",
      "value": "Exchange Union",
      "label": "Exchange Union (XUC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383789/guns.png",
      "value": "GeoFunders",
      "label": "GeoFunders (GUNS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318127/ift.png",
      "value": "InvestFeed",
      "label": "InvestFeed (IFT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780714/cat.png",
      "value": "BitClave",
      "label": "BitClave (CAT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383793/syc.png",
      "value": "SynchroCoin",
      "label": "SynchroCoin (SYC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383794/ind.png",
      "value": "Indorse",
      "label": "Indorse (IND)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383796/aht.png",
      "value": "Bowhead Health",
      "label": "Bowhead Health (AHT*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383797/tribe.jpg",
      "value": "TribeToken",
      "label": "TribeToken (TRIBE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383799/zrx.png",
      "value": "0x",
      "label": "0x (ZRX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383800/tnt.png",
      "value": "Tierion",
      "label": "Tierion (TNT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383801/pst.png",
      "value": "Presearch",
      "label": "Presearch (PRE*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383802/coss.png",
      "value": "COSS",
      "label": "COSS (COSS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383803/storm.jpg",
      "value": "Storm",
      "label": "Storm (STORM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20422/sjcx.png",
      "value": "Storj",
      "label": "Storj (STORJ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383814/omisego.png",
      "value": "OmiseGo",
      "label": "OmiseGo (OMG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383817/1qrfuod6_400x400.jpg",
      "value": "Octanox",
      "label": "Octanox (OTX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383816/eqb.png",
      "value": "Equibit",
      "label": "Equibit (EQB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318263/voise.png",
      "value": "Voise",
      "label": "Voise (VOISE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318348/etbs.png",
      "value": "EthBits",
      "label": "EthBits (ETBS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383821/cvcoin.png",
      "value": "Crypviser",
      "label": "Crypviser (CVCOIN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383822/drp.png",
      "value": "DCORP",
      "label": "DCORP (DRP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383824/arc.png",
      "value": "ArcticCoin",
      "label": "ArcticCoin (ARC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383826/bog.png",
      "value": "Bogcoin",
      "label": "Bogcoin (BOG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383827/ndc.png",
      "value": "NeverDie",
      "label": "NeverDie (NDC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383828/poe.png",
      "value": "Po.et",
      "label": "Po.et (POE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383829/adt.png",
      "value": "AdToken",
      "label": "AdToken (ADT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383836/ae.png",
      "value": "Aeternity",
      "label": "Aeternity (AE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383837/uet.png",
      "value": "Useless Ethereum Token",
      "label": "Useless Ethereum Token (UET)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383838/part.png",
      "value": "Particl",
      "label": "Particl (PART)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383839/agrs.png",
      "value": "Agoras Token",
      "label": "Agoras Token (AGRS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383825/beach.png",
      "value": "BeachCoin",
      "label": "BeachCoin (SAND)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383840/xai.png",
      "value": "AICoin",
      "label": "AICoin (XAI*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14543970/das.png",
      "value": "DAS",
      "label": "DAS (DAS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383846/adst.png",
      "value": "Adshares",
      "label": "Adshares (ADST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383848/bcat1.png",
      "value": "BlockCAT",
      "label": "BlockCAT (CAT*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383849/xcj.png",
      "value": "CoinJob",
      "label": "CoinJob (XCJ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383852/rkc.png",
      "value": "Royal Kingdom Coin",
      "label": "Royal Kingdom Coin (RKC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746543/nlc2.png",
      "value": "NoLimitCoin",
      "label": "NoLimitCoin (NLC2)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383860/linda.png",
      "value": "Linda",
      "label": "Linda (LINDA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383861/spn.png",
      "value": "Spoon",
      "label": "Spoon (SPN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383862/king.png",
      "value": "King93",
      "label": "King93 (KING)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383863/ancp.png",
      "value": "Anacrypt",
      "label": "Anacrypt (ANCP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383864/rcc.png",
      "value": "Reality Clash",
      "label": "Reality Clash (RCC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383851/roots.png",
      "value": "RootProject",
      "label": "RootProject (ROOTS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383865/snk.png",
      "value": "Sosnovkino",
      "label": "Sosnovkino (SNK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383869/cabs.png",
      "value": "CryptoABS",
      "label": "CryptoABS (CABS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383873/opt.png",
      "value": "Opus",
      "label": "Opus (OPT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383875/znt.png",
      "value": "OpenZen",
      "label": "OpenZen (ZNT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383878/bitsd.png",
      "value": "Bits Digit",
      "label": "Bits Digit (BITSD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383879/ivetpxdq_400x400.jpg",
      "value": "LeviarCoin",
      "label": "LeviarCoin (XLC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383880/dsb_amky_400x400.jpg",
      "value": "Skincoin",
      "label": "Skincoin (SKIN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383881/c9fobrlr_400x400.jpg",
      "value": "Mothership",
      "label": "Mothership (MSP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383882/hite.png",
      "value": "HireMatch",
      "label": "HireMatch (HIRE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383883/bbt.png",
      "value": "BrickBlock",
      "label": "BrickBlock (BBT*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383884/rise.png",
      "value": "REAL",
      "label": "REAL (REAL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383890/dfbt.png",
      "value": "DentalFix",
      "label": "DentalFix (DFBT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383891/eq.png",
      "value": "EQUI",
      "label": "EQUI (EQ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383892/wolk.png",
      "value": "Wolk",
      "label": "Wolk (WLK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383893/vib.png",
      "value": "Viberate",
      "label": "Viberate (VIB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383894/onion.png",
      "value": "DeepOnion",
      "label": "DeepOnion (ONION)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383895/btx.png",
      "value": "Bitcore",
      "label": "Bitcore (BTX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383896/46b-uaba_400x400.jpg",
      "value": "iDice",
      "label": "iDice (ICE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383898/xid.jpg",
      "value": "Sphre AIR",
      "label": "Sphre AIR (XID)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383899/gcn.png",
      "value": "GCoin",
      "label": "GCoin (GCN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383901/cosmos.jpg",
      "value": "Cosmos",
      "label": "Cosmos (ATOM*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383903/mana.png",
      "value": "Decentraland",
      "label": "Decentraland (MANA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383904/icoo.jpg",
      "value": "ICO OpenLedger",
      "label": "ICO OpenLedger (ICOO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383905/tme.png",
      "value": "Timereum",
      "label": "Timereum (TME)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383906/smart.png",
      "value": "SmartCash",
      "label": "SmartCash (SMART)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350710/sigt.png",
      "value": "Signatum",
      "label": "Signatum (SIGT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383910/onx.png",
      "value": "Onix",
      "label": "Onix (ONX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383911/coe.png",
      "value": "CoEval",
      "label": "CoEval (COE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318225/arena.png",
      "value": "Arena",
      "label": "Arena (ARENA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383913/wink.png",
      "value": "Wink",
      "label": "Wink (WINK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383915/cream.png",
      "value": "Cream",
      "label": "Cream (CRM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383919/bch.jpg",
      "value": "Bitcoin Cash",
      "label": "Bitcoin Cash / BCC (BCH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383920/dgt.png",
      "value": "DigiPulse",
      "label": "DigiPulse (DGPT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383921/mobi.png",
      "value": "Mobius",
      "label": "Mobius (MOBI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383922/csno.png",
      "value": "BitDice",
      "label": "BitDice (CSNO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383929/kick.png",
      "value": "KickCoin",
      "label": "KickCoin (KICK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383933/sdao.png",
      "value": "Solar DAO",
      "label": "Solar DAO (SDAO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383946/stx.png",
      "value": "Stox",
      "label": "Stox (STX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383947/bnb.png",
      "value": "Binance Coin",
      "label": "Binance Coin (BNB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383950/core.png",
      "value": "Core Group Asset",
      "label": "Core Group Asset (CORE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383953/kencoin.png",
      "value": "Kencoin",
      "label": "Kencoin (KCN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383954/qvt.png",
      "value": "Qvolta",
      "label": "Qvolta (QVT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383955/tie.png",
      "value": "Ties Network",
      "label": "Ties Network (TIE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383956/aut.png",
      "value": "Autoria",
      "label": "Autoria (AUT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383957/ctt.png",
      "value": "CodeTract",
      "label": "CodeTract (CTT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383973/mny.png",
      "value": "Monkey",
      "label": "Monkey (MNY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383976/mth.png",
      "value": "Monetha",
      "label": "Monetha (MTH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383980/ccc.png",
      "value": "CCCoin",
      "label": "CCCoin (CCC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383983/umb.png",
      "value": "Umbrella Coin",
      "label": "Umbrella Coin (UMC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383984/bmxt.png",
      "value": "Bitmxittz",
      "label": "Bitmxittz (BMXT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383858/neo.jpg",
      "value": "Gas",
      "label": "Gas (GAS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383987/fil.png",
      "value": "FileCoin",
      "label": "FileCoin (FIL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383989/ocl.png",
      "value": "Oceanlab",
      "label": "Oceanlab (OCL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383991/bnc.png",
      "value": "Benjacoin",
      "label": "Benjacoin (BNC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383992/tom.png",
      "value": "Tomahawkcoin",
      "label": "Tomahawkcoin (TOM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383996/btm.png",
      "value": "Bytom",
      "label": "Bytom (BTM*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383997/xas.png",
      "value": "Asch",
      "label": "Asch (XAS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383998/sx.png",
      "value": "SMNX",
      "label": "SMNX (SMNX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384001/delta.png",
      "value": "Agrello Delta",
      "label": "Agrello Delta (DLT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384009/mrv.png",
      "value": "Macroverse",
      "label": "Macroverse (MRV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384010/mbrs.png",
      "value": "Embers",
      "label": "Embers (MBRS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384011/sub1.png",
      "value": "Substratum Network",
      "label": "Substratum Network (SUB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384013/met1.png",
      "value": "Memessenger",
      "label": "Memessenger (MET)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384016/nebl.png",
      "value": "Neblio",
      "label": "Neblio (NEBL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384018/pgl.png",
      "value": "Prospectors",
      "label": "Prospectors (PGL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384017/mcc.png",
      "value": "MonacoCoin",
      "label": "MonacoCoin (XMCC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384019/auth.png",
      "value": "Authoreon",
      "label": "Authoreon (AUTH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384020/cpp.png",
      "value": "Cash Poker Pro",
      "label": "Cash Poker Pro (CASH*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384025/dtct.png",
      "value": "DetectorToken",
      "label": "DetectorToken (DTCT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384029/ctr.png",
      "value": "Centra",
      "label": "Centra (CTR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383982/wnet1.png",
      "value": "Wavesnode.net",
      "label": "Wavesnode.net (WNET)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384033/prg.png",
      "value": "Paragon",
      "label": "Paragon (PRG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384039/thnx.jpg",
      "value": "ThankYou",
      "label": "ThankYou (THNX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384040/worm.png",
      "value": "HealthyWorm",
      "label": "HealthyWorm (WORM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384043/fuck.png",
      "value": "Fuck Token",
      "label": "Fuck Token (FUCK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384015/vent.png",
      "value": "Veredictum",
      "label": "Veredictum (VNT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384045/sift.jpg",
      "value": "Smart Investment Fund Token",
      "label": "Smart Investment Fund Token (SIFT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384048/iwt.png",
      "value": "IwToken",
      "label": "IwToken (IWT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384049/jdc.png",
      "value": "JustDatingSite",
      "label": "JustDatingSite (JDC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384050/itt.png",
      "value": "Intelligent Trading Technologies",
      "label": "Intelligent Trading Technologies (ITT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383850/evx.png",
      "value": "Everex",
      "label": "Everex (EVX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383968/icos1.png",
      "value": "ICOBox",
      "label": "ICOBox (ICOS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384024/pix.png",
      "value": "Lampix",
      "label": "Lampix (PIX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384051/medi.png",
      "value": "MediBond",
      "label": "MediBond (MEDI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350692/hgt.jpg",
      "value": "Hello Gold",
      "label": "Hello Gold (HGT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350693/lta.png",
      "value": "Litra",
      "label": "Litra (LTA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350694/nimfa.jpg",
      "value": "Nimfamoney",
      "label": "Nimfamoney (NIMFA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350695/scor.jpg",
      "value": "Scorista",
      "label": "Scorista (SCOR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350696/mls.png",
      "value": "CPROP",
      "label": "CPROP (MLS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350699/kex.png",
      "value": "KexCoin",
      "label": "KexCoin (KEX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350700/cobin.png",
      "value": "Cobinhood",
      "label": "Cobinhood (COB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350701/bro.png",
      "value": "Bitradio",
      "label": "Bitradio (BRO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350702/minex.png",
      "value": "Minex",
      "label": "Minex (MINEX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350703/atlant.png",
      "value": "ATLANT",
      "label": "ATLANT (ATL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350704/ari.png",
      "value": "BeckSang",
      "label": "BeckSang (ARI*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350711/mag.png",
      "value": "Magos",
      "label": "Magos (MAG*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350712/dft.png",
      "value": "Draftcoin",
      "label": "Draftcoin (DFT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318129/ven.png",
      "value": "Vechain",
      "label": "Vechain (VEN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350717/utrust.png",
      "value": "Utrust",
      "label": "Utrust (UTK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350724/lat.png",
      "value": "Latium",
      "label": "Latium (LAT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350725/soj.png",
      "value": "Sojourn Coin",
      "label": "Sojourn Coin (SOJ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350726/hdg.png",
      "value": "Hedge Token",
      "label": "Hedge Token (HDG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350727/stcn.png",
      "value": "Stakecoin",
      "label": "Stakecoin (STCN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350728/sqp.png",
      "value": "SqPay",
      "label": "SqPay (SQP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350737/riya.png",
      "value": "Etheriya",
      "label": "Etheriya (RIYA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350738/lnk.png",
      "value": "Ethereum.Link",
      "label": "Ethereum.Link (LNK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350739/amb.png",
      "value": "Ambrosus",
      "label": "Ambrosus (AMB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350742/wan.jpg",
      "value": "Wanchain",
      "label": "Wanchain (WAN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350745/mntp.png",
      "value": "GoldMint",
      "label": "GoldMint (MNT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350746/altc.png",
      "value": "AltoCar",
      "label": "AltoCar (ALTOCAR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350747/credo.jpg",
      "value": "Credo",
      "label": "Credo (CFT*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350748/blx.png",
      "value": "Blockchain Index",
      "label": "Blockchain Index (BLX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350750/bou.jpg",
      "value": "Boulle",
      "label": "Boulle (BOU)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350753/oxy.png",
      "value": "Oxycoin",
      "label": "Oxycoin (OXY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350755/ttt.jpg",
      "value": "Tap Project",
      "label": "Tap Project (TTT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350756/amt.jpg",
      "value": "Acumen",
      "label": "Acumen (AMT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350762/nyc.png",
      "value": "NewYorkCoin",
      "label": "NewYorkCoin (NYC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350763/lbtc.png",
      "value": "LiteBitcoin",
      "label": "LiteBitcoin (LBTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350764/fraz.png",
      "value": "FrazCoin",
      "label": "FrazCoin (FRAZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350765/emt.png",
      "value": "EasyMine",
      "label": "EasyMine (EMT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350766/gxc.png",
      "value": "Gx Coin",
      "label": "Gx Coin (GXC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350768/hbt.png",
      "value": "Hubiit",
      "label": "Hubiit (HBT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350770/krone.png",
      "value": "Kronecoin",
      "label": "Kronecoin (KRONE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350771/srt.png",
      "value": "Scrypto",
      "label": "Scrypto (SRT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350772/ava.png",
      "value": "Avalon",
      "label": "Avalon (AVA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350775/bt.png",
      "value": "BuildTeam",
      "label": "BuildTeam (BT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350776/acc.jpg",
      "value": "AdCoin",
      "label": "AdCoin (ACC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913465/ar.png",
      "value": "Ar.cash",
      "label": "Ar.cash (AR*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350780/z2.png",
      "value": "Z2 Coin",
      "label": "Z2 Coin (Z2)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350783/linx.png",
      "value": "Linx",
      "label": "Linx (LINX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350784/xcxt.png",
      "value": "CoinonatX",
      "label": "CoinonatX (XCXT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350786/good.png",
      "value": "GoodCoin",
      "label": "GoodCoin (GOOD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350788/scl.png",
      "value": "Social Nexus",
      "label": "Social Nexus (SCL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350789/trv.png",
      "value": "Travel Coin",
      "label": "Travel Coin (TRV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350790/crtm.jpg",
      "value": "Cryptum",
      "label": "Cryptum (CRTM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350791/eon.jpg",
      "value": "Exscudo",
      "label": "Exscudo (EON)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350792/pst.jpg",
      "value": "Primas",
      "label": "Primas (PST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350793/mtx.png",
      "value": "Matryx",
      "label": "Matryx (MTX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/11417639/enjt.png",
      "value": "Enjin Coin",
      "label": "Enjin Coin (ENJ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/11417632/cnx.png",
      "value": "Cryptonex",
      "label": "Cryptonex (CNX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/11417638/drp.png",
      "value": "Dropcoin",
      "label": "Dropcoin (DRC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/11999072/fuel.png",
      "value": "Etherparty",
      "label": "Etherparty (FUEL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/11999076/ace.png",
      "value": "TokenStars",
      "label": "TokenStars (ACE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12317959/wtc.png",
      "value": "Waltonchain",
      "label": "Waltonchain (WTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12317960/brx.png",
      "value": "Breakout Stake",
      "label": "Breakout Stake (BRX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12317962/xuc.png",
      "value": "U.CASH",
      "label": "U.CASH (UCASH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12317963/wrt.png",
      "value": "WRTcoin",
      "label": "WRTcoin (WRT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12317975/omes.png",
      "value": "Ormeus Coin",
      "label": "Ormeus Coin (ORME)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12317976/deep.png",
      "value": "Deep Gold",
      "label": "Deep Gold (DEEP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12317977/tmt.png",
      "value": "ToTheMoon",
      "label": "ToTheMoon (TMT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12317979/cct1.png",
      "value": "Crystal Clear Token ",
      "label": "Crystal Clear Token  (CCT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12317980/wish.png",
      "value": "Wish Finance",
      "label": "Wish Finance (WSH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318005/arna.png",
      "value": "ARNA Panacea",
      "label": "ARNA Panacea (ARNA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318006/bac.png",
      "value": "AB-Chain",
      "label": "AB-Chain (ABC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318007/prp.png",
      "value": "Papyrus",
      "label": "Papyrus (PRP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318008/bmc.png",
      "value": "Blackmoon Crypto",
      "label": "Blackmoon Crypto (BMC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318013/skr.png",
      "value": "Skrilla Token",
      "label": "Skrilla Token (SKR*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318014/3des.png",
      "value": "3DES",
      "label": "3DES (3DES)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318033/pyn.png",
      "value": "Paycentos",
      "label": "Paycentos (PYN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318035/logo_500x500.png",
      "value": "Kapu",
      "label": "Kapu (KAPU)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318034/sense.png",
      "value": "Sense Token",
      "label": "Sense Token (SENSE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/15887416/capp.png",
      "value": "Cappasity",
      "label": "Cappasity (CAPP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318044/vee.png",
      "value": "BLOCKv",
      "label": "BLOCKv (VEE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318045/fc.png",
      "value": "Facecoin",
      "label": "Facecoin (FC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318046/rnc.png",
      "value": "Ripio",
      "label": "Ripio (RCN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318047/nrn.png",
      "value": "Doc.ai Neuron",
      "label": "Doc.ai Neuron (NRN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318064/evc.png",
      "value": "Eventchain",
      "label": "Eventchain (EVC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318078/link.png",
      "value": "ChainLink",
      "label": "ChainLink (LINK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318082/eiboo.png",
      "value": "Eidoo",
      "label": "Eidoo (EDO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318083/atkn.png",
      "value": "A-Token",
      "label": "A-Token (ATKN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318084/knc.png",
      "value": "Kyber Network",
      "label": "Kyber Network (KNC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318085/rustbits.png",
      "value": "Rustbits",
      "label": "Rustbits (RUSTBITS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318086/rex.png",
      "value": "REX",
      "label": "REX (REX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318087/ethd.png",
      "value": "Ethereum Dark",
      "label": "Ethereum Dark (ETHD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318088/sumo.png",
      "value": "Sumokoin",
      "label": "Sumokoin (SUMO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318089/trx.png",
      "value": "Tronix",
      "label": "Tronix (TRX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318091/8s.png",
      "value": "Elite 888",
      "label": "Elite 888 (8S)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318092/h2o.png",
      "value": "Hydrominer",
      "label": "Hydrominer (H2O)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318093/tkt.png",
      "value": "Crypto Tickets",
      "label": "Crypto Tickets (TKT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318096/rhea.png",
      "value": "Rhea",
      "label": "Rhea (RHEA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318097/art.png",
      "value": "Maecenas",
      "label": "Maecenas (ART)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318099/drt.png",
      "value": "DomRaider",
      "label": "DomRaider (DRT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318100/snov.png",
      "value": "Snovio",
      "label": "Snovio (SNOV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318109/dtt.png",
      "value": "DreamTeam Token",
      "label": "DreamTeam Token (DTT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318118/mtn.png",
      "value": "TrackNetToken",
      "label": "TrackNetToken (MTN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318119/stockbet.png",
      "value": "StockBet",
      "label": "StockBet (STOCKBET)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318124/plm.jpg",
      "value": "Algo.Land",
      "label": "Algo.Land (PLM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318125/fdc.jpg",
      "value": "FoodCoin",
      "label": "FoodCoin (FDC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350744/salt.jpg",
      "value": "Salt Lending",
      "label": "Salt Lending (SALT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318128/snd.png",
      "value": "Sandcoin",
      "label": "Sandcoin (SND)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318134/xp.png",
      "value": "XP",
      "label": "XP (XP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318135/lrc.png",
      "value": "Loopring",
      "label": "Loopring (LRC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318137/hsr.png",
      "value": "Hshare",
      "label": "Hshare (HSR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318141/gla.png",
      "value": "Gladius",
      "label": "Gladius (GLA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318142/zna.png",
      "value": "Zenome",
      "label": "Zenome (ZNA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318143/ezm.png",
      "value": "EZMarket",
      "label": "EZMarket (EZM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318145/odn.png",
      "value": "Obsidian",
      "label": "Obsidian (ODN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318144/poll.png",
      "value": "ClearPoll",
      "label": "ClearPoll (POLL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318149/mtk.png",
      "value": "Moya Token",
      "label": "Moya Token (MTK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318166/gjc.png",
      "value": "Global Jobcoin",
      "label": "Global Jobcoin (GJC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318168/ocfkmb0t_400x400.jpg",
      "value": "Wi Coin",
      "label": "Wi Coin (WIC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318169/web.png",
      "value": "Webcoin",
      "label": "Webcoin (WEB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318182/wandxlogo_new1.png",
      "value": "WandX",
      "label": "WandX (WAND)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318172/elix.png",
      "value": "Elixir",
      "label": "Elixir (ELIX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318175/ebtc.png",
      "value": "eBitcoin",
      "label": "eBitcoin (EBTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318176/hac.jpg",
      "value": "Hackspace Capital",
      "label": "Hackspace Capital (HAC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318177/ada.png",
      "value": "Cardano",
      "label": "Cardano (ADA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318178/yoyow.png",
      "value": "Yoyow",
      "label": "Yoyow (YOYOW)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318179/rec.png",
      "value": "Regalcoin",
      "label": "Regalcoin (REC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318191/bis.png",
      "value": "Bismuth",
      "label": "Bismuth (BIS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318188/opp.png",
      "value": "Opporty",
      "label": "Opporty (OPP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318189/rock.png",
      "value": "Ice Rock Mining",
      "label": "Ice Rock Mining (ROCK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318192/icx.png",
      "value": "ICON Project",
      "label": "ICON Project (ICX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318194/vsx.png",
      "value": "Vsync",
      "label": "Vsync (VSX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12317980/wish.png",
      "value": "WishFinance",
      "label": "WishFinance (WISH*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318206/flash.png",
      "value": "FLASH coin",
      "label": "FLASH coin (FLASH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318408/btcz.png",
      "value": "BitcoinZ",
      "label": "BitcoinZ (BTCZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318215/czc.png",
      "value": "Crazy Coin",
      "label": "Crazy Coin (CZC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318216/ppp.png",
      "value": "PayPie",
      "label": "PayPie (PPP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318217/guess.png",
      "value": "Peerguess",
      "label": "Peerguess (GUESS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318223/etp.png",
      "value": "Metaverse",
      "label": "Metaverse (ETP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318222/cnxasterisco.png",
      "value": "Cryptonetix",
      "label": "Cryptonetix (CIX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318229/bac.png",
      "value": "LakeBanker",
      "label": "LakeBanker (BAC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318230/flik.png",
      "value": "FLiK",
      "label": "FLiK (FLIK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318231/trip.png",
      "value": "Trippki",
      "label": "Trippki (TRIP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318238/mbt.png",
      "value": "Multibot",
      "label": "Multibot (MBT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318244/jvy.png",
      "value": "Javvy",
      "label": "Javvy (JVY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318247/alis.png",
      "value": "ALISmedia",
      "label": "ALISmedia (ALIS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318249/lev.png",
      "value": "Leverj",
      "label": "Leverj (LEV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318258/arbi.png",
      "value": "Arbi",
      "label": "Arbi (ARBI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318259/elt.png",
      "value": "Eloplay",
      "label": "Eloplay (ELT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318260/req.png",
      "value": "Request Network",
      "label": "Request Network (REQ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318261/arn.png",
      "value": "Aeron",
      "label": "Aeron (ARN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318265/dat.png",
      "value": "Datum",
      "label": "Datum (DAT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318267/vibe.png",
      "value": "VIBEHub",
      "label": "VIBEHub (VIBE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318268/rok.png",
      "value": "Rockchain",
      "label": "Rockchain (ROK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318269/xred.png",
      "value": "X Real Estate Development",
      "label": "X Real Estate Development (XRED)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318271/day.png",
      "value": "Chronologic",
      "label": "Chronologic (DAY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318279/ast.png",
      "value": "AirSwap",
      "label": "AirSwap (AST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318280/flip.png",
      "value": "Gameflip",
      "label": "Gameflip (FLP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318282/hxt.png",
      "value": "HextraCoin",
      "label": "HextraCoin (HXT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318283/cnd.png",
      "value": "Cindicator",
      "label": "Cindicator (CND)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318284/vrt.png",
      "value": "Prosense.tv",
      "label": "Prosense.tv (VRP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318286/ntm.png",
      "value": "NetM",
      "label": "NetM (NTM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318285/tzc.png",
      "value": "TrezarCoin",
      "label": "TrezarCoin (TZC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318287/eng.png",
      "value": "Enigma",
      "label": "Enigma (ENG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318289/mci.png",
      "value": "Musiconomi",
      "label": "Musiconomi (MCI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318290/wax.png",
      "value": "Worldwide Asset eXchange",
      "label": "Worldwide Asset eXchange (WAX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318291/air.png",
      "value": "AirToken",
      "label": "AirToken (AIR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318293/nto.png",
      "value": "Fujinto",
      "label": "Fujinto (NTO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318294/atcc.png",
      "value": "ATC Coin",
      "label": "ATC Coin (ATCC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318295/kolion.png",
      "value": "Kolion",
      "label": "Kolion (KOLION)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318298/wild.png",
      "value": "Wild Crypto",
      "label": "Wild Crypto (WILD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318300/eltc2.png",
      "value": "eLTC",
      "label": "eLTC (ELTC2)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318299/ilct.png",
      "value": "ILCoin Token",
      "label": "ILCoin Token (ILCT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318301/powr.png",
      "value": "Power Ledger",
      "label": "Power Ledger (POWR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318305/ryz.png",
      "value": "Anryze",
      "label": "Anryze (RYZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318306/gxc.png",
      "value": "GenXCoin",
      "label": "GenXCoin (GXC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318324/ter.png",
      "value": "TerraNovaCoin",
      "label": "TerraNovaCoin (TER)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318323/xcs.png",
      "value": "CybCSec Coin",
      "label": "CybCSec Coin (XCS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318325/bq.png",
      "value": "Bitqy",
      "label": "Bitqy (BQ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913426/ptc.png",
      "value": "Propthereum",
      "label": "Propthereum (PTC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318331/wabi.png",
      "value": "WaBi",
      "label": "WaBi (WABI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318332/evr.png",
      "value": "Everus",
      "label": "Everus (EVR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318334/toacoin.png",
      "value": "TOA Coin",
      "label": "TOA Coin (TOA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318336/mnz.png",
      "value": "Monaize",
      "label": "Monaize (MNZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318337/vivo.png",
      "value": "VIVO Coin",
      "label": "VIVO Coin (VIVO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318339/rpx.png",
      "value": "Red Pulse",
      "label": "Red Pulse (RPX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318340/mda.png",
      "value": "Moeda",
      "label": "Moeda (MDA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318341/zsc.png",
      "value": "Zeusshield",
      "label": "Zeusshield (ZSC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318345/aurs.png",
      "value": "Aureus",
      "label": "Aureus (AURS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318349/playkey.png",
      "value": "Playkey",
      "label": "Playkey (PKT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318355/inxt.png",
      "value": "Internxt",
      "label": "Internxt (INXT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318356/ats.png",
      "value": "Authorship",
      "label": "Authorship (ATS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318357/rgc.png",
      "value": "RG Coin",
      "label": "RG Coin (RGC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318358/ebet.png",
      "value": "EthBet",
      "label": "EthBet (EBET)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318360/r.png",
      "value": "Revain",
      "label": "Revain (R)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318362/mod.png",
      "value": "Modum",
      "label": "Modum (MOD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318363/rup.jpg",
      "value": "Rupee",
      "label": "Rupee (RUP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318368/bon.png",
      "value": "Bonpay",
      "label": "Bonpay (BON)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318370/app.png",
      "value": "AppCoins",
      "label": "AppCoins (APPC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318372/whl.png",
      "value": "WhaleCoin",
      "label": "WhaleCoin (WHL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318377/btg.png",
      "value": "Bitcoin Gold",
      "label": "Bitcoin Gold (BTG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318378/etg.png",
      "value": "Ethereum Gold",
      "label": "Ethereum Gold (ETG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318379/women.png",
      "value": "WomenCoin",
      "label": "WomenCoin (WOMEN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318380/may.png",
      "value": "Theresa May Coin",
      "label": "Theresa May Coin (MAY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318381/rndr.png",
      "value": "Render Token",
      "label": "Render Token (RNDR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318382/eddie.png",
      "value": "Eddie coin",
      "label": "Eddie coin (EDDIE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318383/sct.png",
      "value": "Soma",
      "label": "Soma (SCT*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318384/namo.png",
      "value": "NamoCoin",
      "label": "NamoCoin (NAMO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318389/kcs.png",
      "value": "Kucoin",
      "label": "Kucoin (KCS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318407/blue.png",
      "value": "Ethereum Blue",
      "label": "Ethereum Blue (BLUE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318412/fllw.png",
      "value": "Follow Coin",
      "label": "Follow Coin (FLLW)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318413/wyr.png",
      "value": "Wyrify",
      "label": "Wyrify (WYR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318414/vzt.png",
      "value": "Vezt",
      "label": "Vezt (VZT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318419/indi.png",
      "value": "IndiCoin",
      "label": "IndiCoin (INDI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14543968/pirl.png",
      "value": "Pirl",
      "label": "Pirl (PIRL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14543971/ecash.png",
      "value": "Ethereum Cash",
      "label": "Ethereum Cash (ECASH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746490/drgn.png",
      "value": "Dragonchain",
      "label": "Dragonchain (DRGN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14761889/odmcoin.png",
      "value": "ODMCoin",
      "label": "ODMCoin (ODMC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14761917/ctst.png",
      "value": "CyberTrust",
      "label": "CyberTrust (CABS*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14761903/dtr.png",
      "value": "Dynamic Trading Rights",
      "label": "Dynamic Trading Rights (DTR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14761909/tkr.png",
      "value": "CryptoInsight",
      "label": "CryptoInsight (TKR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14761914/elite.png",
      "value": "EthereumLite",
      "label": "EthereumLite (ELITE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14761915/xios.png",
      "value": "Xios",
      "label": "Xios (XIOS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14761916/dovu.png",
      "value": "DOVU",
      "label": "DOVU (DOVU)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14761932/electroneum.png",
      "value": "Electroneum",
      "label": "Electroneum (ETN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14761937/ave.png",
      "value": "Avesta",
      "label": "Avesta (AVE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14761938/xnn.png",
      "value": "Xenon",
      "label": "Xenon (XNN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14761939/btdx.png",
      "value": "Bitcloud 2.0",
      "label": "Bitcloud 2.0 (BTDX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14761940/loan.png",
      "value": "Lendoit",
      "label": "Lendoit (LOAN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14761946/zab.png",
      "value": "ZABERcoin",
      "label": "ZABERcoin (ZAB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913435/mdl-ico.png",
      "value": "Modulum",
      "label": "Modulum (MDL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19633/btc.png",
      "value": "Bitfinex Bitcoin Future",
      "label": "Bitfinex Bitcoin Future (BT1)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19633/btc.png",
      "value": "Bitcoin SegWit2X",
      "label": "Bitcoin SegWit2X (BT2)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14761952/jcr.png",
      "value": "Jincor",
      "label": "Jincor (JCR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14761953/xbs.png",
      "value": "Extreme Sportsbook",
      "label": "Extreme Sportsbook (XSB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913431/ebst.png",
      "value": "eBoost",
      "label": "eBoost (EBST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913432/kek.png",
      "value": "KekCoin",
      "label": "KekCoin (KEK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913434/bhc.png",
      "value": "BlackholeCoin",
      "label": "BlackholeCoin (BHC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913436/altcom.png",
      "value": "AltCommunity Coin",
      "label": "AltCommunity Coin (ALTCOM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913438/data.png",
      "value": "Streamr DATAcoin",
      "label": "Streamr DATAcoin (DATA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913440/dtc.png",
      "value": "Datacoin",
      "label": "Datacoin (DTC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913441/play.png",
      "value": "HEROcoin",
      "label": "HEROcoin (PLAY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913451/pure.png",
      "value": "Pure",
      "label": "Pure (PURE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913452/cld.png",
      "value": "Cloud",
      "label": "Cloud (CLD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913453/otn.png",
      "value": "Open Trading Network",
      "label": "Open Trading Network (OTN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913455/pos.png",
      "value": "PoSToken",
      "label": "PoSToken (POS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913457/neog.png",
      "value": "NEO Gold",
      "label": "NEO Gold (NEOG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913459/exn.png",
      "value": "ExchangeN",
      "label": "ExchangeN (EXN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913458/ins.png",
      "value": "INS Ecosystem",
      "label": "INS Ecosystem (INS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913462/trct.png",
      "value": "Tracto",
      "label": "Tracto (TRCT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913456/ukg.png",
      "value": "UnikoinGold",
      "label": "UnikoinGold (UKG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913463/btcred.png",
      "value": "Bitcoin Red",
      "label": "Bitcoin Red (BTCRED)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913464/ebch.png",
      "value": "eBitcoinCash",
      "label": "eBitcoinCash (EBCH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913466/jpc.png",
      "value": "J Coin",
      "label": "J Coin (JPC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913467/axt.png",
      "value": "AIX",
      "label": "AIX (AXT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913482/rdn.png",
      "value": "Raiden Network",
      "label": "Raiden Network (RDN*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913483/neu.png",
      "value": "Neumark",
      "label": "Neumark (NEU)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913484/rupx.png",
      "value": "Rupaya",
      "label": "Rupaya (RUPX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913485/bdr.png",
      "value": "BlueDragon",
      "label": "BlueDragon (BDR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913487/dutch.png",
      "value": "Dutch Coin",
      "label": "Dutch Coin (DUTCH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913488/tio.png",
      "value": "Trade.io",
      "label": "Trade.io (TIO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913529/hnc.png",
      "value": "Huncoin",
      "label": "Huncoin (HNC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913531/mdc.png",
      "value": "MadCoin",
      "label": "MadCoin (MDC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913533/pura.png",
      "value": "Pura",
      "label": "Pura (PURA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913536/inn.png",
      "value": "Innova",
      "label": "Innova (INN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913538/hst.png",
      "value": "Decision Token",
      "label": "Decision Token (HST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746476/bcpt.png",
      "value": "BlockMason Credit Protocol",
      "label": "BlockMason Credit Protocol (BCPT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913539/bdl.png",
      "value": "Bitdeal",
      "label": "Bitdeal (BDL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913541/xbl.png",
      "value": "Billionaire Token",
      "label": "Billionaire Token (XBL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913542/zeph.png",
      "value": "Project Zephyr",
      "label": "Project Zephyr (ZEPH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913548/nuls.png",
      "value": "Nuls",
      "label": "Nuls (NULS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913549/phr.png",
      "value": "Phore",
      "label": "Phore (PHR*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913550/lcash.png",
      "value": "LitecoinCash",
      "label": "LitecoinCash (LCASH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913552/cfd.png",
      "value": "Confido",
      "label": "Confido (CFD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913560/wsc.png",
      "value": "WiserCoin",
      "label": "WiserCoin (WSC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913561/dbet.png",
      "value": "Decent.bet",
      "label": "Decent.bet (DBET)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913685/xgox.png",
      "value": "Go!",
      "label": "Go! (xGOx)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913564/newb.png",
      "value": "Newbium",
      "label": "Newbium (NEWB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913568/life.png",
      "value": "LIFE",
      "label": "LIFE (LIFE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913570/rmc.png",
      "value": "Russian Mining Coin",
      "label": "Russian Mining Coin (RMC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913573/credo-1.png",
      "value": "Credo",
      "label": "Credo (CREDO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913574/msr.png",
      "value": "Masari",
      "label": "Masari (MSR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913575/cjt.png",
      "value": "ConnectJob Token",
      "label": "ConnectJob Token (CJT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913585/esc.png",
      "value": "Ethersportcoin",
      "label": "Ethersportcoin (ESC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913587/env.png",
      "value": "Envion",
      "label": "Envion (EVN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913602/bnk.png",
      "value": "Bankera",
      "label": "Bankera (BNK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913603/ella.png",
      "value": "Ellaism",
      "label": "Ellaism (ELLA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913604/bpl.png",
      "value": "BlockPool",
      "label": "BlockPool (BPL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913607/rock1.png",
      "value": "RocketCoin ",
      "label": "RocketCoin  (ROCK*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913608/drxne.png",
      "value": "Droxne",
      "label": "Droxne (DRXNE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913631/skr.png",
      "value": "Sakuracoin",
      "label": "Sakuracoin (SKR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913632/grid.png",
      "value": "Grid+",
      "label": "Grid+ (GRID)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913633/xptx.png",
      "value": "PlatinumBAR",
      "label": "PlatinumBAR (XPTX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913634/gvt.png",
      "value": "Genesis Vision",
      "label": "Genesis Vision (GVT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913641/astro.png",
      "value": "Astronaut",
      "label": "Astronaut (ASTRO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913642/gmt.png",
      "value": "Mercury Protocol",
      "label": "Mercury Protocol (GMT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913643/epy.png",
      "value": "Emphy",
      "label": "Emphy (EPY*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913644/soar.png",
      "value": "Soarcoin",
      "label": "Soarcoin (SOAR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913647/hold.png",
      "value": "Interstellar Holdings",
      "label": "Interstellar Holdings (HOLD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913648/mnx.png",
      "value": "MinexCoin",
      "label": "MinexCoin (MNX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913675/crds.png",
      "value": "Credits",
      "label": "Credits (CRDS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913680/viu.png",
      "value": "Viuly",
      "label": "Viuly (VIU)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913687/dbr.png",
      "value": "Düber",
      "label": "Düber (DBR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913683/abt.png",
      "value": "Advanced Browsing Token",
      "label": "Advanced Browsing Token (ABT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913686/gft.jpg",
      "value": "Giftcoin",
      "label": "Giftcoin (GFT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/15887408/qsp.png",
      "value": "Quantstamp",
      "label": "Quantstamp (QSP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/15887409/ript.png",
      "value": "RiptideCoin",
      "label": "RiptideCoin (RIPT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/15887410/bbt.png",
      "value": "BitBoost",
      "label": "BitBoost (BBT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/15887411/gbx.png",
      "value": "GoByte",
      "label": "GoByte (GBX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/15887421/cstl.png",
      "value": "Castle",
      "label": "Castle (CSTL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/15887424/icc.png",
      "value": "Insta Cash Coin",
      "label": "Insta Cash Coin (ICC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/15887422/jnt.jpg",
      "value": "Jibrel Network Token",
      "label": "Jibrel Network Token (JNT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/15887431/qash.png",
      "value": "Quoine Liquid",
      "label": "Quoine Liquid (QASH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404849/alqo.png",
      "value": "Alqo",
      "label": "Alqo (ALQO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404850/knc.png",
      "value": "KingN Coin",
      "label": "KingN Coin (KNC**)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404852/tria.png",
      "value": "Triaconta",
      "label": "Triaconta (TRIA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404866/pbl.png",
      "value": "Publica",
      "label": "Publica (PBL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404853/mag.png",
      "value": "Magnet",
      "label": "Magnet (MAG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404854/stex.png",
      "value": "STEX",
      "label": "STEX (STEX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404855/ufr.png",
      "value": "Upfiring",
      "label": "Upfiring (UFR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404857/lamden.png",
      "value": "Lamden Tau",
      "label": "Lamden Tau (TAU)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404858/lab.png",
      "value": "Labrys",
      "label": "Labrys (LAB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404862/flixx.png",
      "value": "Flixxo",
      "label": "Flixxo (FLIXX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404865/frd.png",
      "value": "Farad",
      "label": "Farad (FRD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404867/pfr.png",
      "value": "PayFair",
      "label": "PayFair (PFR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404869/eca.png",
      "value": "Electra",
      "label": "Electra (ECA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404870/ldm.png",
      "value": "Ludum token",
      "label": "Ludum token (LDM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404871/ltg.png",
      "value": "LiteCoin Gold",
      "label": "LiteCoin Gold (LTG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404890/spank.png",
      "value": "SpankChain",
      "label": "SpankChain (SPANK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404892/wish.png",
      "value": "MyWish",
      "label": "MyWish (WISH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404893/aerm.png",
      "value": "Aerium",
      "label": "Aerium (AERM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404895/plx.png",
      "value": "PlexCoin",
      "label": "PlexCoin (PLX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404894/nio.png",
      "value": "Autonio",
      "label": "Autonio (NIO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746424/ethb.png",
      "value": "EtherBTC",
      "label": "EtherBTC (ETHB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746425/cdx.png",
      "value": "Commodity Ad Network",
      "label": "Commodity Ad Network (CDX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746442/vot.png",
      "value": "Votecoin",
      "label": "Votecoin (VOT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746443/uqc.png",
      "value": "Uquid Coin",
      "label": "Uquid Coin (UQC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746444/lend.png",
      "value": "EthLend",
      "label": "EthLend (LEND)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746450/tio.png",
      "value": "Tio Tour Guides",
      "label": "Tio Tour Guides (TIO*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746453/xsh.png",
      "value": "SHIELD",
      "label": "SHIELD (XSH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746454/bcd.png",
      "value": "BitCAD",
      "label": "BitCAD (BCD*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746477/bco.png",
      "value": "BridgeCoin",
      "label": "BridgeCoin (BCO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746481/dsr.png",
      "value": "Desire",
      "label": "Desire (DSR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746484/ong.png",
      "value": "onG.social",
      "label": "onG.social (ONG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746488/prl.png",
      "value": "Oyster Pearl",
      "label": "Oyster Pearl (PRL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746489/btcm.png",
      "value": "BTCMoon",
      "label": "BTCMoon (BTCM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746535/etbt.png",
      "value": "Ethereum Black",
      "label": "Ethereum Black (ETBT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746536/zcg.png",
      "value": "ZCashGOLD",
      "label": "ZCashGOLD (ZCG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746537/mut.png",
      "value": "Mutual Coin",
      "label": "Mutual Coin (MUT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746538/aion.png",
      "value": "Aion",
      "label": "Aion (AION)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746539/meow.png",
      "value": "Kittehcoin",
      "label": "Kittehcoin (MEOW)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746540/divx.png",
      "value": "Divi",
      "label": "Divi (DIVX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746541/cnbc.png",
      "value": "Cash & Back Coin",
      "label": "Cash & Back Coin (CNBC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746544/rhoc.png",
      "value": "RChain",
      "label": "RChain (RHOC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746547/arc.png",
      "value": " Arcade City",
      "label": " Arcade City (ARC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746548/xun.png",
      "value": "UltraNote",
      "label": "UltraNote (XUN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746549/rfl.png",
      "value": "RAFL",
      "label": "RAFL (RFL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746556/eltcoin.png",
      "value": "ELTCOIN",
      "label": "ELTCOIN (ELTCOIN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746557/grx.png",
      "value": "Gold Reward Token",
      "label": "Gold Reward Token (GRX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746560/ntk.png",
      "value": "Neurotoken",
      "label": "Neurotoken (NTK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746561/ero.png",
      "value": "Eroscoin",
      "label": "Eroscoin (ERO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746569/cmt.png",
      "value": "CyberMiles",
      "label": "CyberMiles (CMT*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746570/rlx.png",
      "value": "Relex",
      "label": "Relex (RLX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746574/man.png",
      "value": "People",
      "label": "People (MAN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746575/cwv.png",
      "value": "CryptoWave",
      "label": "CryptoWave (CWV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746576/act.png",
      "value": "Achain",
      "label": "Achain (ACT*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746592/nro.png",
      "value": "Neuro",
      "label": "Neuro (NRO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746577/send.png",
      "value": "Social Send",
      "label": "Social Send (SEND)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746578/glt.png",
      "value": "GlobalToken",
      "label": "GlobalToken (GLT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746586/coal.png",
      "value": "BitCoal",
      "label": "BitCoal (COAL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746587/daxx.png",
      "value": "DaxxCoin",
      "label": "DaxxCoin (DAXX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746590/bwk.png",
      "value": "Bulwark",
      "label": "Bulwark (BWK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746651/xmrg.png",
      "value": "Monero Gold",
      "label": "Monero Gold (XMRG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746600/btce.png",
      "value": "EthereumBitcoin",
      "label": "EthereumBitcoin (BTCE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746604/fyp.png",
      "value": "FlypMe",
      "label": "FlypMe (FYP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746605/boxy.png",
      "value": "BoxyCoin",
      "label": "BoxyCoin (BOXY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746609/ngc.png",
      "value": "NagaCoin",
      "label": "NagaCoin (NGC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746611/utn.png",
      "value": "Universa",
      "label": "Universa (UTN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746616/egas.png",
      "value": "ETHGAS",
      "label": "ETHGAS (EGAS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746618/dpp.png",
      "value": "Digital Assets Power Play",
      "label": "Digital Assets Power Play (DPP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746629/tgt.png",
      "value": "TargetCoin",
      "label": "TargetCoin (TGT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746634/xdc.png",
      "value": "XinFin Coin",
      "label": "XinFin Coin (XDC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746638/bmt.png",
      "value": "BMChain",
      "label": "BMChain (BMT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746639/bio.png",
      "value": "Biocoin",
      "label": "Biocoin (BIO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746647/btcl.png",
      "value": "BTC Lite",
      "label": "BTC Lite (BTCL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746648/pcn.png",
      "value": "PeepCoin",
      "label": "PeepCoin (PCN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746652/rbtc.png",
      "value": "Bitcoin Revolution",
      "label": "Bitcoin Revolution (RBTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746661/cred.png",
      "value": "Verify",
      "label": "Verify (CRED)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746667/klk.png",
      "value": "Kalkulus",
      "label": "Kalkulus (KLK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746668/ac3.png",
      "value": "AC3",
      "label": "AC3 (AC3)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746671/gto.png",
      "value": "GIFTO",
      "label": "GIFTO (GTO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746672/tnb.png",
      "value": "Time New Bank",
      "label": "Time New Bank (TNB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746673/chips.png",
      "value": "CHIPS",
      "label": "CHIPS (CHIPS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746674/hkn.png",
      "value": "Hacken",
      "label": "Hacken (HKN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318185/b2bx.png",
      "value": "B2B",
      "label": "B2B (B2BX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746675/loc.png",
      "value": "LockChain",
      "label": "LockChain (LOC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746694/itns.png",
      "value": "IntenseCoin",
      "label": "IntenseCoin (ITNS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746697/smt.png",
      "value": "SmartMesh",
      "label": "SmartMesh (SMT*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746741/ger.png",
      "value": "GermanCoin",
      "label": "GermanCoin (GER)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746704/ltcu.png",
      "value": "LiteCoin Ultra",
      "label": "LiteCoin Ultra (LTCU)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746703/emgo.png",
      "value": "MobileGo",
      "label": "MobileGo (EMGO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746705/btca.png",
      "value": "Bitair",
      "label": "Bitair (BTCA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746735/hqx.png",
      "value": "HOQU",
      "label": "HOQU (HQX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746737/etf.png",
      "value": "EthereumFog",
      "label": "EthereumFog (ETF)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746738/bcx.png",
      "value": "BitcoinX",
      "label": "BitcoinX (BCX*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746739/lux.png",
      "value": "Luxmi Coin",
      "label": "Luxmi Coin (LUX**)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746740/stak.png",
      "value": "Straks",
      "label": "Straks (STAK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746742/bcoin.png",
      "value": "BannerCoin",
      "label": "BannerCoin (BCOIN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746766/med.png",
      "value": "MediBloc",
      "label": "MediBloc (MED*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780588/bnty.png",
      "value": "Bounty0x",
      "label": "Bounty0x (BNTY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780589/brd.png",
      "value": "Bread token",
      "label": "Bread token (BRD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780593/hat.png",
      "value": "Hawala.Today",
      "label": "Hawala.Today (HAT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780600/elf.png",
      "value": "aelf",
      "label": "aelf (ELF)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780608/dbc.png",
      "value": "DeepBrain Chain",
      "label": "DeepBrain Chain (DBC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780609/zen.png",
      "value": "Zen Protocol",
      "label": "Zen Protocol (ZEN*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780610/pop.png",
      "value": "PopularCoin",
      "label": "PopularCoin (POP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780611/crc.png",
      "value": "CrowdCoin",
      "label": "CrowdCoin (CRC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780612/pnx.png",
      "value": "PhantomX",
      "label": "PhantomX (PNX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780613/bas.png",
      "value": "BitAsean",
      "label": "BitAsean (BAS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780614/utt.png",
      "value": "United Traders Token",
      "label": "United Traders Token (UTT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780615/hbc.png",
      "value": "HomeBlockCoin",
      "label": "HomeBlockCoin (HBC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780616/amm.png",
      "value": "MicroMoney",
      "label": "MicroMoney (AMM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780624/dav.png",
      "value": "DavorCoin",
      "label": "DavorCoin (DAV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780625/xcpo.png",
      "value": "Copico",
      "label": "Copico (XCPO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780627/erc20.png",
      "value": "Index ERC20",
      "label": "Index ERC20 (ERC20)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780628/itc.png",
      "value": "IoT Chain",
      "label": "IoT Chain (ITC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780629/html.png",
      "value": "HTML Coin",
      "label": "HTML Coin (HTML)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780637/nms.png",
      "value": "Numus",
      "label": "Numus (NMS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780639/pho.png",
      "value": "Photon",
      "label": "Photon (PHO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780641/extra-logo-white.png",
      "value": "ExtraCredit",
      "label": "ExtraCredit (XTRA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780649/ntwk.png",
      "value": "Network Token",
      "label": "Network Token (NTWK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780650/sucr.png",
      "value": "Sucre",
      "label": "Sucre (SUCR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780651/smart.png",
      "value": "SmartBillions",
      "label": "SmartBillions (SMART*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780652/gnx.png",
      "value": "Genaro Network",
      "label": "Genaro Network (GNX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780653/nas.png",
      "value": "Nebulas",
      "label": "Nebulas (NAS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780654/acco.png",
      "value": "Accolade",
      "label": "Accolade (ACCO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780656/bth.png",
      "value": "Bytether ",
      "label": "Bytether  (BTH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780658/rem.png",
      "value": "REMME",
      "label": "REMME (REM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780659/tok.png",
      "value": "TokugawaCoin",
      "label": "TokugawaCoin (TOK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780660/ereal.png",
      "value": "eREAL",
      "label": "eREAL (EREAL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780661/cpn.png",
      "value": "CompuCoin",
      "label": "CompuCoin (CPN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780662/xft.png",
      "value": "Footy Cash",
      "label": "Footy Cash (XFT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780666/bte.png",
      "value": "BitSerial",
      "label": "BitSerial (BTE*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780667/omgc.png",
      "value": "OmiseGO Classic",
      "label": "OmiseGO Classic (OMGC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19872/q2c.jpg",
      "value": "QubitCoin",
      "label": "QubitCoin (Q2C)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780668/blt.png",
      "value": "Bloom Token",
      "label": "Bloom Token (BLT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780669/spf.png",
      "value": "SportyFi",
      "label": "SportyFi (SPF)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780673/tds.png",
      "value": "TokenDesk",
      "label": "TokenDesk (TDS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780695/ore.png",
      "value": "Galactrum",
      "label": "Galactrum (ORE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780696/spk.png",
      "value": "Sparks",
      "label": "Sparks (SPK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780698/goa.png",
      "value": "GoaCoin",
      "label": "GoaCoin (GOA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780701/fuloos.png",
      "value": "Fuloos Coin",
      "label": "Fuloos Coin (FLS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780702/phils.png",
      "value": "PhilsCurrency",
      "label": "PhilsCurrency (PHILS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780703/gun.png",
      "value": "GunCoin",
      "label": "GunCoin (GUN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780715/dfs.png",
      "value": "DFSCoin",
      "label": "DFSCoin (DFS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780731/polis.png",
      "value": "PolisPay",
      "label": "PolisPay (POLIS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780732/well.png",
      "value": "Well",
      "label": "Well (WELL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780740/cl.png",
      "value": "CoinLancer",
      "label": "CoinLancer (CL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780741/shnd.png",
      "value": "StrongHands",
      "label": "StrongHands (SHND)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780743/aua.png",
      "value": "ArubaCoin",
      "label": "ArubaCoin (AUA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780745/saga.png",
      "value": "SagaCoin",
      "label": "SagaCoin (SAGA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780749/tsl.png",
      "value": "Energo",
      "label": "Energo (TSL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780751/irl.png",
      "value": "IrishCoin",
      "label": "IrishCoin (IRL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780753/bot.png",
      "value": "Bodhi",
      "label": "Bodhi (BOT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780758/pma.png",
      "value": "PumaPay",
      "label": "PumaPay (PMA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780762/troll.png",
      "value": "Trollcoin",
      "label": "Trollcoin (TROLL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780764/for.png",
      "value": "Force Coin",
      "label": "Force Coin (FOR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780766/sgr.png",
      "value": "Sugar Exchange",
      "label": "Sugar Exchange (SGR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780772/jet.png",
      "value": "Jetcoin",
      "label": "Jetcoin (JET)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780773/ipnvhhke_400x400.jpg",
      "value": "MediShares",
      "label": "MediShares (MDS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780774/lcp.png",
      "value": "Litecoin Plus",
      "label": "Litecoin Plus (LCP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780776/gtc.png",
      "value": "Game",
      "label": "Game (GTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780777/ieth.png",
      "value": "iEthereum",
      "label": "iEthereum (IETH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780778/gcc.png",
      "value": "TheGCCcoin",
      "label": "TheGCCcoin (GCC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780780/sdrn.png",
      "value": "Sanderon",
      "label": "Sanderon (SDRN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780781/ink.png",
      "value": "Ink",
      "label": "Ink (INK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780785/monk.png",
      "value": "Monkey Project",
      "label": "Monkey Project (MONK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780786/jinn.png",
      "value": "Jinn",
      "label": "Jinn (JINN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780787/set.png",
      "value": "Setcoin",
      "label": "Setcoin (SET)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780788/mgn.png",
      "value": "MagnaCoin",
      "label": "MagnaCoin (MGN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780789/kz.png",
      "value": "KZCash",
      "label": "KZCash (KZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780791/gnr.png",
      "value": "Gainer",
      "label": "Gainer (GNR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780798/brc.png",
      "value": "BrightCoin",
      "label": "BrightCoin (BRC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780799/wcg.png",
      "value": "World Crypto Gold",
      "label": "World Crypto Gold (WCG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780800/flat.png",
      "value": "Hive",
      "label": "Hive (HIVE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792566/etl.png",
      "value": "EtherLite",
      "label": "EtherLite (ETL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792571/drg.png",
      "value": "Dragon Coin",
      "label": "Dragon Coin (DRG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792572/brc.png",
      "value": "BinaryCoin",
      "label": "BinaryCoin (BRC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792575/zap.png",
      "value": "Zap",
      "label": "Zap (ZAP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792577/aidoc.png",
      "value": "AI Doctor",
      "label": "AI Doctor (AIDOC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792579/ecc.png",
      "value": "ECC",
      "label": "ECC (ECC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792592/lct.png",
      "value": "LendConnect",
      "label": "LendConnect (LCT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746688/mnt.png",
      "value": "Media Network Coin",
      "label": "Media Network Coin (MNT*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19828/nlg.png",
      "value": "Gulden",
      "label": "Gulden (NLG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792600/stn.png",
      "value": "Steneum Coin",
      "label": "Steneum Coin (STN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792603/int.png",
      "value": "Internet Node Token",
      "label": "Internet Node Token (INT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780805/luck.png",
      "value": "Luckbox",
      "label": "Luckbox (LCK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318367/b3.png",
      "value": "B3 Coin",
      "label": "B3 Coin (B3)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383831/chan2.png",
      "value": "ChanCoin",
      "label": "ChanCoin (CHAN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780782/kbr.png",
      "value": "Kubera Coin",
      "label": "Kubera Coin (KBR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792606/pcoin.png",
      "value": "Pioneer Coin",
      "label": "Pioneer Coin (PCOIN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792607/bln.png",
      "value": "Bolenum",
      "label": "Bolenum (BLN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383785/stu.png",
      "value": "BitJob",
      "label": "BitJob (STU)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746447/seth.png",
      "value": "Sether",
      "label": "Sether (SETH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792611/blas.png",
      "value": "BlakeStar",
      "label": "BlakeStar (BLAS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792612/dime.png",
      "value": "DimeCoin",
      "label": "DimeCoin (DIME)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19723/gld.png",
      "value": "GoldCoin",
      "label": "GoldCoin (GLD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792608/luc.png",
      "value": "Play 2 Live",
      "label": "Play 2 Live (LUC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792615/edt.png",
      "value": "EtherDelta",
      "label": "EtherDelta (EDT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913555/plus.png",
      "value": "PlusCoin",
      "label": "PlusCoin (PLC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746451/abyss.png",
      "value": "The Abyss",
      "label": "The Abyss (ABYSS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792621/brat.png",
      "value": "Brat",
      "label": "Brat (BRAT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792623/cag1.png",
      "value": "Change",
      "label": "Change (CAG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383651/xbt.png",
      "value": "Tezos",
      "label": "Tezos (XTZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20331/key.png",
      "value": "KeyCoin",
      "label": "KeyCoin (KEY*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351492/atm.png",
      "value": "Autumncoin",
      "label": "Autumncoin (ATM*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792638/arct.png",
      "value": "ArbitrageCT",
      "label": "ArbitrageCT (ARCT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792569/tel.png",
      "value": "Telcoin",
      "label": "Telcoin (TEL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780744/dnn.png",
      "value": "DNN Token",
      "label": "DNN Token (DNN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792643/aura.png",
      "value": "Aurora",
      "label": "Aurora (AURA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792645/cbt.png",
      "value": "CommerceBlock Token",
      "label": "CommerceBlock Token (CBT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792650/xbp.png",
      "value": "Black Pearl Coin",
      "label": "Black Pearl Coin (XBP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780642/exrn.png",
      "value": "EXRNchain",
      "label": "EXRNchain (EXRN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792642/imv.png",
      "value": "ImmVRse",
      "label": "ImmVRse (IMV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792654/bft.png",
      "value": "BF Token (BFT)",
      "label": "BF Token (BFT) (BFT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318264/7638-nty_400x400.jpg",
      "value": "DigiByte",
      "label": "DigiByte (DGB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318422/lux.png",
      "value": "LUXCoin",
      "label": "LUXCoin (LUX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792655/lgo.png",
      "value": "Legolas Exchange",
      "label": "Legolas Exchange (LGO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20646/eth_logo.png",
      "value": "Ethereum",
      "label": "Ethereum (ETH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792662/itz.png",
      "value": "Interzone",
      "label": "Interzone (ITZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792653/agi.png",
      "value": "SingularityNET",
      "label": "SingularityNET (AGI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792665/crpt.png",
      "value": "Crypterium",
      "label": "Crypterium (CRPT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318350/smt.png",
      "value": "Social Media Market",
      "label": "Social Media Market (SMT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792624/zen.png",
      "value": "ZenCash",
      "label": "ZenCash (ZEN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792671/sgl.png",
      "value": "Sigil",
      "label": "Sigil (SGL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780607/cwx.png",
      "value": "Crypto-X",
      "label": "Crypto-X (CWX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780646/hion.png",
      "value": "Handelion",
      "label": "Handelion (HION)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318208/grf.png",
      "value": "Graft Blockchain",
      "label": "Graft Blockchain (GRF)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19680/dmd.png",
      "value": "Diamond",
      "label": "Diamond (DMD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792680/dta.png",
      "value": "Data",
      "label": "Data (DTA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792672/tnc.png",
      "value": "Trinity Network Credit",
      "label": "Trinity Network Credit (TNC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792684/krb.png",
      "value": "Karbo",
      "label": "Karbo (KRB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792685/dtx.png",
      "value": "DataBroker DAO",
      "label": "DataBroker DAO (DTX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010445/mcu.png",
      "value": "MediChain",
      "label": "MediChain (MCU)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010448/ocn.png",
      "value": "Odyssey",
      "label": "Odyssey (OCN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318352/echt.png",
      "value": "e-Chat",
      "label": "e-Chat (ECHT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010450/theta.png",
      "value": "Theta",
      "label": "Theta (THETA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010446/cv.png",
      "value": "CarVertical",
      "label": "CarVertical (CV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913645/exy.png",
      "value": "Experty",
      "label": "Experty (EXY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913430/atm.png",
      "value": "ATMChain",
      "label": "ATMChain (ATM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010455/sgn.png",
      "value": "Signals Network",
      "label": "Signals Network (SGN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383674/xrb.png",
      "value": "RaiBlocks",
      "label": "RaiBlocks (XRB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010459/iost.png",
      "value": "IOS token",
      "label": "IOS token (IOST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010460/tct.png",
      "value": "TokenClub ",
      "label": "TokenClub  (TCT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010454/bpt.png",
      "value": "Blockport",
      "label": "Blockport (BPT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010463/trac.png",
      "value": "OriginTrail",
      "label": "OriginTrail (TRAC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010462/mot.png",
      "value": "Olympus Labs",
      "label": "Olympus Labs (MOT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010465/horse.png",
      "value": "Ethorse ",
      "label": "Ethorse  (HORSE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010466/qun.png",
      "value": "QunQun",
      "label": "QunQun (QUN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010467/acc.png",
      "value": "Accelerator Network",
      "label": "Accelerator Network (ACC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351989/mdt.png",
      "value": "Midnight",
      "label": "Midnight (MDT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010451/mdt.png",
      "value": "Measurable Data Token ",
      "label": "Measurable Data Token  (MDT*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010469/oloethnw_400x400.jpg",
      "value": "Qbao",
      "label": "Qbao (QBT*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792636/bot.png",
      "value": "Bottos",
      "label": "Bottos (BTO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746642/mtrc.png",
      "value": "ModulTrade",
      "label": "ModulTrade (MTRC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318361/bm.png",
      "value": "Bitcomo",
      "label": "Bitcomo (BM*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010470/ipl.png",
      "value": "InsurePal",
      "label": "InsurePal (IPL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010471/sent.png",
      "value": "Sentinel",
      "label": "Sentinel (SENT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010472/swftc.png",
      "value": "SwftCoin",
      "label": "SwftCoin (SWFTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010474/opc.png",
      "value": "OP Coin",
      "label": "OP Coin (OPC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383807/aig.png",
      "value": "Aigang",
      "label": "Aigang (AIX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010477/sha.png",
      "value": "Shacoin",
      "label": "Shacoin (SHA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913439/ugt.png",
      "value": "ugChain",
      "label": "ugChain (UGC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010479/pylnt.png",
      "value": "Pylon Network",
      "label": "Pylon Network (PYLNT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/11999078/wrc.png",
      "value": "Worldcore",
      "label": "Worldcore (WRC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792637/doc.png",
      "value": "Doc Coin",
      "label": "Doc Coin (DOC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913547/ges.png",
      "value": "Galaxy eSolutions",
      "label": "Galaxy eSolutions (GES)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010480/garlic.png",
      "value": "Garlicoin",
      "label": "Garlicoin (GRLC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010482/repux.png",
      "value": "Repux",
      "label": "Repux (REPUX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010484/joy.png",
      "value": "JoyToken",
      "label": "JoyToken (JOY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010485/gtcoin.png",
      "value": "Global Tour Coin",
      "label": "Global Tour Coin (GTC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010483/yee.png",
      "value": "Yee",
      "label": "Yee (YEE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010464/zil.png",
      "value": "Zilliqa",
      "label": "Zilliqa (ZIL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792563/mfg.png",
      "value": "SyncFab",
      "label": "SyncFab (MFG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746591/fnt.png",
      "value": "FinTab",
      "label": "FinTab (FNT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14761950/shp.png",
      "value": "Sharpe Capital",
      "label": "Sharpe Capital (SHP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010488/xcd.png",
      "value": "Capdax",
      "label": "Capdax (XCD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351926/zcl.png",
      "value": "ZClassic",
      "label": "ZClassic (ZCL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780801/gamex.png",
      "value": "GameX",
      "label": "GameX (GX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20694/pkb.png",
      "value": "ParkByte",
      "label": "ParkByte (PKB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780665/qlc.png",
      "value": "QLINK",
      "label": "QLINK (QLC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913556/srn.png",
      "value": "SirinLabs",
      "label": "SirinLabs (SRN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404872/bcd.png",
      "value": "Bitcoin Diamond",
      "label": "Bitcoin Diamond (BCD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382096/allsafe.jpg",
      "value": "Allsafe",
      "label": "Allsafe (ASAFE2)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350693/karm.png",
      "value": "Karmacoin",
      "label": "Karmacoin (KARMA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746666/sbtc.png",
      "value": "Super Bitcoin",
      "label": "Super Bitcoin (SBTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792626/ubc.png",
      "value": "UnitedBitcoin",
      "label": "UnitedBitcoin (UBTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010490/btw.png",
      "value": "BitcoinWhite",
      "label": "BitcoinWhite (BTW)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351740/air.png",
      "value": "Aircoin",
      "label": "Aircoin (AIR*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/9350797/prix.png",
      "value": "Privatix",
      "label": "Privatix (PRIX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383792/pro.png",
      "value": "Propy",
      "label": "Propy (PRO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383158/btcs.png",
      "value": "Bitcoin Scrypt",
      "label": "Bitcoin Scrypt (BTCS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913540/comsa.png",
      "value": "COMSA",
      "label": "COMSA (CMS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318303/cpay.png",
      "value": "CryptoPay",
      "label": "CryptoPay (CPAY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351090/cmt.png",
      "value": "CometCoin",
      "label": "CometCoin (CMT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010496/bsty.png",
      "value": "GlobalBoost",
      "label": "GlobalBoost (BSTY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010498/fjc.png",
      "value": "FujiCoin",
      "label": "FujiCoin (FJC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780746/gxs.png",
      "value": "GXShares",
      "label": "GXShares (GXS)"
    },
    {
      "value": "Waves",
      "label": "Waves (WAVES)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010501/cpc.png",
      "value": "CPChain",
      "label": "CPChain (CPC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318081/wiz.png",
      "value": "Crowdwiz",
      "label": "Crowdwiz (WIZ)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318328/cav.png",
      "value": "Caviar",
      "label": "Caviar (CAV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010506/zpt.png",
      "value": "Zeepin",
      "label": "Zeepin (ZPT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010507/gim.png",
      "value": "Gimli",
      "label": "Gimli (GIM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010509/ref.png",
      "value": "RefToken",
      "label": "RefToken (REF)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010511/sxdt.png",
      "value": "SPECTRE Dividend Token",
      "label": "SPECTRE Dividend Token (SXDT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20626/imageedit_27_4355944719.png",
      "value": "DigitalCash",
      "label": "DigitalCash (DASH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19945/vtc.png",
      "value": "VertCoin",
      "label": "VertCoin (VTC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010511/sxdt.png",
      "value": "SPECTRE Utility Token",
      "label": "SPECTRE Utility Token (SXUT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010514/fair.png",
      "value": "FairGame",
      "label": "FairGame (FAIR*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010515/val.png",
      "value": "Valorbit",
      "label": "Valorbit (VAL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010516/man.png",
      "value": "Matrix AI Network",
      "label": "Matrix AI Network (MAN*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010517/1.png",
      "value": "BlockCDN ",
      "label": "BlockCDN  (BCDN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/351238/ibank.png",
      "value": "iBankCoin",
      "label": "iBankCoin (IBANK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350896/cab.png",
      "value": "CabbageUnit",
      "label": "CabbageUnit (CAB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/352131/plnc.png",
      "value": "PLNCoin",
      "label": "PLNCoin (PLNC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010513/ldc.png",
      "value": "LeadCoin",
      "label": "LeadCoin (LDC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318066/npx.png",
      "value": "Napoleon X",
      "label": "Napoleon X (NPX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780792/lnc.png",
      "value": "BlockLancer",
      "label": "BlockLancer (LNC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792570/transferir-copiar.png",
      "value": "Sp8de",
      "label": "Sp8de (SPX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746767/ccos.png",
      "value": "CrowdCoinage",
      "label": "CrowdCoinage (CCOS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792674/cfty.png",
      "value": "Crafty",
      "label": "Crafty (CFTY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318190/earth.png",
      "value": "Earth Token",
      "label": "Earth Token (EARTH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792581/et4.png",
      "value": "Eticket4",
      "label": "Eticket4 (ET4)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792673/fsbt.png",
      "value": "Forty Seven Bank",
      "label": "Forty Seven Bank (FSBT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746475/gea.png",
      "value": "Goldea",
      "label": "Goldea (GEA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792573/onl.png",
      "value": "On.Live",
      "label": "On.Live (ONL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383652/eos_1.png",
      "value": "EOS",
      "label": "EOS (EOS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746438/drc.png",
      "value": "Darico",
      "label": "Darico (DRC*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14543951/bar.png",
      "value": "TBIS token",
      "label": "TBIS token (BAR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746551/cofi.png",
      "value": "CoinFi",
      "label": "CoinFi (COFI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010452/prps.png",
      "value": "Purpose",
      "label": "Purpose (PRPS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14761912/key.png",
      "value": "SelfKey",
      "label": "SelfKey (KEY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404856/loci.png",
      "value": "LociCoin",
      "label": "LociCoin (LOCI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318390/gat.png",
      "value": "GATCOIN",
      "label": "GATCOIN (GAT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010505/pxs.png",
      "value": "Pundi X",
      "label": "Pundi X (PXS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404874/stp.png",
      "value": "StashPay",
      "label": "StashPay (STP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913454/rebl.png",
      "value": "Rebellious",
      "label": "Rebellious (REBL)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318374/up.png",
      "value": "UpToken",
      "label": "UpToken (UP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318218/canya.png",
      "value": "CanYaCoin",
      "label": "CanYaCoin (CAN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792618/srnt.png",
      "value": "Serenity",
      "label": "Serenity (SRNT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780606/vlr.png",
      "value": "Valorem",
      "label": "Valorem (VLR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010453/dubi.png",
      "value": "Decentralized Universal Basic Income",
      "label": "Decentralized Universal Basic Income (DUBI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383745/hvt.png",
      "value": "Hive Project",
      "label": "Hive Project (HVN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318148/cas.png",
      "value": "Cashaa",
      "label": "Cashaa (CAS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383700/act.png",
      "value": "ACT",
      "label": "ACT (ACT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20275/etc2.png",
      "value": "Ethereum Classic",
      "label": "Ethereum Classic (ETC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792604/sfu.png",
      "value": "Saifu",
      "label": "Saifu (SFU)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010510/plr.png",
      "value": "Pillar",
      "label": "Pillar (PLR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19994/cloak.png",
      "value": "CloakCoin",
      "label": "CloakCoin (CLOAK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318262/iop.png",
      "value": "Internet of People",
      "label": "Internet of People (IOP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913635/etk.png",
      "value": "Energi Token",
      "label": "Energi Token (ETK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010502/spend.png",
      "value": "Spend",
      "label": "Spend (SPEND)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20077/xst.png",
      "value": "StealthCoin",
      "label": "StealthCoin (XST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383969/ent.png",
      "value": "Entropy Token",
      "label": "Entropy Token (ENTRP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746482/bdg.png",
      "value": "BitDegree",
      "label": "BitDegree (BDG)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384036/cmpo.png",
      "value": "CampusCoin",
      "label": "CampusCoin (CMPCO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913551/sphtx.png",
      "value": "SophiaTX",
      "label": "SophiaTX (SPHTX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913571/bkx.png",
      "value": "BANKEX",
      "label": "BANKEX (BKX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318288/cov.png",
      "value": "Covesting",
      "label": "Covesting (COV)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010481/eve.png",
      "value": "Devery",
      "label": "Devery (EVE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746619/adb.png",
      "value": "Adbank",
      "label": "Adbank (ADB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010491/axp.png",
      "value": "aXpire",
      "label": "aXpire (AXP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746588/x8x.png",
      "value": "X8Currency",
      "label": "X8Currency (X8X)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383841/dmarket.png",
      "value": "DMarket",
      "label": "DMarket (DMT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/350614/egc.png",
      "value": "EverGreenCoin",
      "label": "EverGreenCoin (EGC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318302/c20.png",
      "value": "Crypto20",
      "label": "Crypto20 (C20)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010497/fota.png",
      "value": "Fortuna",
      "label": "Fortuna (FOTA)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318075/ctx1.png",
      "value": "CarTaxi",
      "label": "CarTaxi (CTX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780626/get.png",
      "value": "Guaranteed Entrance Token",
      "label": "Guaranteed Entrance Token (GET)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913433/aidcoin.png",
      "value": "AidCoin",
      "label": "AidCoin (AID)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383999/dcn.png",
      "value": "Dentacoin",
      "label": "Dentacoin (DCN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746427/food.png",
      "value": "FoodCoin",
      "label": "FoodCoin (FOOD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318162/mat.png",
      "value": "MiniApps",
      "label": "MiniApps (MAT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383760/ddf.png",
      "value": "Digital Developers Fund",
      "label": "Digital Developers Fund (DDF)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20028/dem.png",
      "value": "eMark",
      "label": "eMark (DEM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1383971/grwi.png",
      "value": "Growers International",
      "label": "Growers International (GRWI)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318308/elm.png",
      "value": "Elements",
      "label": "Elements (ELM)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010540/mzx.png",
      "value": "Mosaic Token",
      "label": "Mosaic Token (MZX)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913545/atfs.png",
      "value": "ATFS Project",
      "label": "ATFS Project (ATFS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792602/cpy.png",
      "value": "COPYTRACK",
      "label": "COPYTRACK (CPY)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913681/scr.png",
      "value": "Scorum",
      "label": "Scorum (SCR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792622/ebc.png",
      "value": "EBCoin",
      "label": "EBCoin (EBC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14543969/wpr.png",
      "value": "WePower",
      "label": "WePower (WPR)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010473/sfu.png",
      "value": "Safinus",
      "label": "Safinus (SAF)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780733/flot.png",
      "value": "FireLotto",
      "label": "FireLotto (FLOT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010495/stac1.png",
      "value": "STAC",
      "label": "STAC (STAC)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780630/gene.png",
      "value": "PARKGENE",
      "label": "PARKGENE (GENE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14761941/dtt1.png",
      "value": "Data Trading",
      "label": "Data Trading (DTT*)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780794/lwf.png",
      "value": "Local World Forwarders",
      "label": "Local World Forwarders (LWF)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16404861/deb.png",
      "value": "Debitum Token",
      "label": "Debitum Token (DEB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/16746650/pyp.png",
      "value": "PayPro",
      "label": "PayPro (PYP)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792598/vst.png",
      "value": "Vestarin",
      "label": "Vestarin (VST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792620/mlt.png",
      "value": "MultiGames",
      "label": "MultiGames (MLT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792644/idh.png",
      "value": "IndaHash",
      "label": "IndaHash (IDH)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913437/st.png",
      "value": "Simple Token",
      "label": "Simple Token (OST)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318329/clout.png",
      "value": "Clout",
      "label": "Clout (CLOUT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/12318226/ert.png",
      "value": "Esports.com",
      "label": "Esports.com (ERT)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19630/btcd_1.png",
      "value": "BitcoinDark",
      "label": "BitcoinDark (BTCD)"
    },
    {
      "img": "https://www.cryptocompare.com/media/19937/uro.png",
      "value": "UroCoin",
      "label": "UroCoin (URO)"
    },
    {
      "img": "https://www.cryptocompare.com/media/20780783/hpb.png",
      "value": "High Performance Blockchain",
      "label": "High Performance Blockchain (HPB)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14913486/xin.png",
      "value": "Infinity Economics",
      "label": "Infinity Economics (XIN)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1384046/ignis.png",
      "value": "Ignis",
      "label": "Ignis (IGNIS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/25792617/cyder.png",
      "value": "Cyder Coin",
      "label": "Cyder Coin (CYDER)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010524/stk.png",
      "value": "STK Token",
      "label": "STK Token (STK)"
    },
    {
      "img": "https://www.cryptocompare.com/media/27010560/vc.png",
      "value": "SPiCE Venture Capital ",
      "label": "SPiCE Venture Capital  (SPICE)"
    },
    {
      "img": "https://www.cryptocompare.com/media/1382862/amis.png",
      "value": "AMIS",
      "label": "AMIS (AMIS)"
    },
    {
      "img": "https://www.cryptocompare.com/media/14761934/rea.png",
      "value": "Realisto",
      "label": "Realisto (REA)"
    }
  ];
