import * as types from './actionTypes';
import {beginLoading, terminateLoading} from './loadingActions';
import alertify from 'alertify.js';
import * as constant from '../config/const';

export function updateUsernameSuccess(userDetails) {
  return { type: types.UPDATE_USERNAME_SUCCESS, userDetails};
}

export function updateUserSuccess(userDetails) {
  return { type: types.UPDATE_USER_SUCCESS, userDetails};
}

export function sameState() {
  return { type: types.SAME_STATE };
}

export function updateUsername(username, uid) {
  return function(dispatch) {
    dispatch(beginLoading());
    return fetch(constant.FETCH_ADDRESS+'/updateUsername',{
      method: "POST",
      body: JSON.stringify({"username": username, "uid": uid})
    })
    .then((response) => response.json())
    .then(response => {
      if(response.user == "USERNAME_EXIST") {
        $('#usernameExistError').removeClass('display-none');
        $('#usernameExistError').addClass('shake');
        dispatch(terminateLoading());
        dispatch(sameState());
      }
      else {
        $('#usernameExistError').addClass('display-none');
        $('#usernameExistError').removeClass('shake');
        localStorage.cryptoLinkJWT = response.token;
        alertify.success("Username updated!");
        $('.profile-element').find('button').addClass('disabled');
        dispatch(updateUsernameSuccess(response));
      }
    }).catch(error => {
      dispatch(terminateLoading());
      alertify.error("Something went wrong!");
      throw(error);
    });
  };
}

export function updateUser(user, uid) {
  return function(dispatch) {
    dispatch(beginLoading());
    user.uid = uid;
    return fetch(constant.FETCH_ADDRESS+'/updateUser',{
      method: "POST",
      body: JSON.stringify(user)
    })
    .then((response) => response.json())
    .then(response => {
      if(response.user == "EMAIL_EXIST") {
        $('#emailExistError').removeClass('display-none');
        $('#emailExistError').addClass('shake');
        dispatch(terminateLoading());
        dispatch(sameState());
      }
      else {
        $('#emailExistError').addClass('display-none');
        $('#emailExistError').removeClass('shake');
        localStorage.cryptoLinkJWT = response.token;
        alertify.success("Profile details are updated!");
        $('.profile-element').find('button').addClass('disabled');
        dispatch(updateUserSuccess(response));
      }
    }).catch(error => {
      dispatch(terminateLoading());
      throw(error);
    });
  };
}

export function uploadDp(file, uid) {
  const formData = new FormData();
  formData.append('photo', file);
  formData.append('uid', uid);
  return function(dispatch) {
    dispatch(beginLoading());
    return fetch(constant.FETCH_ADDRESS+'/uploadDp',{
      method: "POST",
      body: formData
    })
    .then((response) => response.json())
    .then(response => {
      $('#dpUpload').val("");
      localStorage.cryptoLinkJWT = response.token;
      dispatch(updateUserSuccess(response));
    })
    .catch(error => {
      dispatch(terminateLoading());
      throw(error);
    });
  };
}
