import {beginLoading, terminateLoading} from './loadingActions';
import {loadWalletList} from './walletActions';
import alertify from 'alertify.js';
import {browserHistory} from 'react-router';
import * as types from './actionTypes';
import * as constant from '../config/const';

export function loginSuccess(userDetails) {
  return { type: types.LOGIN_SUCCESS, userDetails};
}

export function signupSuccess(userDetails) {
  return { type: types.SIGNUP_SUCCESS, userDetails};
}

export function signupError(errorDetails) {
  if(errorDetails == "USERNAME_EXIST") {
      $('.signup-other-error').html('**USERNAME NOT AVAILABLE**');
  }
  else if(errorDetails == "EMAIL_EXIST") {
      $('.signup-other-error').html('**EMAIL ALREADY EXISTS**');
  }
  else $('.signup-other-error').html('**USER ALREADY EXISTS**');
  $('.signup-other-error').removeClass('display-none');
  $('.signup-other-error').addClass('shake');
  setTimeout(function(){
    $('.signup-other-error').removeClass('shake');
  }, 2000);
  return { type: types.SAME_STATE };
}

export function logoutUserSuccess() {
  return { type: types.LOGOUT_USER_SUCCESS };
}

export function logoutWalletSuccess() {
  return { type: types.LOGOUT_WALLET_SUCCESS };
}

export function logoutSuccess() {
  return { type: types.LOGOUT_SUCCESS };
}

export function authenticationSuccess(token) {
  return { type: types.AUTHENTICATION_SUCCESS, token };
}

/**************************LOGIN*****************************/
export function login(email, password) {
  return function(dispatch) {
    dispatch(beginLoading());
    return fetch(constant.FETCH_ADDRESS+'/login',{
      method: "POST",
      body: JSON.stringify({"email": email, "password": password})
    })
    .then((response) => response.json())
    .then((response) => {
      if(response.user != undefined){
        localStorage.cryptoLinkJWT = response.user.token;
        $('.login-error-section').addClass('display-none');
        dispatch(loadWalletList(response.user.uid));
        dispatch(loginSuccess(response.user));
        dispatch(beginLoading());
        dispatch(authenticationSuccess(response.user.token));
        $('#cryptoLink').removeClass('unauthenticated');
      }
      else {
        $('.login-error-section').removeClass('display-none');
        $('.login-error-section').addClass('shake');
        setTimeout(function(){
          $('.login-error-section').removeClass('shake');
        }, 2000);
        dispatch(terminateLoading());
        // alertify.error("Invalid Credentials"); //Alertify Error
      }
    }).catch(error => {
      dispatch(terminateLoading());
      throw(error);
    });
  };
}

export function relogin(uid) {
  return function(dispatch) {
    dispatch(beginLoading());
    return fetch(constant.FETCH_ADDRESS+'/relogin',{
      method: "POST",
      body: JSON.stringify({"uid": uid})
    })
    .then((response) => response.json())
    .then((response) => {
      if(response.user != undefined){
        localStorage.cryptoLinkJWT = response.user.token;
        $('.login-error-section').addClass('display-none');
        dispatch(loginSuccess(response.user));
        dispatch(beginLoading());
        dispatch(authenticationSuccess(response.user.token));
      }
      else {
        $('.login-error-section').removeClass('display-none');
        $('.login-error-section').addClass('shake');
        setTimeout(function(){
          $('.login-error-section').removeClass('shake');
        }, 2000);
        dispatch(terminateLoading());
        // alertify.error("Invalid Credentials"); //Alertify Error
      }
    }).catch(error => {
      dispatch(terminateLoading());
      throw(error);
    });
  };
}

/**************************LOGOUT*****************************/
export function logout() {
  return function(dispatch) {
    dispatch(beginLoading());
    return fetch(constant.FETCH_ADDRESS+'/logout',{
      method: "GET"
    })
    .then((response) => {
      localStorage.removeItem('cryptoLinkJWT');
      dispatch(logoutSuccess());
      dispatch(logoutUserSuccess());
      dispatch(beginLoading());
      dispatch(logoutWalletSuccess());
    }).catch(err => {
      dispatch(terminateLoading());
      throw (err);
    });
  };
}

/**************************SIGNUP*****************************/
export function signup(email, password, username, fullname) {
  return function(dispatch) {
    dispatch(beginLoading());
    return fetch(constant.FETCH_ADDRESS+'/signup',{
      method: "POST",
      body: JSON.stringify({"email": email, "password": password, "username": username, "full_name": fullname})
    })
    .then((response) => response.json())
    .then((response) => {
      if(response.user == "USER_EXIST" || response.user == "USERNAME_EXIST" || response.user == "EMAIL_EXIST") {
        dispatch(signupError(response.user));
        dispatch(terminateLoading());
      }
      else {
        localStorage.cryptoLinkJWT = response.user.token;
        $('.signup-common-error').addClass('display-none');
        dispatch(signupSuccess(response.user));
        dispatch(beginLoading());
        dispatch(authenticationSuccess(response.user.token));
        $('#cryptoLink').removeClass('unauthenticated');
      }
    }).catch(error => {
      dispatch(terminateLoading());
      throw(error);
    });
  };
}

/**************************SIGNOUT*****************************/
export function forgot_password(email) {
  return function(dispatch) {
    dispatch(beginLoading());
    return fetch(constant.FETCH_ADDRESS+'/forgotPassword',{
      method: "POST",
      body: JSON.stringify({"email": email})
    })
    .then((response) => response.json())
    .then((response) => {
      if(response.message == 'SUCCESS'){
        alertify.success("Please check your inbox to reset password"); //Alertify Error
        dispatch(terminateLoading());
        window.location = constant.FETCH_ADDRESS+"/";
      }
      else if(response.message == 'NO_USER'){
        alertify.error("No user found pertaining to the entered email address"); //Alertify Error
        dispatch(terminateLoading());
      }
      else {
        alertify.error("ERROR"); //Alertify Error
        dispatch(terminateLoading());
        // alertify.error("Invalid Credentials"); //Alertify Error
      }
    }).catch(error => {
      dispatch(terminateLoading());
      throw(error);
    });
  };
}

export function reset_password(uid, password) {
  return function(dispatch) {
    dispatch(beginLoading());
    return fetch(constant.FETCH_ADDRESS+'/resetPassword',{
      method: "POST",
      body: JSON.stringify({"uid": uid, "password": password})
    })
    .then((response) => response.json())
    .then((response) => {
      if(response.message == 'SUCCESS'){
        alertify.success("Your password has been successfully reset.");
        dispatch(terminateLoading());
        window.location = constant.FETCH_ADDRESS+"/login";
      }
      else if(response.message == 'NO_USER'){
        alertify.error("No user found pertaining to the entered email address"); //Alertify Error
        dispatch(terminateLoading());
      }
      else if(response.message == 'NO_REQUEST_INITIATED'){
        alertify.error("Link has already been used to reset password. Please request a new link."); //Alertify Error
        dispatch(terminateLoading());
      }
      else {
        alertify.error("ERROR"); //Alertify Error
        dispatch(terminateLoading());
        // alertify.error("Invalid Credentials"); //Alertify Error
      }
    }).catch(error => {
      dispatch(terminateLoading());
      throw(error);
    });
  };
}
