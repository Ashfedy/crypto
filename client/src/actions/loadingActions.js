import * as types from './actionTypes';

export function beginLoading() {
  return {type: types.BEGIN_LOADING};
}

export function terminateLoading() {
  return {type: types.TERMINATE_LOADING};
}

export function beginPublicLoading() {
  return {type: types.BEGIN_PUBLIC_LOADING};
}

export function terminatePublicLoading() {
  return {type: types.TERMINATE_PUBLIC_LOADING};
}
