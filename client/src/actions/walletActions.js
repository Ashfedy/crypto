import * as types from './actionTypes';
import {beginLoading, terminateLoading} from './loadingActions';
import alertify from 'alertify.js';
import * as constant from '../config/const';

export function addWalletSuccess(wallet) {
  return { type: types.ADD_WALLET_SUCCESS, wallet};
}

export function updateWalletSuccess(modifiedWallet) {
  return { type: types.UPDATE_WALLET_SUCCESS, modifiedWallet};
}

export function loadWalletListSuccess(walletList) {
  return { type: types.LOAD_WALLET_LIST_SUCCESS, walletList};
}

export function deleteFromCartSuccess(wid) {
  return { type: types.DELETE_WALLET_SUCCESS, wid};
}

export function addWallet(type, name, address, uid) {
  return function(dispatch) {
    dispatch(beginLoading());
    return fetch(constant.FETCH_ADDRESS+'/addWallet',{
      method: "POST",
      body: JSON.stringify({"uid": uid, "type": type, "name": name, "address": address})
    })
    .then((newWallet) => newWallet.json())
    .then(newWallet => {
      alertify.success("New "+newWallet.type+" wallet has been added to the list.");
      dispatch(addWalletSuccess(newWallet));
    }).catch(error => {
      alertify.error("Something went wrong!");
      dispatch(terminateLoading());
      throw(error);
    });
  };
}

export function updateWallet(type, name, address, wid) {
  return function(dispatch) {
    dispatch(beginLoading());
    return fetch(constant.FETCH_ADDRESS+'/updateWallet',{
      method: "POST",
      body: JSON.stringify({"wid": wid, "type": type, "name": name, "address": address})
    })
    .then((modifiedWallet) => modifiedWallet.json())
    .then(modifiedWallet => {
      $('#walletEditModal').modal('hide');
      alertify.success(modifiedWallet.type+" Wallet Updated");
      dispatch(updateWalletSuccess(modifiedWallet));
    }).catch(error => {
      dispatch(terminateLoading());
      throw(error);
    });
  };
}

export function loadWalletList(uid) {
  const user_id = uid;
  return function(dispatch) {
    dispatch(beginLoading());
    return fetch(constant.FETCH_ADDRESS+'/loadWalletList',{
      method: "POST",
      body: JSON.stringify({"uid": user_id})
    })
    .then((walletList) => walletList.json())
    .then(walletList => {
      dispatch(loadWalletListSuccess(walletList));
    }).catch(error => {
      dispatch(terminateLoading());
      throw(error);
    });
  };
}

export function deleteWallet(wid) {
  return function(dispatch) {
    dispatch(beginLoading());
    return fetch(constant.FETCH_ADDRESS+'/deleteWallet',{
      method: "POST",
      body: JSON.stringify({"wid":wid})
    })
    .then((wallet) => wallet.json())
    .then(wallet => {
      alertify.error("Your "+wallet.type+" wallet is removed.");
      dispatch(deleteFromCartSuccess(wallet.wid));
    }).catch(error => {
      dispatch(terminateLoading());
      throw(error);
    });
  };
}

export function sortWallet(uid, walletOrder, sortedWalletList, unsortedWalletList) {
  return function(dispatch) {
    dispatch(beginLoading());
    return fetch(constant.FETCH_ADDRESS+'/sortWallet',{
      method: "POST",
      body: JSON.stringify({"uid": uid, "walletOrder": walletOrder})
    })
    .then((response) => response.json())
    .then(response => {
      if(response.status === 'SUCCESS')
      dispatch(loadWalletListSuccess(sortedWalletList));
      else {
        dispatch(loadWalletListSuccess(unsortedWalletList));
        alertify.error("Wallet List not updated");
      }
    }).catch(error => {
      dispatch(terminateLoading());
      throw(error);
    });
  };
}
