import * as types from './actionTypes';
import {beginPublicLoading, terminatePublicLoading} from './loadingActions';
import alertify from 'alertify.js';
import * as constant from '../config/const';

export function emptyPreviousSearch() {
  return { type: types.EMPTY_USERNAME_SEARCH };
}

export function searchUsernameSuccess(userDetails) {
  return { type: types.SEARCH_USERNAME_RECEIVED, userDetails };
}

export function searchUsername(username) {
  return function(dispatch) {
    dispatch(emptyPreviousSearch());
    dispatch(beginPublicLoading());
    return fetch(constant.FETCH_ADDRESS+'/searchUsername',{
      method: "POST",
      body: JSON.stringify({"username": username})
    })
    .then((response) => response.json())
    .then(response => {
      if(response.search_result == "USER_NOT_FOUND") {
        dispatch(terminatePublicLoading());
        alertify.error("USER not found");
      }
      else {
          setTimeout(function() {
            dispatch(searchUsernameSuccess(response.search_result));
            dispatch(terminatePublicLoading());
          }, 1000);
      }
    }).catch(error => {
      dispatch(terminatePublicLoading());
      alertify.error("Something went wrong!");
      throw(error);
    });
  };
}
