/* eslint-disable import/default */

import 'babel-polyfill';
import React from 'react';
import {render} from 'react-dom';
import configureStore from './store/configureStore';
import {Provider} from 'react-redux';
import { BrowserRouter, Route } from "react-router-dom";
import jwt from 'jsonwebtoken';
import App from './components/App';
import {authenticationSuccess, relogin} from './actions/authenticateActions';
import {loadWalletList} from './actions/walletActions';
import {beginLoading} from './actions/loadingActions';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './styles/index.css';

const store = configureStore();
if(localStorage.cryptoLinkJWT) {
  const decoded = jwt.verify(localStorage.cryptoLinkJWT, 'secretkey');
  const user = {token: localStorage.cryptoLinkJWT, uid: decoded.uid, username: decoded.username, full_name: decoded.full_name, email: decoded.email, user_image: decoded.user_image};
  store.dispatch(beginLoading());
  store.dispatch(authenticationSuccess(user.token));
  store.dispatch(relogin(user.uid));
  store.dispatch(loadWalletList(user.uid));
}
// debugger;
// debugger;

render(
  <BrowserRouter>
    <Provider store={store}>
      <Route path="/" component={App}/>
    </Provider>
  </BrowserRouter>,
    document.getElementById('cryptoLink')
);
